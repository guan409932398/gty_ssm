function navBar(strData) {
    var data;

    if (typeof (strData) == "string") {
        var data = JSON.parse(strData); //部分用户解析出来的是字符串，转换一下
    } else {
        data = strData;
    }

    var ulHtml = '<ul class="layui-nav layui-nav-tree">';

    for (var i = 0; i < data.length; i++) {
        if (data[i].spread) {
            ulHtml += '<li class="layui-nav-item">';
        } else {
            ulHtml += '<li class="layui-nav-item layui-nav-itemed">';
        }

        if (data[i].childNodes != undefined && data[i].childNodes.length > 0) {
            ulHtml += '<a href="javascript:;">';

            if (data[i].icon != undefined && data[i].icon != '') {
                if (data[i].icon.indexOf("icon-") != -1) {
                    ulHtml += '<i class="iconfont ' + data[i].icon + '" data-icon="' + data[i].icon + '"></i>';
                } else {
                    ulHtml += '<i class="layui-icon" data-icon="' + data[i].icon + '">' + data[i].icon + '</i>';
                }
            }

            ulHtml += '<cite>' + data[i].title + '</cite>';
            ulHtml += '<span class="layui-nav-more"></span>';
            ulHtml += '</a>';
            ulHtml += '<dl class="layui-nav-child">';

            for (var j = 0; j < data[i].childNodes.length; j++) {
                if (data[i].childNodes[j].target == "_blank") {
                    ulHtml += '<dd><a href="javascript:;" data-url="' + data[i].childNodes[j].href + '" target="' + data[i].childNodes[j].target + '">';
                } else {
                    ulHtml += '<dd><a href="javascript:;" data-url="' + data[i].childNodes[j].href + '">';
                }

                if (data[i].childNodes[j].icon != undefined && data[i].childNodes[j].icon != '') {
                    if (data[i].childNodes[j].icon.indexOf("icon-") != -1) {
                        ulHtml += '<i class="iconfont ' + data[i].childNodes[j].icon + '" data-icon="' + data[i].childNodes[j].icon + '"></i>';
                    } else {
                        ulHtml += '<i class="layui-icon" data-icon="' + data[i].childNodes[j].icon + '">' + data[i].childNodes[j].icon + '</i>';
                    }
                }

                ulHtml += '<cite>' + data[i].childNodes[j].title + '</cite></a></dd>';
            }

            ulHtml += "</dl>";
        } else {
            if (data[i].target == "_blank") {
                ulHtml += '<a href="javascript:;" data-url="' + data[i].href + '" target="' + data[i].target + '">';
            } else {
                ulHtml += '<a href="javascript:;" data-url="' + data[i].href + '">';
            }

            if (data[i].icon != undefined && data[i].icon != '') {
                if (data[i].icon.indexOf("icon-") != -1) {
                    ulHtml += '<i class="iconfont ' + data[i].icon + '" data-icon="' + data[i].icon + '"></i>';
                } else {
                    ulHtml += '<i class="layui-icon" data-icon="' + data[i].icon + '">' + data[i].icon + '</i>';
                }
            }

            ulHtml += '<cite>' + data[i].title + '</cite></a>';
        }

        ulHtml += '</li>';
    }

    ulHtml += '</ul>';

    return ulHtml;
}
