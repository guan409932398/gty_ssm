Docker
----------------
**系统要求**

	CentOS 7 64bit
	内核版本不低于 3.10 由于内核版本比较低，部分功能（如 overlay2 存储层驱动）无法使用，并且部分功能可能不太稳定。

**1. 卸载旧版本Docker**

	sudo yum remove docker docker-common docker-selinux docker-engine

**2.安装Docker依赖包**

	sudo yum install -y yum-utils device-mapper-persistent-data lvm2

**3.添加国内yum源**

	sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
	
	///////////以下为官方源///////////
	sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

**4.如需使用最新版本CE使用以下命令**

	sudo yum-config-manager --enable docker-ce-edge
	sudo yum-config-manager --enable docker-ce-test

**5.更新yum软件源缓存**

	sudo yum makecache fast

**6.安装Docker CE版（moby：开源社区维护, ce版：开源免费，ee版：闭源商用）**

	sudo yum install docker-ce

**7.修改Docker官方仓库到中国区加速**

	vi /etc/docker/daemon.json

	///////////以下daemon.json修改后内容///////////
	{
		"registry-mirrors": [
			"https://registry.docker-cn.com"
		]
	}

	///////////检查加速期是否生效///////////
	// docker info
	// 返回信息出现 “https://registry.docker-cn.com/” 说明成功

**8.设置Docker开机启动**

	systemctl enable docker.service

**9.启动Docker CE**
	
	sudo systemctl enable docker
	sudo systemctl start docker

**10.验证Docker是否成功安装(返回信息中出现“Hello from Docker!”说明已成功安装)**

	docker run hello-world

**11.1.安装Tomcat，搜索Tomcat镜像**

	docker search tomcat

**11.2.安装Tomcat镜像（Apache tomcat 一般是第一位）**

	// 拉取指定版本的tomcat镜像(支持的版本信息可以从 “https://hub.docker.com/” 查询得到)
	docker pull tomcat:8.0
	// 拉取最新版本的tomcat镜像
	docker pull tomcat

**11.3.设置容器名、端口并尝试启动**
	
	docker run -p 8080:8080 --name 容器别名 tomcat:容器版本号 
	// -p : 前边的8080是本机的端口，冒号后面的8080是docker容器的端口，tomcat默认是8080
	// –name : 是给容器起一个别名，方便使用，不然docker会默认给容器一个随机字符串的名称

**11.4.设置容器名、端口并尝试启动**

	docker start 容器ID或容器名

**12.1.安装jdk**
	
	下载jdk

**12.2.复制jdk文件到容器中**

	docker cp 文件目录/jdk文件名 容器名称或者ID:/root

**12.3.进入Docker容器（注：容器必须先启动）**

	docker exec -it 容器名称或者ID bash

**12.4.在容器中解压gz文件**
	cd ~
	tar -zxvf  jdk-8u121-linux-x64.tar.gz
	chown root:root ~/jdk1.8.0_121 -R

**12.5.在容器中建立容器系统的JAVA_HOME目录**

	mkdir /usr/lib/jvm

**12.6.移动jdk目录到JAVA_HOME目录**

	mv ~/jdk1.8.0_121  /usr/lib/jvm

**12.7.安装容器vim**

	apt-get update
	apt-get install vim

**12.8.设置容器环境变量**
	
	vim ~/.bashrc
	#set oracle jdk environment
	export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_121  ## 这里要注意目录要换成自己解压的jdk 目录
	export JRE_HOME=${JAVA_HOME}/jre  
	export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib  
	export PATH=${JAVA_HOME}/bin:$PATH

**12.9.使得环境变量马上生效**

	source ~/.bashrc

**12.10.验证JDK版本**

	java -version

**13.docker容器中安装ifconfig **

	apt-get update
	apt install net-tools # ifconfig 
	apt install iputils-ping # ping

**Docker 常用命令**

	// 启动服务
	service docker start
	// systemctl start docker
	// 重启服务
	service docker restart 
	// 停止服务
	service docker stop
	// 查看Docker版本
	docker version
	// 查看Docker上正在运行的容器
	docker ps
	// 查看Docker上所有容器
	docker ps -a
	// 查看Docker上所有的镜像
	docker images
	// 启动Docker上的容器
	docker start 容器ID或容器名（容器ID可以从 docker ps -a 获取）
	// 停止Docker上的容器
	docker stop 容器ID或容器名（容器ID可以从 docker ps -a 获取）
	// docker stop -t=60 容器ID或容器名 
	 -- 参数 -t：关闭容器的限时，如果超时未能关闭则用kill强制关闭，默认值10s，这个时间用于容器的自己保存状态 
	 -- 直接关闭容器
	docker kill 容器ID或容器名
	// 重启容器
	docker restart 
	// 进入当前容器
	docker exec -it 容器名称或者ID bash
	// 退出当前容器
	// 退出容器不关闭容器
	ctrl+p+q 
	// 退出容器且关闭容器
	ctrl+d
	// 向Docker容器中复制文件
	docker cp 文件目录/文件名 容器名称或者ID:/root
	 -- 例：docker cp /usr/tomcat/apache-tomcat-8.0.30/webapps/todo_yinshan mytomcat:/usr/local/tomcat/webapps
	







