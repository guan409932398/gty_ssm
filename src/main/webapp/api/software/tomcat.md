Tomcat
----------------
**server.xml 配置调优**

	<?xml version="1.0" encoding="UTF-8"?>
	<Server port="8005" shutdown="SHUTDOWN">
		<Listener className="org.apache.catalina.startup.VersionLoggerListener"/>
		<Listener SSLEngine="on" className="org.apache.catalina.core.AprLifecycleListener"/>
		<Listener className="org.apache.catalina.core.JasperListener"/>
		<Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener"/>
		<Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener"/>
		<Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener"/>
	
		<GlobalNamingResources>
			<Resource auth="Container" description="User database that can be updated and saved" factory="org.apache.catalina.users.MemoryUserDatabaseFactory" name="UserDatabase" pathname="conf/tomcat-users.xml" type="org.apache.catalina.UserDatabase"/>
		</GlobalNamingResources>
	
		<Service name="Catalina">
			<!-- 
				Tomcat 线程池（处理tomcat并发）
				maxThreads：最大并发数（默认设置 200，一般建议在 500 ~ 800）
				minSpareThreads：Tomcat 初始化时创建的线程数（默认设置 25）
				prestartminSpareThreads： 在 Tomcat 初始化的时候就初始化 minSpareThreads 的参数值，如果不等于 true，minSpareThreads 的值就没啥效果了
				maxQueueSize：最大的等待队列数，超过则拒绝请求
			-->
			<Executor 
				name="tomcatThreadPool" 
				namePrefix="catalina-exec-" 
				maxThreads="500" 
				minSpareThreads="100" 
				prestartminSpareThreads="true" 
				maxQueueSize = "100"/>
	
			<!-- 
				Tomcat 连接器
				executor： 使用的线程池名称
				port： 访问端口
				protocol：协议（Tomcat 8 设置 nio2 更好：org.apache.coyote.http11.Http11Nio2Protocol，Tomcat 6、7 设置 nio 更好：org.apache.coyote.http11.Http11NioProtocol）
				connectionTimeout： 超时时间
				maxConnections： 最大连接数
				redirectPort： 重定向端口
				enableLookups：禁用DNS查询
				acceptCount：指定当所有可以使用的处理请求的线程数都被使用时，可以放到处理队列中的请求数，超过这个数的请求将不予处理，默认设置 100
				maxPostSize：以 FORM URL 参数方式的 POST 提交方式，限制提交最大的大小，默认是 2097152(2兆)，它使用的单位是字节。10485760 为 10M。如果要禁用限制，则可以设置为 -1。
				acceptorThreadCount：用于接收连接的线程的数量，默认值是1。一般这个指需要改动的时候是因为该服务器是一个多核CPU，如果是多核 CPU 一般配置为 2.
			-->
			<Connector 
				executor="tomcatThreadPool"
				port="80" 
				protocol="org.apache.coyote.http11.Http11NioProtocol" 
				connectionTimeout="20000" 
				maxConnections="10000" 
				redirectPort="8443" 
				enableLookups="false" 
				acceptCount="100" 
				maxPostSize="10485760" 
				compression="on" 
				disableUploadTimeout="true" 
				compressionMinSize="2048" 
				acceptorThreadCount="2" 
				compressableMimeType="text/html,text/xml,text/plain,text/css,text/javascript,application/javascript" 
				URIEncoding="utf-8"/>
			
			<!-- 禁用 AJP -->
			<!-- <Connector port="8009" protocol="AJP/1.3" redirectPort="8443"/> -->
	
			<Engine defaultHost="localhost" name="Catalina">
				<Host appBase="webapps" autoDeploy="true" name="localhost" unpackWARs="true">
					<Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs" pattern="%h %l %u %t &quot;%r&quot; %s %b" prefix="localhost_access_log." suffix=".txt"/>
					<Context docBase="gty_fp" path="/gty_fp" reloadable="true" source="org.eclipse.jst.jee.server:gty_fp"/>
				</Host>
			</Engine>
		</Service>
	</Server>