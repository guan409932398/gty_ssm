Mysql
----------------
**导出**

	// 导出SQL文件
	mysqldump -u 用户名 -p 数据库名 > 导出的文件名;
	
**字段**

	// 增加列
	alter table TABLE_NAME add column NEW_COLUMN_NAME varchar(20) not null;
	
**索引**

	// 普通索引
	alter table table_name add index index_name (column_list) ;
	// 唯一索引
	alter table table_name add unique (column_list) ;
	// 主键索引
	alter table table_name add primary key (column_list) ;
	// 删除索引
	drop index index_name on table_name ;
