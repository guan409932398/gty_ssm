Sql Server
----------------
**1. 查询数据库dbid**

	Select * from master..sysdatabases where name = '数据库名'

**2. 查询使用数据库的进程**

	 Select * from sys.sysprocesses a where a.dbid = '1步骤中的dbid';
	
**3. 停止使用进程**

	kill 进程号

**4. 设置数据库offline**

	ALTER DATABASE 数据库名 SET OFFLINE WITH ROLLBACK IMMEDIATE
	
**5. 设置数据库online**
	
	ALTER DATABASE 数据库名 SET ONLINE WITH ROLLBACK IMMEDIATE  

**6. 设置数据库单用户模式**
	
	 USE MASTER
     GO
     ALTER DATABASE 数据库名字 SET SINGLE_USER WITH ROLLBACK IMMEDIATE; 
     GO
	
**7. 设置数据库多用户模式**

    USE master; 
    GO
    DECLARE @SQL VARCHAR(MAX); 
    SET @SQL='' 
    SELECT @SQL=@SQL+'; KILL '+RTRIM(SPID) 
    FROM master..sysprocesses 
    WHERE dbid=DB_ID('数据库名'); 
    EXEC(@SQL); 
    GO
    ALTER DATABASE 数据库名 SET MULTI_USER;
