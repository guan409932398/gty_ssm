Git
----------------
**1. 安装Git,运行Git Bash**

	Git Bash

**2. 设置用户名、邮箱（首次时设置即可）**

	 git config --global user.name "gty"
	 git config --global user.email "guan409932398@qq.com"
	
**3. 进入D盘，创建本地仓库文件夹git_repository（首次时设置即可）**

	cd D:
	mkdir git_repository

**4. 复制码云克隆地址（https），克隆目标仓库（首次时设置即可）**

	cd git_repository
	git clone https://gitee.com/gty409932398/gty_fp.git
	#输入码云帐号/密码
	#git clone -b 分支名称 XXX （从指定分支检出）
	
**5. 初始化本地仓库（当前）目录（远程仓库无需此步，本地仓库首次时设置即可）**
	
	git init

**6. 更新本地仓库（master：分支名，–allow-unrelated-histories：是为了防止git pull 失败 ,提示：fatal: refusing to merge unrelated histories【拒绝合并无关的历史】）**
	
	git pull gty_fp master --allow-unrelated-histories
	
**7. 修改项目内容**

**8. 提交当前目录下所有文件**

	git add .

**9. 对提交的动作进行说明**

	git commit -m '初始化项目信息'
	
**10. 添加新的远程仓库关联（gty_fp： 仓库名）**

	git remote add gty_fp https://gitee.com/gty409932398/gty_fp.git
	#删除关联的远程仓库：git remote remove gty_fp

**11. 推送到远程仓库（gty_fp： 仓库名，master： 分支）**

	git push gty_fp master 

**>> 常见命令**

	1. 查看状态：git status
	2. 切换分支：git checkout 分支名
	3. 查看remote列表：git remote -v

**>> 常见问题处理**

**>> 1. 执行git add . 命令时，提示（warning: LF will be replaced by CRLF）**

	异常原因：windows中的换行符为 CRLF， 而在Linux下的换行符为LF
	处理方法：git config core.autocrlf false
	
**>> 2. error: RPC failed; curl 18 transfer closed with outstanding read data remaining**

    异常原因：curl的postBuffer的默认值太小
    处理方法：git config --global http.postBuffer 524288000

**>> 3. 提交时忽略某些文件**

	> 进入仓库运行： touch .gitignore
	> 编辑 .gitignore 文件（可以使用notepad），添加过滤文件内容
		##ignore this file##
		/target/ 
		
		.classpath
		.project
		.settings      
		 ##filter databfile、sln file##
		*.mdb  
		*.ldb  
		*.sln    
		##class file##
		*.com  
		*.class  
		*.dll  
		*.exe  
		*.o  
		*.so  
		# compression file
		*.7z  
		*.dmg  
		*.gz  
		*.iso  
		*.jar  
		*.rar  
		*.tar  
		*.zip  
		*.via
		*.tmp
		*.err 
		# OS generated files #  
		.DS_Store  
		.DS_Store?  
		._*  
		.Spotlight-V100  
		.Trashes  
		Icon?  
		ehthumbs.db  
		Thumbs.db

	备注：
		/target/ ：过滤文件设置，表示过滤这个文件夹
		
		*.mdb  ，*.ldb  ，*.sln 表示过滤某种类型的文件
		/mtk/do.c ，/mtk/if.h  表示指定过滤某个文件下具体文件
		 !*.c , !/dir/subdir/     !开头表示不过滤
		 *.[oa]    支持通配符：过滤repo中所有以.o或者.a为扩展名的文件
		
		该方法保证任何人都提交不了这类文件











