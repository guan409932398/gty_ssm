1、获取短信验证码: /common/sendSms
----------------
**请求参数**

    {
        "phone": "String",//手机号
        "sms_model": "String"//短信模版代码
    }

**返回参数**

    {
        "code": "Integer",//返回码
        "count": "Integer",//记录数
        "msg": "String",//返回码说明
        "dec": "String",//详细说明
        "data": "Integer",//成功发送次数
    }
    
2、获取OSSToken: /common/getOssToken
----------------
**请求参数**

    {
        "ossType": "String"//OSS服务商类型（qiniu: 七牛）
    }

**返回参数**

    {
        "code": "Integer",//返回码
        "count": "Integer",//记录数
        "msg": "String",//返回码说明
        "dec": "String",//详细说明
        "data": "String",//OSSToken
    }