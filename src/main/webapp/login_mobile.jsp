<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta name="description" content=""><meta name="author" content="">
		<title>${elfunc:getCon('system_name')}</title>
		<link rel="icon" href="${ctx }/images/favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="${ctx }/plugins/supersized/css/reset.css">
		<link rel="stylesheet" href="${ctx }/plugins/supersized/css/supersized.css">
		<link rel="stylesheet" href="${ctx }/plugins/supersized/css/style.css">
		<!--[if lt IE 9]>
		<script src="${ctx }/js/html5shiv.min.js"></script>
		<![endif]-->
	</head>

	<body oncontextmenu="return false">
		<div class="page-container">
			<h1>${elfunc:getCon('system_name')}</h1>

			<form id="thisForm" action="${ctx }/user/login" method="post">
				<div>
					<input id="account" name="account" type="text" class="loginuser" value="${empty po.account ? '' : po.account}" onclick="JavaScript:this.value=''"/>
				</div>

				<div>
					<input id="password" name="password" type="password" class="loginpwd" value="" onclick="JavaScript:this.value=''"/>
				</div>

				<div>
					<span>
						<input id="securityCode" name="securityCode" type="text" value="验证码" onclick="JavaScript:this.value=''" style="width: 150px;"/>
						<cite><img id="captcha" src="${ctx }/common/getSecurityCode" onclick="refreshCode(this)" style="vertical-align:middle"/></cite>
					</span>
				</div>

				<button id="submit" type="button" onclick="_submit()">登录</button>
			</form>

			<div class="connect"></div>
		</div>

		<!-- Javascript -->
		<script src="${ctx }/plugins/uimaker/js/jquery.js" language="JavaScript" ></script>
		<script src="${ctx }/plugins/supersized/js/supersized.3.2.7.min.js"></script>
		<script src="${ctx }/plugins/layer/layer.js" type="text/javascript"></script>

		<script>
			var isShow = false;

			jQuery(function($){
				$.supersized({slide_interval:6000,transition:1,transition_speed:3000,performance:1,min_width:0,min_height:0,vertical_center:1,horizontal_center:1,fit_always:0,fit_portrait:1,fit_landscape:0,slide_links:"blank",slides:[{image:"${ctx }/plugins/supersized/img/1.jpg"},{image:"${ctx }/plugins/supersized/img/2.jpg"},{image:"${ctx }/plugins/supersized/img/3.jpg"}]});

				if('${msg }' != ''){
					isShow = true;
					layer.alert('${msg }', {icon: 0}, function(){
						isShow = false;
						layer.closeAll();
					});
				}
			});

			if(window != top){
				top.location.href=location.href;
			}

			document.onkeydown = function (event) {
				var e = event || window.event;

				if (e && e.keyCode == 13) {
					$("#submit").click();
				}
			};

			function refreshCode(){
				$('#captcha').attr('src',"${ctx}/common/getSecurityCode?t=" + new Date().getTime());
			}

			function _submit(){
				alert(1);

				if($('#account').val() == ''){
					isShow = true;
					layer.alert('帐号不能为空', {icon: 0}, function(){
						isShow = false;
						layer.closeAll();
					});
					return;
				}

				alert(2);

				if($('#password').val() == ''){
					isShow = true;
					layer.alert('密码不能为空', {icon: 0}, function(){
						isShow = false;
						layer.closeAll();
					});
					return;
				}

				alert(3);

				if($('#securityCode').val() == ''){
					isShow = true;
					layer.alert('验证码不能为空', {icon: 0}, function(){
						isShow = false;
						layer.closeAll();
					});
					return;
				}

				alert(4);

				$('#thisForm').submit();
			}
		</script>
	</body>
</html>