<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <meta charset="utf-8">
	    <title>${elfunc:getCon('system_name')}</title>
	    <link rel="icon" href="${ctx }/images/favicon.ico">
	    <link href="${ctx }/plugins/uimaker/css/style.css" rel="stylesheet" type="text/css" />
		<script src="${ctx }/plugins/uimaker/js/jquery.js" language="JavaScript" ></script>
		<script src="${ctx }/plugins/uimaker/js/cloud.js" type="text/javascript"></script>
		<script src="${ctx }/plugins/layer/layer.js" type="text/javascript"></script>
		
		<script>  
	        var ctx = "${ctx}";  
	    </script>
	    
		<script language="javascript">
			var isShow = false;
		
			$(function(){
		    	$('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
		    	
				$(window).resize(function(){  
			    	$('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
			    });
				
				if('${msg }' != ''){
					isShow = true;
					layer.alert('${msg }', {icon: 0}, function(){
						isShow = false;
						layer.closeAll();
					});
				}
			});  
		</script> 
	</head>
	
	<body style="background-color:#1c77ac; background-image:url(${ctx }/plugins/uimaker/images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">
	    <div id="mainBody">
			<div id="cloud1" class="cloud"></div>
			<div id="cloud2" class="cloud"></div>
	    </div>  


		<div class="logintop">    
    		<span>欢迎登录${elfunc:getCon('system_name')}</span> 
    		   
		    <ul>
			    <li><a href="#">帮助</a></li>
			    <li><a href="#">关于</a></li>
		    </ul>    
    	</div>
    
    	<div class="loginbody">
			<span class="systemlogo"></span> 
       		
       		<form id="thisForm" action="${ctx }/user/login" method="post">
				<div class="loginbox loginbox1">
				    <ul>
					    <li><input id="account" name="account" type="text" class="loginuser" value="${empty po.account ? 'admin' : po.account}" onclick="JavaScript:this.value=''"/></li>
					    <li><input id="password" name="password" type="password" class="loginpwd" value="${empty po.password ? '123456' : po.password}" onclick="JavaScript:this.value=''"/></li>
					    <li class="yzm">
					    	<span><input id="securityCode" name="securityCode" type="text" value="验证码" onclick="JavaScript:this.value=''"/></span><cite><img id="captcha" src="${ctx }/common/getSecurityCode" onclick="refreshCode(this)"/></cite> 
					    </li>
					    <li><input id="loginbtn" type="button" class="loginbtn" value="登录"  onclick="_submit()"/></li>
				    </ul>	
	    		</div>
    		</form>
    	</div>
    
    	<div class="loginbm">版权所有  2018  <a href="#">官天野</a></div>
	</body>
	
	<script language="javascript">
		if(window != top){
	        top.location.href=location.href;
	    }
	    
	    document.onkeydown = function (event) {
	    	if(!isShow){
	    		var e = event || window.event;
		            
	    		if (e && e.keyCode == 13) {
	                $("#loginbtn").click();
	            }
	    	} else {
	    		isShow = false;
	    		layer.closeAll();
	    	}
	    };
	
		function refreshCode(){
			$('#captcha').attr('src',"${ctx}/common/getSecurityCode?t=" + new Date().getTime());
		}
		
		function _submit(){
			if($('#account').val() == ''){
				isShow = true;
				layer.alert('帐号不能为空', {icon: 0}, function(){
					isShow = false;
					layer.closeAll();
				});
				return;
			}
			
			if($('#password').val() == ''){
				isShow = true;
				layer.alert('密码不能为空', {icon: 0}, function(){
					isShow = false;
					layer.closeAll();
				});
				return;
			}
			
			if($('#securityCode').val() == ''){
				isShow = true;
				layer.alert('验证码不能为空', {icon: 0}, function(){
					isShow = false;
					layer.closeAll();
				});
				return;
			}
			
			$('#thisForm').submit();
		}
	</script>
</html>