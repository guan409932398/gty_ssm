<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <meta charset="utf-8">
	    <title>${elfunc:getCon('system_name')}</title>
	    <link rel="icon" href="${ctx }/images/favicon.ico">
	    <link href="${ctx }/plugins/uimaker/css/style.css" rel="stylesheet" type="text/css" />
		<script src="${ctx }/plugins/uimaker/js/jquery.js" language="JavaScript" ></script>
		
		<script>  
	        var ctx = "${ctx}";  
	    </script>
	</head>
	
	<body>
		<ul class="imglist">
			<c:choose>
				<c:when test="${!empty apis}">
					<c:forEach items="${apis}" var="api" varStatus="num">
						<a target="_blank" href="${ctx }/api/detail?key=${api.key }">
						    <li class="selected">
						    	<span><img src="${ctx }/plugins/uimaker/images/api.png" style="width: 168px;height: 126px;"/></span>
						    	<h2 style="font-size: 24px;">${api.key }</h2>
						    </li>
					    </a>
					</c:forEach>
				</c:when>
			</c:choose>
    	</ul>
	</body>
</html>