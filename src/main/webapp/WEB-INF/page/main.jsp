<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>后台首页</title>

		<meta charset="utf-8">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx}/layui/css/layui.css" media="all" />
		<link rel="stylesheet" href="${ctx}/css/font_eolqem241z66flxr.css" media="all" />
		<link rel="stylesheet" href="${ctx}/css/main.css" media="all" />
		<script type="text/javascript" src="${ctx}/js/echarts.js"></script>

		<script>
			var ctx = "${ctx}";
		</script>
	</head>

	<body class="childrenBody" style="margin: 1%">
		<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
	</body>
</html>