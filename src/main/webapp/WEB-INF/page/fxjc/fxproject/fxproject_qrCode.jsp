<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html lang="zh">
	<head>
		<link href="${ctx }/css/qrcode_logo.css" rel="stylesheet" />
	</head>

	<body>
		<input type="hidden" id="render" value="canvas">
		<input type="hidden" id="size" value="400">
		<input type="hidden" id="color" value="#333333">
		<input type="hidden" id="background" value="#ffffff">
		<input type="hidden" id="text" value="http://${elfunc:getCon('qr_code_prefix')}/gty_ssm/fxproject/goDetail?id=${id }">
		<input type="hidden" id="minversion" value="6">
		<input type="hidden" id="eclevel" value="H">
		<input type="hidden" id="quiet" value="1">
		<input type="hidden" id="radius" value="50">

		<input type="hidden" id="mode" value="4">
		<input type="hidden" id="msize" value="25">
		<input type="hidden" id="mposx" value="50">
		<input type="hidden" id="mposy" value="50">
		<input type="hidden" id="label" value="">
		<input type="hidden" id="font" value="仿宋">
		<input type="hidden" id="fontcolor" value="#ff9818">

		<img id="img-buffer" src="${ctx}/images/fxjc/dummy_new.png" />

		<div id="container"></div>

		<script src="${ctx }/js/jquery-1.11.0.min.js" type="text/javascript"></script>
		<script src="${ctx }/js/jquery-qrcode.min.js"></script>
		<script src="${ctx }/js/qrcode_logo.js"></script>
	</body>
</html>