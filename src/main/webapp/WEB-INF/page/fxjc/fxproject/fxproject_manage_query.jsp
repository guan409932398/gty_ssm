<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>列表</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />
        
        <script>
            var ctx = "${ctx}";
        </script>
    </head>

    <body class="childrenBody">
        <blockquote class="layui-elem-quote list_search">
            <form class="layui-form">
                <div class="layui-inline">
                    <input type="text" id="beginDate" class="layui-input" name="beginDate" placeholder="请输入生产开始日期" value="">
                </div>

                <div class="layui-inline">
                    <input type="text" id="endDate" class="layui-input" name="endDate" placeholder="请输入生产结束日期" value="">
                </div>

                <div class="layui-inline">
                    <select id="name" name="name" class="layui-select" >
                        <option value="">请选择产品</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_product_name') and !empty elfunc:getDic('fxjc_product_name').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_product_name').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <select id="model" name="model" class="layui-select" >
                        <option value="">请选择型号</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_model') and !empty elfunc:getDic('fxjc_model').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_model').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <select id="color" name="color" class="layui-select" >
                        <option value="">请选择颜色</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_color') and !empty elfunc:getDic('fxjc_color').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_color').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <select id="executionStandard" name="executionStandard" class="layui-select" >
                        <option value="">请选择执行标准</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_execution_standard') and !empty elfunc:getDic('fxjc_execution_standard').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_execution_standard').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <select id="qualityGrade" name="qualityGrade" class="layui-select" >
                        <option value="">请选择质量等级</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_quality_grade') and !empty elfunc:getDic('fxjc_quality_grade').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_quality_grade').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <select id="inspector" name="inspector" class="layui-select">
                        <option value="">请选择质检员</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_quality_inspector') and !empty elfunc:getDic('fxjc_quality_inspector').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_quality_inspector').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search"><i class="layui-icon">&#xe615;</i>查询</a>
                <a class="layui-btn layui-btn-normal add_btn"><i class="layui-icon">&#xe608;</i> 添加</a>
            </form>
        </blockquote>

        <!-- 数据表格 -->
        <table id="tableList" lay-filter="dt"></table>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/fxjc/fxproject/fxproject_manage_query.js"></script>

        <script type="text/html" id="barEdit">
            <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
            <a class="layui-btn layui-btn-xs" lay-event="editor">编辑</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>

            {{#  if(d.recordState === 'active'){ }}
                <a class="layui-btn layui-btn-xs" lay-event="generate">生成</a>
            {{#  } else{ }}
                <a class="layui-btn layui-btn-xs" lay-event="qrCode">二维码</a>
            {{#  } }}
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-07-15 17:24:50 -->

