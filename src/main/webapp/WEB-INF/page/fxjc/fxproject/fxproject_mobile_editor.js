layui.config({
    base : "js/"
}).use(['form','layer','jquery','laydate','upload'],function(){
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;
    
    form.on("submit(submit_but)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        var msg="发生错误！",flag=false;

        $.ajax({
            type: "post",
            url: ctx + "/fxproject/save",
            data:data.field,
            dataType:"json",
            success:function(d){
            	if(d.code != '200'){
                    top.layer.close(index);
                    top.layer.msg("(" + d.msg + ")");
            	}else{
                    window.history.back();
            	}
			},
            error:function() {
                flag=true;
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        return false;
    });

    // 生产日期
    laydate.render({
        elem: '#productionDateShow',
        type:'datetime'
    });

    if($('#productionDate').val() != null){
        $('#productionDateShow').val(formatTime($('#productionDate').val(),'yyyy-MM-dd hh:mm:ss'));
    }

    //$("input[name=radio_sample][value=value_true]").attr("checked", $('#_radio_sample').val() == 'value_true' ? true : false);
    //$("input[name=radio_sample][value=value_false]").attr("checked", $('#_radio_sample').val() == 'value_false' ? true : false);
    //layui.form.render('radio');
    
    $('#name').val($('#_name').val());
    $('#model').val($('#_model').val());
    $('#color').val($('#_color').val());
    $('#executionStandard').val($('#_executionStandard').val());
    $('#qualityGrade').val($('#_qualityGrade').val());
    $('#inspector').val($('#_inspector').val());
	layui.form.render('select');
})

//格式化时间
function formatTime(UNIX_timestamp,fmt){
    if(UNIX_timestamp == '' || UNIX_timestamp == null){
        return '';
    }

    var datetime = new Date(UNIX_timestamp * 1000);

    if (parseInt(datetime)==datetime) {
        if (datetime.length==10) {
            datetime=parseInt(datetime)*1000;
        } else if(datetime.length==13) {
            datetime=parseInt(datetime);
        }
    }

    datetime=new Date(datetime);

    var o = {
        "M+" : datetime.getMonth()+1,                 //月份
        "d+" : datetime.getDate(),                    //日
        "h+" : datetime.getHours(),                   //小时
        "m+" : datetime.getMinutes(),                 //分
        "s+" : datetime.getSeconds(),                 //秒
        "q+" : Math.floor((datetime.getMonth()+3)/3), //季度
        "S"  : datetime.getMilliseconds()             //毫秒
    };

    if(/(y+)/.test(fmt)){
        fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));
    }

    for(var k in o){
        if(new RegExp("("+ k +")").test(fmt)){
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
    }

    return fmt;
}

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-07-15 17:24:50
