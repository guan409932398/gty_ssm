<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>${po.nameShow }</title>

        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="${ctx }/css/fxjc_project.css">

        <script type="text/javascript" src="${ctx }/plugins/uimaker/js/jquery.js"></script>
    </head>

    <body>
        <div class="container">
            <div class="brand-info list">
                <div class="title brand-title">
                    <img src="${ctx }/images/fxjc/brand.jpg" alt="brand">
                </div>
                <div class="brand-name name">
                    ${po.nameShow }
                </div>
                <div class="product-model flex item">
                    <div class="label">产品型号:</div>
                    <div>${po.modelShow }</div>
                </div>
                <div class="brand-content">
                    <div class="brand-item">
                        <div>${po.colorShow }</div>
                        <div>产品颜色</div>
                    </div>
                    <div class="brand-item">
                        <div>${po.qualityGradeShow }</div>
                        <div>质量等级</div>
                    </div>
                    <div class="brand-item">
                        <div>${po.inspectorShow }</div>
                        <div>检验员</div>
                    </div>
                </div>
                <div class="date flex item">
                    <div class="label">生产日期:</div>
                    <div id="productionDate"></div>
                </div>
                <div class="standard flex item">
                    <div class="label">执行标准:</div>
                    <div>${po.executionStandardShow }</div>
                </div>
            </div>
            <div class="company-info list">
                <div class="title company-title">
                    <img src="${ctx }/images/fxjc/company.jpg" alt="company">
                </div>
                <div class="company-name name">
                    ${elfunc:getCon('company_name')}
                </div>
                <div class="telephone flex item">
                    <div class="label">电话:</div>
                    <div>${elfunc:getCon('company_phone')}</div>
                </div>
                <div class="url flex item">
                    <div class="label">网址:</div>
                    <div>${elfunc:getCon('company_website')}</div>
                </div>
                <div class="address flex item">
                    <div class="label">地址:</div>
                    <div>${elfunc:getCon('company_address')}</div>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript">
        if('${not empty po.productionDate}'){
            $('#productionDate').html(formatTime('${po.productionDate}','yyyy-MM-dd'));
        }

        //格式化时间
        function formatTime(UNIX_timestamp,fmt){
            if(UNIX_timestamp == '' || UNIX_timestamp == null){
                return '';
            }

            var datetime = new Date(UNIX_timestamp * 1000);

            if (parseInt(datetime)==datetime) {
                if (datetime.length==10) {
                    datetime=parseInt(datetime)*1000;
                } else if(datetime.length==13) {
                    datetime=parseInt(datetime);
                }
            }

            datetime=new Date(datetime);

            var o = {
                "M+" : datetime.getMonth()+1,                 //月份
                "d+" : datetime.getDate(),                    //日
                "h+" : datetime.getHours(),                   //小时
                "m+" : datetime.getMinutes(),                 //分
                "s+" : datetime.getSeconds(),                 //秒
                "q+" : Math.floor((datetime.getMonth()+3)/3), //季度
                "S"  : datetime.getMilliseconds()             //毫秒
            };

            if(/(y+)/.test(fmt)){
                fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));
            }

            for(var k in o){
                if(new RegExp("("+ k +")").test(fmt)){
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
                }
            }

            return fmt;
        }
    </script>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-07-15 17:24:50 -->