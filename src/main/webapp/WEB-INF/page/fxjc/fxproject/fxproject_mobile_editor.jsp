<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }" >

            <div class="layui-form-item">
                <label class="layui-form-label">产品名称</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_name" value="${po.name}">

                    <select id="name" name="name" class="layui-select" lay-verify="required">
                        <option value="">请选择产品名称</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_product_name') and !empty elfunc:getDic('fxjc_product_name').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_product_name').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">产品型号</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_model" value="${po.model}">

                    <select id="model" name="model" class="layui-select" lay-verify="required">
                        <option value="">请选择产品型号</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_model') and !empty elfunc:getDic('fxjc_model').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_model').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">产品颜色</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_color" value="${po.color}">

                    <select id="color" name="color" class="layui-select" lay-verify="required">
                        <option value="">请选择产品颜色</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_color') and !empty elfunc:getDic('fxjc_color').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_color').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">生产日期</label>
                <div class="layui-input-block">
                    <input type="hidden" id="productionDate" value="${po.productionDate}" >

                    <input type="text" id="productionDateShow" class="layui-input" placeholder="请选择生产日期" name="productionDateShow" value="" style="width: 75%;float: left;">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">执行标准</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_executionStandard" value="${po.executionStandard}">

                    <select id="executionStandard" name="executionStandard" class="layui-select" lay-verify="required">
                        <option value="">请选择执行标准</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_execution_standard') and !empty elfunc:getDic('fxjc_execution_standard').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_execution_standard').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">质量等级</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_qualityGrade" value="${po.qualityGrade}">

                    <select id="qualityGrade" name="qualityGrade" class="layui-select" lay-verify="required">
                        <option value="">请选择质量等级</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_quality_grade') and !empty elfunc:getDic('fxjc_quality_grade').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_quality_grade').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">产品检验</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_inspector" value="${po.inspector}">

                    <select id="inspector" name="inspector" class="layui-select" lay-verify="required">
                        <option value="">请选择产品检验</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('fxjc_quality_inspector') and !empty elfunc:getDic('fxjc_quality_inspector').dics}">
                                <c:forEach items="${elfunc:getDic('fxjc_quality_inspector').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/fxjc/fxproject/fxproject_mobile_editor.js"></script>
        
        <script type="text/javascript">
            // 回车提交表单
            document.onkeydown = function (e) { 
                var theEvent = window.event || e; 
                var code = theEvent.keyCode || theEvent.which; 

                if (code == 13) { 
                    return false;
                } 
            }
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-07-15 17:24:50 -->

