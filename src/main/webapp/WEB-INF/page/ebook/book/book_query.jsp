<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>列表</title>

        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

    <body class="childrenBody">
        <blockquote class="layui-elem-quote list_search">
            <form class="layui-form">
                <div class="layui-inline">
                    <input type="text" id="title" class="layui-input" name="title" placeholder="请输入书籍名称" value="">
                </div>

                <div class="layui-inline">
                    <input type="text" id="author" class="layui-input" name="author" placeholder="请输入作者" value="">
                </div>

                <div class="layui-inline">
                    <input type="text" id="tag" class="layui-input" name="tag" placeholder="请输入标签" value="">
                </div>

                <div class="layui-inline">
                    <select id="category" name="category" class="layui-select" >
                        <option value="">请选择书籍类别</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('ebook_category') and !empty elfunc:getDic('ebook_category').dics}">
                                <c:forEach items="${elfunc:getDic('ebook_category').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <select id="state" name="state" class="layui-select" >
                        <option value="">请选择书籍状态</option>
                        <option value="serial">连载中</option>
                        <option value="final">已完本</option>
                    </select>
                </div>

                <a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search"><i class="layui-icon">&#xe615;</i>查询</a>
                <a class="layui-btn layui-btn-normal add_btn"><i class="layui-icon">&#xe608;</i> 添加</a>
            </form>
        </blockquote>

        <!-- 数据表格 -->
        <table id="tableList" lay-filter="dt"></table>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/ebook/book/book_query.js"></script>

        <script type="text/html" id="barEdit">
            <a class="layui-btn layui-btn-xs" lay-event="chapter">章节</a>
            <a class="layui-btn layui-btn-xs" lay-event="editor">编辑</a>
            <a class="layui-btn layui-btn-xs" lay-event="download">下载</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        </script>

        <script type="text/html" id="stateTpl">
            {{#  if(d.state === 'serial'){ }}
                <span style="color: #00FF00;">连载中</span>
            {{#  } else if(d.state === 'final'){ }}
                <span style="color: #FF0000;">已完本</span>
            {{#  } else{ }}
                <span style="color: #FFFF00;">未知状态</span>
            {{#  } }}
        </script>

        <script type="text/html" id="proofreadTpl">
            {{#  if(d.proofread === 'unchecked'){ }}
            <span style="color: #00FF00;">未校对</span>
            {{#  } else if(d.proofread === 'checked'){ }}
            <span style="color: #FF0000;">已校对</span>
            {{#  } else{ }}
            <span style="color: #FFFF00;">未知状态</span>
            {{#  } }}
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-20 09:55:54 -->

