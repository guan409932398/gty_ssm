<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" href="${ctx }/plugins/Tags/css/tag.css" />

        <script type="text/javascript" src="${ctx }/plugins/Tags/js/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="${ctx }/plugins/Tags/js/tag.js"></script>

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }" >
			
            <div class="layui-form-item">
                <label class="layui-form-label">书籍标题</label>
                <div class="layui-input-block">
                	<input type="text" id="title" class="layui-input" placeholder="请输入书籍标题" name="title" value="${po.title }">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">书籍类别</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_category" value="${po.category }" >

                    <select id="category" name="category" class="layui-select" >
                        <option value="">请选择书籍类别</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('ebook_category') and !empty elfunc:getDic('ebook_category').dics}">
                                <c:forEach items="${elfunc:getDic('ebook_category').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">作者</label>
                <div class="layui-input-block">
                	<input type="text" id="author" class="layui-input" placeholder="请输入作者" name="author" value="${po.author }">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">关键字</label>
                <div class="layui-input-block">
                    <label style="height: 38px;line-height: 38px;color: #FF5722">Enter（回车）输入下一关键字</label>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">&nbsp;&nbsp;</label>
                <div class="layui-input-block" style="margin-top: -20px;">
                    <input type="text" id="tag" class="layui-input" placeholder="请输入关键字" name="tag" value="${po.tag}" maxlength="200">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">书籍状态</label>
                <div class="layui-input-block">
                    <input type="hidden" id="state" value="${po.state }" >
                    <input type="radio" name="state" value="serial" title="连载中"  >
                    <input type="radio" name="state" value="final" title="已完本" >
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">校对状态</label>
                <div class="layui-input-block">
                    <input type="hidden" id="proofread" value="${po.proofread }" >
                    <input type="radio" name="proofread" value="unchecked" title="未校对"  >
                    <input type="radio" name="proofread" value="checked" title="已校对" >
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">字数</label>
                <div class="layui-input-block">
                    <label style="height: 38px;line-height: 38px;">${empty po.words ? 0 : po.words}</label>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" placeholder="请输入备注" class="layui-textarea" rows="5">${po.remark }</textarea>
                </div>
            </div>
            
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/ebook/book/book_editor.js"></script>
        
        <script type="text/javascript">
            // 回车提交表单
            document.onkeydown = function (e) { 
                var theEvent = window.event || e; 
                var code = theEvent.keyCode || theEvent.which; 

                if (code == 13) { 
                    return false;
                } 
            }  
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-20 09:55:54 -->

