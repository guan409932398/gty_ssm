layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/book/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                top.layer.close(index);

                if (d.code != '200') {
                    top.layer.msg("(" + d.msg + ")");
                } else {
                    layer.closeAll("iframe");
                }
            }
        });

        return false;
    });


    $("input[name=proofread][value=unchecked]").attr("checked", $('#proofread').val() == 'unchecked' || $('#proofread').val() == '' ? true : false);
    $("input[name=proofread][value=checked]").attr("checked", $('#proofread').val() == 'checked' ? true : false);

    $("input[name=state][value=serial]").attr("checked", $('#state').val() == 'serial' || $('#state').val() == '' ? true : false);
    $("input[name=state][value=final]").attr("checked", $('#state').val() == 'final' ? true : false);
    layui.form.render('radio');

    $('#category').val($('#_category').val());
    layui.form.render('select');

    var tag1 = new Tag("tag");
    tag1.tagValue = $('#tag').val();
    tag1.initView();
})

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-06-20 09:55:54
