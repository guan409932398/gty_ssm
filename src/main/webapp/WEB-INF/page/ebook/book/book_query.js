layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl', 'laydate'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var table = layui.table;
    var laypage = layui.laypage;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    active = {
        search: function () {
            var title = $('#title');
            var author = $('#author');
            var state = $('#state');
            var category = $('#category');
            var tag = $('#tag');

            table.reload('tableList', {
                page: {
                    curr: 1
                },
                where: {
                    title: title.val(),
                    author: author.val(),
                    state: state.val(),
                    category: category.val(),
                    tag: tag.val()
                }
            });
        }
    };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/book/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'title', title: '名称'},
            {field: 'author', title: '作者'},
            {field: 'tag', title: '关键字'},
            {field: 'categoryName', title: '类别', width: 100, align: 'center'},
            {field: 'proofread', title: '校对', templet: '#proofreadTpl', width: 100, align: 'center'},
            {field: 'state', title: '状态', templet: '#stateTpl', width: 100, align: 'center'},
            {field: 'words', title: '字数', width: 110},
            {
                field: 'createTime',
                title: '创建时间',
                templet: '<div>{{ formatTime(d.createTime,"yyyy-MM-dd")}}</div>',
                width: 110
            },
            {title: '操作', toolbar: '#barEdit', align: 'center', width: 210}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(dt)', function (obj) {
        var data = obj.data;

        if (obj.event === 'del') {
            layer.confirm('删除后无法恢复，确认删除吗？', function (index) {
                $.ajax({
                    url: ctx + '/book/del?id=' + data.id,
                    type: "post",
                    success: function (result) {
                        if (result.code == 200) {
                            $(".search_btn").click();
                        } else {
                            layer.msg(result.msg, {icon: 5});
                        }
                    }
                });

                layer.close(index);
            });
        } else if (obj.event === 'editor') {
            layer.open({
                type: 2,
                title: "书籍维护",
                area: ['800px', '600px'],
                content: ctx + "/book/goEditor?id=" + data.id,
                end: function () {
                    table.reload('tableList', {});
                }
            });
        } else if (obj.event === 'chapter') {
            var iframes = $("iframe", parent.document);

            for (var i = 0; i < iframes.length; i++) {
                var iframe = iframes[i];

                if ($(iframe).attr("src") === "book/goQuery" || $(iframe).attr("src").indexOf("chapter/goQuery?bookId=") != -1) {
                    $(iframe).attr("src", "chapter/goQuery?bookId=" + data.id + "&bookName=" + data.title);
                }
            }
        } else if (obj.event === 'download') {
            window.open(ctx + "/chapter/download?bookId=" + data.id + "&bookName=" + data.title);
        }
    });

    //添加
    $(".add_btn").click(function () {
        layer.open({
            title: "信息新增",
            type: 2,
            area: ['800px', '600px'],
            content: ctx + "/book/goAdd",
            end: function () {
                table.reload('tableList', {});
            }
        });
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})

//格式化时间
function formatTime(UNIX_timestamp, fmt) {
    if (UNIX_timestamp == '' || UNIX_timestamp == null) {
        return '';
    }

    var datetime = new Date(UNIX_timestamp * 1000);

    if (parseInt(datetime) == datetime) {
        if (datetime.length == 10) {
            datetime = parseInt(datetime) * 1000;
        } else if (datetime.length == 13) {
            datetime = parseInt(datetime);
        }
    }

    datetime = new Date(datetime);

    var o = {
        "M+": datetime.getMonth() + 1,                 //月份
        "d+": datetime.getDate(),                    //日
        "h+": datetime.getHours(),                   //小时
        "m+": datetime.getMinutes(),                 //分
        "s+": datetime.getSeconds(),                 //秒
        "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
        "S": datetime.getMilliseconds()             //毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }

    return fmt;
}

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-06-20 09:55:54
