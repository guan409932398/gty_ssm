layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/chapter/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                if (d.code == 200) {
                    location.reload();
                } else {
                    layer.msg("(" + d.msg + ")");
                }
            }
        });

        return false;
    });

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/chapter/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                layer.msg("(" + d.msg + ")");
            }
        });

        return false;
    });

    $("#preview_but").bind("click", function () {
        window.open(ctx + "/chapter/goDetail?id=" + $('#id').val());
        return false;
    });

    $('#content').css("width", $(document.body).width());
    $('#content').css("height", $(document).height() - 100);

    $(window).resize(function () {
        $('#content').css("width", $(document.body).width());
        $('#content').css("height", $(document).height() - 100);
    });
})

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-06-20 10:06:04
