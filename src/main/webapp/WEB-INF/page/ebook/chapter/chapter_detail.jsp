<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>${po.title }</title>

        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <script>
            var ctx = "${ctx}";
        </script>

        <script src="${ctx }/plugins/uimaker/js/jquery.js" language="JavaScript" ></script>

        <style type="text/css">
            blockquote,body,button,dd,dl,dt,embed,fieldset,form,h1,h2,h3,h4,h5,h6,input,li,object,ol,pre,table,td,th,ul{margin:0;padding:0;font-size:12px;word-wrap:break-word;font-family:PingFangSC-Regular,HelveticaNeue-Light,'Helvetica Neue Light','Microsoft YaHei',sans-serif,Simsun;color:#333;}
            div{display:block;}
            div,p{padding:0;margin:0;}
            element.style{user-select:none;font-size:14px;}
            .rw_3{width:960px;}
            #reader_warp{margin:0 auto;position:relative;}
            .rb_1{background:#d9cdb6;}
            .reader_box{padding:0 100px;position:relative;}
            .rb_1 .reader_box{background:#faeed7;box-shadow:0 2px 20px 0 rgba(0,0,0,.05);}
            .reader_box .title{padding-top:80px;padding-bottom:10px;text-align:center;}
            .reader_box .title .iconbox{display:inline-block;}
            .title_txtbox{line-height:40px;font-size:28px;color:#333;letter-spacing:1.08px;display:inline;font-weight:700;}
            .bookinfo{text-align:center;font-family:MicrosoftYaHei;font-size:12px;letter-spacing:.86px;line-height:23px;padding-bottom:20px;position:relative;}
            .bookinfo{color:#999;}
            .reader_box .content{padding:30px 0 50px;}
            .reader_box .content p{letter-spacing:1px;line-height:2em;text-indent:2em;padding:.5em .3em;}
            .reader_box .content p{color:#333;-moz-user-select:none;-webkit-user-select:none;}
            .reader_line{border-top:1px dotted #ccc;height:1px;overflow:hidden;display:block;}
            .h10-blank,.h15-blank,.h20-blank,.h40-blank,.h5-blank{line-height:0;font-size:0;overflow:hidden;clear:both;}
            .h10-blank{height:10px;}
            .space_h80{height:80px;clear:both;}
        </style>
    </head>

    <body class="rb_1">
        <div class="rw_3" id="reader_warp">
            <div class="rft_1" id="readerFt" style="user-select: none; font-size: 14px;" unselectable="on" onselectstart="return false;">
                <div class="reader_box">
                    <div class="title">
                        <div class="iconbox"></div>
                        <div class="title_txtbox">${po.title }</div>
                    </div>
                    <div class="bookinfo">
                        作者：${author }&nbsp;&nbsp;|&nbsp;&nbsp;字数：<span>${po.word }</span>&nbsp;&nbsp;|&nbsp;&nbsp;更新时间：<span id="modifyTime"></span>
                    </div>
                    <div class="reader_line"></div>
                    <div class="content" itemprop="acticleBody">
                        <c:choose>
                            <c:when test="${!empty contents}">
                                <c:forEach items="${contents}" var="vpo" varStatus="num">
                                    <p>${vpo }</p>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td colspan="99" style="text-align: center;">暂无数据</td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="reader_line"></div>
                    <div class="h10-blank"></div>
                    <div style="position:relative" class="flowbottom_donate"></div>
                    <div class="space_h80"></div>
                </div>
            </div>
        </div>
    </body>

    <script language="javascript">
        $(function(){
            $('#modifyTime').html(formatTime('${po.modifyTime}','yyyy-MM-dd hh:mm:ss'));
        });

        //格式化时间
        function formatTime(UNIX_timestamp, fmt) {
            if (UNIX_timestamp == '' || UNIX_timestamp == null) {
                return '';
            }

            var datetime = new Date(UNIX_timestamp * 1000);

            if (parseInt(datetime) == datetime) {
                if (datetime.length == 10) {
                    datetime = parseInt(datetime) * 1000;
                } else if (datetime.length == 13) {
                    datetime = parseInt(datetime);
                }
            }

            datetime = new Date(datetime);

            var o = {
                "M+": datetime.getMonth() + 1,                 //月份
                "d+": datetime.getDate(),                    //日
                "h+": datetime.getHours(),                   //小时
                "m+": datetime.getMinutes(),                 //分
                "s+": datetime.getSeconds(),                 //秒
                "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
                "S": datetime.getMilliseconds()             //毫秒
            };

            if (/(y+)/.test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
            }

            for (var k in o) {
                if (new RegExp("(" + k + ")").test(fmt)) {
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                }
            }

            return fmt;
        }
    </script>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-20 10:06:04 -->