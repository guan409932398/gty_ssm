<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">

            <input type="hidden" id="id" name="id" value="${po.id }" >
            <input type="hidden" id="bookid" name="bookid" value="${po.bookid }" >

            <div class="layui-form-item">
                <label class="layui-form-label">章节序号</label>
                <div class="layui-input-block">
                    <input type="text" id="sort" class="layui-input" placeholder="请输入章节序号" name="sort" value="${po.sort }" >
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">章节标题</label>
                <div class="layui-input-block">
                	<input type="text" id="title" class="layui-input" placeholder="请输入章节标题" name="title" value="${po.title }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">字数</label>
                <div class="layui-input-block">
                    <label style="height: 38px;line-height: 38px;">${po.word}</label>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">校对状态</label>
                <div class="layui-input-block">
                    <input type="hidden" id="proofread" value="${po.proofread }" >
                    <input type="radio" name="proofread" value="unchecked" title="未校对"  >
                    <input type="radio" name="proofread" value="checked" title="已校对" >
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">章节内容</label>
                <div class="layui-input-block">
                    <textarea id="content" name="content" placeholder="请输入章节内容" class="layui-textarea" rows="13">${po.content }</textarea>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/ebook/chapter/chapter_editor.js"></script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-20 10:06:04 -->

