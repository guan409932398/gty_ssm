layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});

        $.ajax({
            type: "post",
            url: ctx + "/chapter/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                top.layer.close(index);

                if (d.code != '200') {
                    top.layer.msg("(" + d.msg + ")");
                } else {
                    layer.closeAll("iframe");
                }
            },
            error: function () {
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        return false;
    });

    $("input[name=proofread][value=unchecked]").attr("checked", $('#proofread').val() == 'unchecked' || $('#proofread').val() == '' ? true : false);
    $("input[name=proofread][value=checked]").attr("checked", $('#proofread').val() == 'checked' ? true : false);

    layui.form.render('radio');

    //$('#select_sample').val($('#_select_sample').val());
    //layui.form.render('select');
})

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-06-20 10:06:04
