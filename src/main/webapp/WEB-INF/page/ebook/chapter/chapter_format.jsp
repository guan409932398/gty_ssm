<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>${po.bookid }格式调整</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
            <input type="hidden" id="id" name="id" value="${po.id }" >
            <input type="hidden" id="bookid" name="bookid" value="${po.bookid }" >

            <textarea id="content" name="content" placeholder="请输入章节内容" class="layui-textarea">${po.content }</textarea>

            <div class="layui-form-item" style="margin-top: 15px;">
                <div class="layui-input-block" style="text-align:center">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">提交</button>
                    &nbsp;&nbsp;
                    <button class="layui-btn" id="preview_but">预览</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/ebook/chapter/chapter_format.js"></script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-20 10:06:04 -->

