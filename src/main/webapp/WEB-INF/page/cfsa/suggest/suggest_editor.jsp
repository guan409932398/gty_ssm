<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, in itial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>

        <style type="text/css">
            .layui-form-label {width: 140px;}
            .layui-input-block {margin-left: 170px;}
        </style>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }" >
            <input type="hidden" id="opinionState" name="opinionState" value="${empty po.opinionState ? 'create' : po.opinionState}" >

            <div class="layui-collapse" style="width: 780px;">
                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">基本信息</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">标准名称<label style="color: red;">&nbsp;*&nbsp;</label></label>
                            <div class="layui-input-block">
                                <input type="text" id="name" class="layui-input" placeholder="请输入标准名称" name="name" value="${po.name }" lay-verify="required">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">制订或修订<label style="color: red;">&nbsp;*&nbsp;</label></label>
                            <div class="layui-input-block">
                                <input type="hidden" id="makeOrAmend" value="${po.makeOrAmend }" >
                                <input type="radio" name="makeOrAmend" value="make" title="制定" lay-filter="makeOrAmend">
                                <input type="radio" name="makeOrAmend" value="amend" title="修订" lay-filter="makeOrAmend">
                            </div>
                        </div>

                        <div class="layui-form-item" id="beRevisedStandardNo_show">
                            <label class="layui-form-label">被修订标准号</label>
                            <div class="layui-input-block">
                                <input type="text" id="beRevisedStandardNo" class="layui-input" placeholder="请输入被修订标准号" name="beRevisedStandardNo" value="${po.beRevisedStandardNo }">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">标准类别<label style="color: red;">&nbsp;*&nbsp;</label></label>
                            <div class="layui-input-block">
                                <input type="hidden" id="_standardCategory" value="${po.standardCategory }">

                                <select id="standardCategory" name="standardCategory" class="layui-select" lay-verify="required">
                                    <option value="">请选择标准类别</option>
                                    <c:choose>
                                        <c:when test="${!empty elfunc:getDic('cfsa_standard_category') and !empty elfunc:getDic('cfsa_standard_category').dics}">
                                            <c:forEach items="${elfunc:getDic('cfsa_standard_category').dics}" var="vpo" varStatus="num">
                                                <option value="${vpo.code}">（${vpo.remark }）${vpo.name }</option>
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">项目提出单位基本情况</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">单位名称</label>
                            <div class="layui-input-block">
                                <input type="hidden" id="draftingUnitId" name="draftingUnitId" value="${empty po.draftingUnitId ? company.id : po.draftingUnitId}">
                                <input type="hidden" id="draftingUnitName" name="draftingUnitName" value="${empty po.draftingUnitName ? company.realName : po.draftingUnitName}">
                                <label style="height: 38px;line-height: 38px;">${empty po.draftingUnitName ? company.realName : po.draftingUnitName}</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">地址</label>
                            <div class="layui-input-block">
                                <input type="hidden" id="draftingUnitAddress" name="draftingUnitAddress" value="${empty po.draftingUnitAddress ? company.address : po.draftingUnitAddress}">
                                <label style="height: 38px;line-height: 38px;">${empty po.draftingUnitAddress ? company.address : po.draftingUnitAddress}</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系人</label>
                            <div class="layui-input-block">
                                <input type="hidden" id="contactId" name="contactId" value="${empty po.contactId ? company.id : po.contactId}">
                                <input type="hidden" id="contactName" name="contactName" value="${empty po.contactName ? company.realName : po.contactName}">
                                <label style="height: 38px;line-height: 38px;">${empty po.contactName ? company.realName : po.contactName}</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系电话</label>
                            <div class="layui-input-block">
                                <input type="hidden" id="contactPhone" name="contactName" value="${empty po.contactPhone ? company.phone : po.contactPhone}">
                                <label style="height: 38px;line-height: 38px;">${empty po.contactPhone ? company.phone : po.contactPhone}</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">电子邮箱</label>
                            <div class="layui-input-block">
                                <input type="hidden" id="contactEmail" name="contactEmail" value="${empty po.contactEmail ? company.email : po.contactEmail}">
                                <label style="height: 38px;line-height: 38px;">${empty po.contactEmail ? company.email : po.contactEmail}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">候选起草单位</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">单位名称</label>
                            <div class="layui-input-block">
                                <input type="text" id="candidateUnitName" class="layui-input" placeholder="请输入候选起草单位_单位名称" name="candidateUnitName" value="${po.candidateUnitName }" lay-verify="required">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系人</label>
                            <div class="layui-input-block">
                                <input type="text" id="candidateUnitContactName" class="layui-input" placeholder="请输入候选起草单位_联系人" name="candidateUnitContactName" value="${po.candidateUnitContactName }" lay-verify="required">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系电话</label>
                            <div class="layui-input-block">
                                <input type="text" id="candidateUnitContactPhone" class="layui-input" placeholder="请输入候选起草单位_联系电话" name="candidateUnitContactPhone" value="${po.candidateUnitContactPhone }" lay-verify="required">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">项目详细信息</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">完成项目所需时限</label>
                            <div class="layui-input-block">
                                <input type="text" id="completeTime" class="layui-input" placeholder="完成项目所需时限" name="completeTime" value="${po.completeTime }">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">拟解决的<br/>食品安全问题</label>
                            <div class="layui-input-block">
                                <textarea id="readySolveFoodSafetyIssues" name="readySolveFoodSafetyIssues" placeholder="请输入拟解决的食品安全问题" class="layui-textarea" lay-verify="required">${po.readySolveFoodSafetyIssues }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">立项背景和理由</label>
                            <div class="layui-input-block">
                                <textarea id="backgroundAndReasons" name="backgroundAndReasons" placeholder="请输入立项背景和理由" class="layui-textarea" lay-verify="required">${po.backgroundAndReasons }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">主要技术指标<br/>已开展的风险监测<br/>和风险评估情况</label>
                            <div class="layui-input-block">
                                <textarea id="technicalIndicatorsRelatedInformation" name="technicalIndicatorsRelatedInformation" placeholder="请输入主要技术指标已开展的风险监测和风险评估情况" class="layui-textarea" lay-verify="required">${po.technicalIndicatorsRelatedInformation }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">标准范围<br/>和主要技术内容</label>
                            <div class="layui-input-block">
                                <textarea id="scopeAndContent" name="scopeAndContent" placeholder="请输入标准范围和主要技术内容" class="layui-textarea" lay-verify="required">${po.scopeAndContent }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">国际同类标准和国内<br/>相关法规标准情况</label>
                            <div class="layui-input-block">
                                <textarea id="internationalEquivalentInformation" name="internationalEquivalentInformation" placeholder="请输入国际同类标准和国内相关法规标准情况" class="layui-textarea" lay-verify="required">${po.internationalEquivalentInformation }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">成本预算（万元）</label>
                            <div class="layui-input-block">
                                <input type="text" id="costBudget" class="layui-input" placeholder="请输入项目成本预算" name="costBudget" value="${empty po.costBudget ? 0 : po.costBudget}" maxlength="16" lay-verify="number">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">经费使用计划</label>
                            <div class="layui-input-block">
                                <textarea id="expenditurePlan" name="expenditurePlan" placeholder="请输入经费使用计划" class="layui-textarea" lay-verify="required">${po.expenditurePlan}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-input-block" style="text-align: center;">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/cfsa/suggest/suggest_editor.js"></script>
        
        <script type="text/javascript">
            // 回车提交表单
            document.onkeydown = function (e) { 
                var theEvent = window.event || e; 
                var code = theEvent.keyCode || theEvent.which; 

                if (code == 13) { 
                    return false;
                } 
            }  
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-07-01 09:35:25 -->

