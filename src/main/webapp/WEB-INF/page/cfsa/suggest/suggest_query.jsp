<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>列表</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />
        
        <script>
            var ctx = "${ctx}";
        </script>
    </head>

    <body class="childrenBody">
        <blockquote class="layui-elem-quote list_search">
            <form class="layui-form">
                <div class="layui-inline">
                    <input type="text" id="name" class="layui-input" name="name" placeholder="请输入标准名称" value="">
                </div>

                <div class="layui-inline">
                    <select id="standardCategory" name="standardCategory" class="layui-select" >
                        <option value="">请选择标准类别</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('cfsa_standard_category') and !empty elfunc:getDic('cfsa_standard_category').dics}">
                                <c:forEach items="${elfunc:getDic('cfsa_standard_category').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">（${vpo.remark }）${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>

                <div class="layui-inline">
                    <input type="text" id="beginDate" class="layui-input" name="beginDate" placeholder="请输入提交开始日期" value="">
                </div>

                <div class="layui-inline">
                    <input type="text" id="endDate" class="layui-input" name="endDate" placeholder="请输入提交结束日期" value="">
                </div>

                <a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search"><i class="layui-icon">&#xe615;</i>查询</a>
                <a class="layui-btn layui-btn-normal add_btn"><i class="layui-icon">&#xe608;</i>添加</a>
                <a class="layui-btn layui-btn-normal download_btn layui-bg-red"><i class="layui-icon">&#xe601;</i>下载模版</a>
            </form>
        </blockquote>

        <!-- 数据表格 -->
        <table id="tableList" lay-filter="dt"></table>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/cfsa/suggest/suggest_query.js"></script>

        <script type="text/html" id="barEdit">
            <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
            <a class="layui-btn layui-btn-xs" lay-event="print">打印</a>
            <a class="layui-btn layui-btn-xs" lay-event="editor">修改</a>

            {{#  if(d.opinionState === 'create' || d.opinionState === 'back'){ }}
            <a class="layui-btn layui-btn-xs" lay-event="submit">提交</a>
            {{#  } }}

            {{#  if(d.opinionState === 'back'){ }}
            <a class="layui-btn layui-btn-xs" lay-event="back">退回原因</a>
            {{#  } }}

            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        </script>

        <script type="text/html" id="opinionStateTpl">
            {{#  if(d.opinionState === 'create'){ }}
            <span style="color: #5FB878;">新创建</span>
            {{#  } else if(d.opinionState === 'submit'){ }}
            <span style="color: #009688;">已提交</span>
            {{#  } else if(d.opinionState === 'project'){ }}
            <span style="color: #01AAED;">已立项</span>
            {{#  } else if(d.opinionState === 'back'){ }}
            <span style="color: #FFB800;">被退回</span>
            {{#  } else{ }}
            <span style="color: #FF5722;">未知状态</span>
            {{#  } }}
        </script>

        <script type="text/html" id="makeOrAmendTpl">
            {{#  if(d.makeOrAmend === 'make'){ }}
            <span style="color: #1E9FFF;">制定</span>
            {{#  } else if(d.makeOrAmend === 'amend'){ }}
            <span style="color: #009688;">修订</span>
            {{#  } else{ }}
            <span style="color: #FF5722;">未知状态</span>
            {{#  } }}
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-07-01 09:35:25 -->

