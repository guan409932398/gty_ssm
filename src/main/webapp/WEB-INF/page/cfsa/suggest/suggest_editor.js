layui.config({
    base : "js/"
}).use(['element','form','layer','jquery','laydate','upload'],function(){
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var element = layui.element;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;
    
    form.on("submit(submit_but)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        var msg="发生错误！",flag=false;

        $.ajax({
            type: "post",
            url: ctx + "/suggest/save",
            data:data.field,
            dataType:"json",
            async:false,
            success:function(d){
            	top.layer.close(index);
            	
            	if(d.code != '200'){
            		top.layer.msg("(" + d.msg + ")");
            	}else{
            		layer.closeAll("iframe");
            	}
			},
            error:function() {
                flag=true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        return false;
    });

    form.on('radio(makeOrAmend)', function (data) {
        if(data.value == 'make'){
            $('#beRevisedStandardNo_show').hide();
        } else {
            $('#beRevisedStandardNo_show').show();
        }
    });

    if($('#makeOrAmend').val() != 'amend'){
        $('#beRevisedStandardNo_show').hide();
    }

    $("input[name=makeOrAmend][value=make]").attr("checked", $('#makeOrAmend').val() == 'make' || $('#makeOrAmend').val() == '' ? true : false);
    $("input[name=makeOrAmend][value=amend]").attr("checked", $('#makeOrAmend').val() == 'amend' ? true : false);
    layui.form.render('radio');
    
    $('#standardCategory').val($('#_standardCategory').val());
	layui.form.render('select');
})

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-07-01 09:35:25
