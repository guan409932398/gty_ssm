<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>明细</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" type="text/css" href="${ctx }/plugins/Tags/css/tag.css" />
        <link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>

        <style type="text/css">
            .layui-form-label {width: 140px;}
            .layui-input-block {margin-left: 170px;}
            .layui-form-item .layui-inline {width: 33.333%; float: left; margin-right: 0;}
            .labStyle {height: 38px;line-height: 38px; }
        </style>
    </head>

    <body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
            <input type="hidden" id="id" class="layui-input" name="id" value="${po.id }" maxlength="32" >

            <div class="layui-collapse" style="width: 780px;">
                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">基本信息</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">标准名称</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.name }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">制订或修订</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.makeOrAmend eq 'make' ? '制订' : po.makeOrAmend eq 'amend' ? '修订' : '未知' }</label>
                            </div>
                        </div>

                        <c:if test="${po.makeOrAmend eq 'amend'}">
                            <div class="layui-form-item">
                                <label class="layui-form-label">被修订标准号</label>
                                <div class="layui-input-block">
                                    <label class="labStyle" >${po.beRevisedStandardNo }</label>
                                </div>
                            </div>
                        </c:if>

                        <div class="layui-form-item">
                            <label class="layui-form-label">标准类别</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.categoryName }</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">项目提出单位基本情况</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">单位名称</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.draftingUnitName }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">地址</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.draftingUnitAddress }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系人</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.contactName }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系电话</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.contactPhone }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">电子邮箱</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.contactEmail }</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">候选起草单位</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">单位名称</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.candidateUnitName }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系人</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.candidateUnitContactName }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">联系电话</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.candidateUnitContactPhone }</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-colla-item">
                    <h2 class="layui-colla-title">项目详细信息</h2>
                    <div class="layui-colla-content layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label">完成项目所需时限(年)</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.completeTime }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">拟解决的<br/>食品安全问题</label>
                            <div class="layui-input-block">
                                <textarea id="readySolveFoodSafetyIssues" name="readySolveFoodSafetyIssues" placeholder="请输入拟解决的食品安全问题" class="layui-textarea" lay-verify="required" readonly="readonly">${po.readySolveFoodSafetyIssues }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">立项背景和理由</label>
                            <div class="layui-input-block">
                                <textarea id="backgroundAndReasons" name="backgroundAndReasons" placeholder="请输入立项背景和理由" class="layui-textarea" lay-verify="required" readonly="readonly">${po.backgroundAndReasons }</textarea>
                    </div>
                </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">主要技术指标<br/>已开展的风险监测<br/>和风险评估情况</label>
                            <div class="layui-input-block">
                                <textarea id="technicalIndicatorsRelatedInformation" name="technicalIndicatorsRelatedInformation" placeholder="请输入主要技术指标已开展的风险监测和风险评估情况" class="layui-textarea" lay-verify="required" readonly="readonly">${po.technicalIndicatorsRelatedInformation }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">标准范围<br/>和主要技术内容</label>
                            <div class="layui-input-block">
                                <textarea id="scopeAndContent" name="scopeAndContent" placeholder="请输入标准范围和主要技术内容" class="layui-textarea" lay-verify="required" readonly="readonly">${po.scopeAndContent }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">国际同类标准和国内<br/>相关法规标准情况</label>
                            <div class="layui-input-block">
                                <textarea id="internationalEquivalentInformation" name="internationalEquivalentInformation" placeholder="请输入国际同类标准和国内相关法规标准情况" class="layui-textarea" lay-verify="required" readonly="readonly">${po.internationalEquivalentInformation }</textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">成本预算（万元）</label>
                            <div class="layui-input-block">
                                <label class="labStyle" >${po.costBudget }</label>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">经费使用计划</label>
                            <div class="layui-input-block">
                                <textarea id="expenditurePlan" name="expenditurePlan" placeholder="请输入经费使用计划" class="layui-textarea" lay-verify="required" readonly="readonly">${po.expenditurePlan}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>

        <script type="text/javascript">
            layui.config({
                base : "js/"
            }).use(['element','form','layer','jquery','laydate','upload'],function(){
                var $ = layui.jquery;
                var form = layui.form;
                var upload = layui.upload;
                var laydate = layui.laydate;
                var element = layui.element;
                var layer = parent.layer === undefined ? layui.layer : parent.layer;
            })
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-07-01 09:35:25 -->