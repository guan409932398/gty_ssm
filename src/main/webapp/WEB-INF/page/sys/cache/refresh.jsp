<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>配置维护</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		
		<script>  
	        var ctx = "${ctx}";  
	    </script>  
	    
		<style type="text/css">
			.layui-form-item .layui-inline {
				width: 33.333%;
				float: left;
				margin-right: 0;
			}
		</style>
	</head>
	
	<body class="childrenBody">
		<form class="layui-form" style="width: 80%;" id="this_form">
			<div class="layui-form-item" style="margin-top: 50px;">
				<label class="layui-form-label">配置缓存</label>
				<div class="layui-input-block">
					<input type="text" id="conf_code" class="layui-input" placeholder="请输入更新的配置代码，不输入则更新所有。" name="conf_code" value="" style="width: 350px;;float: left;">
					
					<button class="layui-btn" style="float: left;margin-left:20px;width: 80px;" id="conf_refresh">
						<i class="layui-icon">&#xe9aa;</i> 刷新
					</button>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">字典缓存</label>
				<div class="layui-input-block">
					<input type="text" id="dic_code" class="layui-input" placeholder="请输入更新的字典代码，不输入则更新所有。" name="dic_code" value="" style="width: 350px;;float: left;">
					
					<button class="layui-btn" style="float: left;margin-left:20px;width: 80px;" id="dic_refresh">
						<i class="layui-icon">&#xe9aa;</i> 刷新
					</button>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript">
			layui.config({
				base : "js/"
			}).use(['jquery','layer'], function(args){
				var $ = layui.jquery;
				var layer = parent.layer === undefined ? layui.layer : parent.layer;
				
				$("#conf_refresh").click(function(){
					var index = layer.msg('配置缓存刷新中，请稍候',{icon: 16,time:false,shade:0.8});
					
					$.post(
						ctx + '/config/refresh',
						{code: $('#conf_code').val() },
				    	function (result) {
							layer.closeAll();
							layer.alert(result.data);
						}
					);
				});
				
				$("#dic_refresh").click(function(){
					var index = layer.msg('字典缓存刷新中，请稍候',{icon: 16,time:false,shade:0.8});
					
					$.post(
						ctx + '/dictionary/refresh',
						{code: $('#dic_code').val() },
				    	function (result) {
							layer.closeAll();
							layer.alert(result.data);
						}
					);
				});
			});
		</script>
	</body>
</html>