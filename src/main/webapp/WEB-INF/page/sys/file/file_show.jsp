<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>file show</title>
		
		<script>
            var ctx = "${ctx}";
        </script>
	</head>

	<body>
		<img src="${ctx }/file/download?file_id=${file_id }">
	</body>
</html>