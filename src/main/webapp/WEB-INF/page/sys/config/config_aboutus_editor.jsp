<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta charset="utf-8">
        <title>规则维护</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>

        <style type="text/css">
            .layui-form-item .layui-inline {
                width: 33.333%;
                float: left;
                margin-right: 0;
            }
        </style>
    </head>

    <body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
            <input type="hidden" id="id" name="id" value="${po.id }">
            <input type="hidden" id="remark" name="remark" value="${po.remark }">
            <input type="hidden" id="name" name="name" value="${po.name }">
            <input type="hidden" id="code" name="code" value="${po.code }">
            <input type="hidden" id="type" name="type" value="about">

            <div class="layui-form-item">
                <label class="layui-form-label">配置名称</label>
                <div class="layui-input-block">
                    <label style="height: 38px;line-height: 38px;">${po.name }</label>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">规则代码</label>
                <div class="layui-input-block">
                    <label style="height: 38px;line-height: 38px;">${po.code }</label>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">规则值</label>
                <div class="layui-input-block">
                    <textarea id="values" name="values" placeholder="请输入规则值" class="layui-textarea">${po.values }</textarea>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/sys/config/config_aboutus_editor.js"></script>
    </body>
</html>