var $;
var $form;
var form;
layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate'], function () {
    var layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage, laydate = layui.laydate;
    $ = layui.jquery;
    form = layui.form;

    form.on('switch(switchConf)', function (data) {
        if (data.elem.checked) {
            $('#values').val('open');
        } else {
            $('#values').val('close');
        }

        return false;
    });

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/config/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                msg = d.msg;
            },
            error: function () {
                flag = true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        setTimeout(function () {
            if (!flag) {
                top.layer.close(index);
                top.layer.msg(msg);
                layer.closeAll("iframe");
            }
        }, 1000);

        return false;
    })
})