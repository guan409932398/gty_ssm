layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl'], function () {
    var form = layui.form, table = layui.table;
    layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    active = {
        search: function () {
            var code = $("#code");
            var name = $("#name");

            table.reload('tableList', {
                page: {
                    curr: 1
                },
                where: {
                    code: code.val(),
                    name: name.val()
                }
            });
        }
    };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/config/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'name', title: '配置名称'},
            {field: 'code', title: '配置代码'},
            {field: 'values', title: '配置值'},
            {title: '操作', toolbar: '#barEdit'}
        ]],
        page: true,
        where: {
            type: 'about',
            timestamp: (new Date()).valueOf()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(test)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            layer.open({
                type: 2,
                title: "规则维护",
                area: ['750px', '600px'],
                content: ctx + "/config/goAboutUsEditor?id=" + data.id,
                end: function () {
                    table.reload('tableList', {});
                }
            })
        }
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    })
})
