layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl'], function () {
    var $ = layui.jquery;
    var laypage = layui.laypage;
    var form = layui.form, table = layui.table;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    active = {
        search: function () {
            var code = $("#code");
            var name = $("#name");
            var category = $("#category");

            table.reload('tableList', {
                page: {
                    curr: 1
                },
                where: {
                    code: code.val(),
                    name: name.val(),
                    category: category.val()
                }
            });
        }
    };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/config/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'name', title: '配置名称'},
            {field: 'code', title: '配置代码'},
            {field: 'values', title: '配置值'},
            {field: 'remark', title: '备注'},
            {title: '操作', toolbar: '#barEdit'}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf(),
            type: 'config'
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(tableList)', function (obj) {
        var data = obj.data;

        if (obj.event === 'del') {
            layer.confirm('该功能为系统支撑功能，如非专业人员请勿操作。', function (index) {
                $.ajax({
                    url: ctx + '/config/del?code=' + data.code,
                    type: "post",
                    success: function (d) {
                        if (d.code == 200) {
                            $(".search_btn").click();
                        } else {
                            layer.msg("权限不足，联系超管！", {icon: 5});
                        }
                    }
                });

                layer.close(index);
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2,
                title: "配置信息维护",
                area: ['750px', '600px'],
                content: ctx + "/config/goEditor?id=" + data.id,
                end: function () {
                    table.reload('tableList', {});
                }
            })
        }
    });

    //添加配置
    $(".add_btn").click(function () {
        layer.open({
            title: "添加配置信息",
            type: 2,
            area: ['800px', '600px'],
            content: ctx + "/config/goAdd",
            end: function () {
                table.reload('tableList', {});
            }
        })
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})
