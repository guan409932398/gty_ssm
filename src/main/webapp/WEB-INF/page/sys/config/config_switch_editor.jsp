<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>配置维护</title>
		
		<meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
		<link rel="stylesheet" href="${ctx }/css/base.css" />
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		
		<script>  
	        var ctx = "${ctx}";  
	    </script>  
	</head>
	
	<body class="childrenBody">
		<form class="layui-form" style="width: 80%;" id="this_form">
			<input type="hidden" id="id" name="id" value="${po.id }">
			<input type="hidden" id="type" name="type" value="switch">
			<input type="hidden" id="category" name="category" value="">
		
			<div class="layui-form-item">
				<label class="layui-form-label">开关名称</label>
				<div class="layui-input-block">
					<input type="text" id="name" class="layui-input" lay-verify="required" placeholder="请输入开关名称" name="name" value="${po.name }">
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">开关代码</label>
				<div class="layui-input-block">
					<c:if test="${empty po.code}">
						<input type="text" id="code" class="layui-input" lay-verify="required" placeholder="请输入开关代码" name="code" value="${po.code }">
					</c:if>
					<c:if test="${not empty po.code}">
						<input type="hidden" id="code" name="code" value="${po.code }">
						<label style="height: 38px;line-height: 38px;">${po.code}</label>
					</c:if>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">开关状态</label>
				<div class="layui-input-block">
					<input type="hidden" id="values" name="values" value="${po.values }">
					<input type="checkbox" name="_values" lay-skin="switch" lay-filter="switchConf" lay-text="开启|关闭" value="open" ${po.values eq 'open' ? 'checked' : '' }>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea id="remark" name="remark" placeholder="请输入备注" class="layui-textarea">${po.remark }</textarea>
				</div>
			</div>
			
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript" src="${ctx }/page/sys/config/config_switch_editor.js"></script>
	</body>
</html>