layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var laypage = layui.laypage, laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/config/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                msg = d.msg;
            },
            error: function () {
                flag = true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        setTimeout(function () {
            if (!flag) {
                top.layer.close(index);
                top.layer.msg(msg);
                layer.closeAll("iframe");
            }
        }, 1000);

        return false;
    });

    $('#category').val($('#_category').val());
    layui.form.render('select');
})