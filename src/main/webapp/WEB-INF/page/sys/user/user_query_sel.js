layui.config({
    base: "js/"
}).use(['layer', 'jquery', 'table', 'laytpl'], function () {
    var $ = layui.jquery;
    var table = layui.table;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    active = {
        search: function () {
            var account = $('#account');
            var nickname = $('#nickname');
            var phone = $('#phone');
            var state = $('#state');

            table.reload('tableList', {
                page: {
                    curr: 1
                },
                where: {
                    account: account.val(),
                    nickname: nickname.val(),
                    phone: phone.val(),
                    state: state.val()
                }
            });
        }
    };

    // 数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/user/queryJoinRole',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'account', title: '用户帐号', width: 120},
            {field: 'nickname', title: '用户昵称'},
            {field: 'phone', title: '手机号', width: 120},
            {field: 'state', title: '用户状态', templet: '#stateTpl', width: 95},
            {field: 'roleName', title: '用户角色', width: 110},
            {
                field: 'createTime',
                title: '创建时间',
                templet: '<div>{{ formatTime(d.createTime,"yyyy-MM-dd")}}</div>',
                width: 110
            },
            {title: '操作', toolbar: '#barEdit', align: 'center'}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf(),
            index: $('#index').val(),
            show_field: $('#show_field').val(),
            value_field: $('#value_field').val()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    // 监听工具条
    table.on('tool(dt)', function (obj) {
        var data = obj.data;

        if (obj.event === 'sel') {
            var index = parent.layer.getFrameIndex(window.name);

            $("#layui-layer-iframe" + $('#index').val(), parent.document).contents().find('#' + $('#valueField').val()).val(data.id);
            $("#layui-layer-iframe" + $('#index').val(), parent.document).contents().find('#' + $('#showField').val()).val(data.nickname);

            parent.layer.close(index);
        }
    });

    // 查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})

//格式化时间
function formatTime(UNIX_timestamp, fmt) {
    if (UNIX_timestamp == '' || UNIX_timestamp == null) {
        return '';
    }

    var datetime = new Date(UNIX_timestamp * 1000);

    if (parseInt(datetime) == datetime) {
        if (datetime.length == 10) {
            datetime = parseInt(datetime) * 1000;
        } else if (datetime.length == 13) {
            datetime = parseInt(datetime);
        }
    }

    datetime = new Date(datetime);

    var o = {
        "M+": datetime.getMonth() + 1,                 //月份
        "d+": datetime.getDate(),                    //日
        "h+": datetime.getHours(),                   //小时
        "m+": datetime.getMinutes(),                 //分
        "s+": datetime.getSeconds(),                 //秒
        "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
        "S": datetime.getMilliseconds()             //毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

// @author 官天野
// @email guan409932398@qq.com
// @date 2019-03-12 16:18:09
