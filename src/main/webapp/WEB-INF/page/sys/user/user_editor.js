layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/user/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                top.layer.close(index);

                if (d.code != '200') {
                    top.layer.msg("(" + d.msg + ")");
                } else {
                    layer.closeAll("iframe");
                }
            },
            error: function () {
                flag = true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        return false;
    });

    // 用户头像
    upload.render({
        elem: '#user_image_upload_but',
        url: ctx + '/file/upload',
        exts: 'jpg|jpeg|bmp|png',
        size: 10240,
        before: function (obj) {
            layer.load(2, {shade: [0.8]});
        }, done: function (res) {
            layer.closeAll('loading');

            if (res.code == 0) {
                $('#image').val(res.data);

                var oss_service_providers = $('#oss_service_providers').val();

                if (oss_service_providers == 'aliyun') {
                    $('#user_image_show').html('<a href="' + $('#alioss_outLink').val() + res.data + '" target="_blank" title="点击查看大图"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
                } else if (oss_service_providers == 'qiniu') {
                    $('#user_image_show').html('<a href="' + $('#qiniu_outLink').val() + res.data + '" target="_blank" title="点击查看大图"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
                } else if (oss_service_providers == 'server') {
                    $('#user_image_show').html('<a href="' + ctx + "/file/show?file_id=" + res.data + '" target="_blank" title="点击查看大图"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
                }
            } else {
                return layer.msg('上传失败');
            }
        }, error: function () {
            layer.closeAll('loading');
            return layer.msg('上传失败');
        }
    });

    $("input[name=state][value=active]").attr("checked", $('#_state').val() == 'active' || $('#_state').val() == '' ? true : false);
    $("input[name=state][value=banned]").attr("checked", $('#_state').val() == 'banned' ? true : false);

    $("input[name=gender][value=man]").attr("checked", $('#_gender').val() == 'man' || $('#_gender').val() == '' ? true : false);
    $("input[name=gender][value=woman]").attr("checked", $('#_gender').val() == 'woman' ? true : false);
    layui.form.render('radio');

    $('#userRole').val($('#_userRole').val());
    layui.form.render('select');
})

// @author 官天野
// @email guan409932398@qq.com
// @date 2019-03-12 16:18:09
