<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>明细</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" type="text/css" href="${ctx }/plugins/Tags/css/tag.css" />
        <link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>

        <style type="text/css">
            .layui-form-item .layui-inline {width: 33.333%; float: left; margin-right: 0;}
            .labStyle {height: 38px;line-height: 38px; }
        </style>

    </head>

    <body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
            <input type="hidden" id="id" class="layui-input" name="id" value="${po.id }" maxlength="32" >

            <div class="layui-form-item">
                <label class="layui-form-label">帐号</label>
                <div class="layui-input-block">
                	<label class="labStyle" >${po.account }</label>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-block">
                	<label class="labStyle" >${po.phone }</label>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">昵称</label>
                <div class="layui-input-block">
                	<label class="labStyle" >${po.nickname }</label>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                	<c:if test="${po.state eq 'active' }">
                		<label class="labStyle" style="color: #00CC00;">正常</label>
                	</c:if>
                	
                	<c:if test="${po.state eq 'banned' }">
                		<label class="labStyle" style="color: #FF0000;">封禁</label>
                	</c:if>
                	
                	<c:if test="${po.state ne 'active' and po.state ne 'banned' }">
                		<label class="labStyle" style="color: #080808;">未知</label>
                	</c:if>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">头像</label>
                <div class="layui-input-block" style="max-width: 650px;">
                	<c:if test="${not empty po.image }">
                		<c:if test="${elfunc:getCon('oss_service_providers') eq 'server' }">
                			<img src="${ctx }/file/download?file_id=${po.image }" style="width: auto;height: auto;max-width: 100%;max-height: 100%;">
                		</c:if>
                		
                		<c:if test="${elfunc:getCon('oss_service_providers') eq 'qiniu' }">
                			<img src="${elfunc:getCon('qiniu_outLink')}${po.image }" style="width: auto;height: auto;max-width: 100%;max-height: 100%;">
                		</c:if>
                		
                		<c:if test="${elfunc:getCon('oss_service_providers') eq 'aliyun' }">
                			<img src="${elfunc:getCon('alioss_outLink')}${po.image }" style="width: auto;height: auto;max-width: 100%;max-height: 100%;">
                		</c:if>
                	</c:if>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">年龄(性别)</label>
                <div class="layui-input-block">
                	<label class="labStyle" >${empty po.age ? '保密' : '' }&nbsp;&nbsp;（${po.gender eq 'man' ? '男' :  po.gender eq 'woman' ? '女' : '保密' }）</label>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">简介</label>
                <div class="layui-input-block">
                	<textarea id="desc" name="desc" class="layui-textarea" rows="3" readonly>${po.desc }</textarea>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @email guan409932398@qq.com -->
<!-- @date 2019-03-12 16:18:09 -->
