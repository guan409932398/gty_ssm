<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
        
        <style type="text/css">
            .labStyle {height: 38px;line-height: 38px; }
        </style>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
       		<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }">
            <input type="hidden" id="password" name="password" value="${po.password }">
            <input type="hidden" id="image" name="image" value="${po.image }" >

            <input type="hidden" id="_userRole" value="${po.userRole }" >
            <input type="hidden" id="_type" value="${po.type }" >
            <input type="hidden" id="_state" value="${po.state }" >
            <input type="hidden" id="_gender" value="${po.gender }" >

            <input type="hidden" id="back" name="back" value="back" >
            
            <div class="layui-form-item">
                <label class="layui-form-label">帐号</label>
                <div class="layui-input-block">
                	<c:if test="${empty po.id }">
                		<label class="labStyle" >保存时自动生成</label>
                	</c:if>
                	
                	<c:if test="${not empty po.id }">
                		<label class="labStyle" >${po.account }</label>
                	</c:if>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">角色</label>
                <div class="layui-input-block">
                	<select id="userRole" name="userRole" class="layui-select" lay-verify="required">
						<option value="">请选择用户角色</option>
						<c:choose>
							<c:when test="${!empty roles }">
								<c:forEach items="${roles }" var="vpo" varStatus="num">
									<option value="${vpo.id}">${vpo.name }</option>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<label style="height: 38px;line-height: 38px;">没有可选的角色</label>
							</c:otherwise>
						</c:choose>
					</select>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-block">
                	<c:if test="${empty po.id }">
                		<input type="text" id="phone" class="layui-input" placeholder="请输入用户手机号" name="phone" value="${po.phone }" lay-verify="required">
                	</c:if>
                	
                	<c:if test="${not empty po.id }">
                		<label class="labStyle" >${po.phone }</label>
                	</c:if>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">昵称</label>
                <div class="layui-input-block">
                	<input type="text" id="nickname" class="layui-input" placeholder="请输入用户昵称" name="nickname" value="${po.nickname }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                	<input type="radio" name="state" value="active" title="正常" >
                	<input type="radio" name="state" value="banned" title="封禁" >
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">头像</label>
                <div class="layui-input-block">
                	<button type="button" class="layui-btn layui-bg-blue" id="user_image_upload_but" style="display: inline;">
						<i class="layui-icon">&#xe67c;</i> 上传图片
					</button>
					
                	<div class="layui-upload-list" id="user_image_show" style="display: inline;">
						<c:if test="${not empty po.image }">
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'server' }">
								<a href="${ctx }/file/show?file_id=${po.image }" target="_blank" title="点击查看大图">
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
							
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'qiniu' }">
								<a href="${elfunc:getCon('qiniu_outLink')}${po.image }" target="_blank" title="点击查看大图">
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
							
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'aliyun' }">
								<a href="${elfunc:getCon('alioss_outLink')}${po.image }" target="_blank" title="点击查看大图">
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
						</c:if>
					</div>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">年龄</label>
                <div class="layui-input-block">
                	<input type="text" id="age" class="layui-input" placeholder="请输入年龄" name="age" value="${po.age }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">性别</label>
                <div class="layui-input-block">
                	<input type="radio" name="gender" value="man" title="男"  >
                	<input type="radio" name="gender" value="woman" title="女" >
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">简介</label>
                <div class="layui-input-block">
                    <textarea id="desc" name="desc" placeholder="请输入简介" class="layui-textarea">${po.desc }</textarea>
                </div>
            </div>
            
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/sys/user/user_editor.js"></script>
        
        <script type="text/javascript">
            // 回车提交表单
            document.onkeydown = function (e) { 
                var theEvent = window.event || e; 
                var code = theEvent.keyCode || theEvent.which; 

                if (code == 13) { 
                    return false;
                } 
            }  
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @email guan409932398@qq.com -->
<!-- @date 2019-03-12 16:18:09 -->

