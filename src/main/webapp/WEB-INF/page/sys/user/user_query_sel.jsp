<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>列表</title>

		<meta charset="utf-8">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />

		<script>
			var ctx = "${ctx}";
		</script>
	</head>

	<body class="childrenBody">
		<blockquote class="layui-elem-quote list_search">
			<form class="layui-form">
				<input id="index" name="index" type="hidden" value="${entity.index }">
				<input id="showField" name="showField" type="hidden" value="${entity.showField }">
				<input id="valueField" name="valueField" type="hidden" value="${entity.valueField }">

				<div class="layui-inline">
					<input type="text" id="account" class="layui-input" name="account" placeholder="请输入用户帐号" value="">
				</div>

				<div class="layui-inline">
					<input type="text" id="nickname" class="layui-input" name="nickname" placeholder="请输入用户昵称" value="">
				</div>

				<div class="layui-inline">
					<input type="text" id="phone" class="layui-input" name="phone" placeholder="请输入用户手机号" value="">
				</div>

				<div class="layui-inline">
					<select id="state" name="state" class="layui-select" >
						<option value="">请选择用户状态</option>
						<option value="active">正常</option>
						<option value="banned">封禁</option>
					</select>
				</div>

				<a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search"><i class="layui-icon">&#xe615;</i>查询</a>
			</form>
		</blockquote>

		<!-- 数据表格 -->
		<table id="tableList" lay-filter="dt"></table>

		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript" src="${ctx }/page/sys/user/user_query_sel.js"></script>

		<script type="text/html" id="barEdit">
			<a class="layui-btn layui-btn-xs" lay-event="sel">选择</a>
		</script>

		<script type="text/html" id="stateTpl">
			{{#  if(d.state === 'active'){ }}
			<span style="color: #00FF00;">正常</span>
			{{#  } else if(d.state === 'banned'){ }}
			<span style="color: #FF0000;">封禁</span>
			{{#  } else{ }}
			<span style="color: #FFFF00;">未知</span>
			{{#  } }}
		</script>
	</body>
</html>

<!-- @author 官天野 -->
<!-- @email guan409932398@qq.com -->
<!-- @date 2019-03-12 16:18:09 -->

