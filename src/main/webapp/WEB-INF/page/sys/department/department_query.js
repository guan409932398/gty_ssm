layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl', 'laydate'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var table = layui.table;
    var laypage = layui.laypage;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    active = {
        search: function () {
            // var input_sample = $('#input_sample');

            table.reload('tableList', {
                page: {
                    curr: 1
                },
                where: {
                    // input_sample: input_sample.val()
                }
            });
        }
    };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/department/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {
                field: 'createTime',
                title: '创建时间',
                templet: '<div>{{ formatTime(d.createTime,"yyyy-MM-dd")}}</div>',
                width: 110
            },
            {title: '操作', toolbar: '#barEdit', align: 'center'}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(dt)', function (obj) {
        var data = obj.data;

        if (obj.event === 'del') {
            layer.confirm('删除后无法恢复，确认删除吗？', function (index) {
                $.ajax({
                    url: ctx + '/department/del?id=' + data.id,
                    type: "post",
                    success: function (result) {
                        if (result.code == 200) {
                            $(".search_btn").click();
                        } else {
                            layer.msg(result.msg, {icon: 5});
                        }
                    }
                });

                layer.close(index);
            });
        } else if (obj.event === 'editor') {
            layer.open({
                type: 2,
                title: "信息维护",
                area: ['800px', '600px'],
                content: ctx + "/department/goEditor?id=" + data.id,
                end: function () {
                    table.reload('tableList', {});
                }
            });
        } else if (obj.event === 'detail') {
            layer.open({
                type: 2,
                title: "信息明细",
                area: ['800px', '600px'],
                content: ctx + "/department/goDetail?id=" + data.id
            });
        }
    });

    //添加
    $(".add_btn").click(function () {
        layer.open({
            title: "信息新增",
            type: 2,
            area: ['800px', '600px'],
            content: ctx + "/department/goAdd",
            end: function () {
                table.reload('tableList', {});
            }
        });
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})

//格式化时间
function formatTime(UNIX_timestamp, fmt) {
    if (UNIX_timestamp == '' || UNIX_timestamp == null) {
        return '';
    }

    var datetime = new Date(UNIX_timestamp * 1000);

    if (parseInt(datetime) == datetime) {
        if (datetime.length == 10) {
            datetime = parseInt(datetime) * 1000;
        } else if (datetime.length == 13) {
            datetime = parseInt(datetime);
        }
    }

    datetime = new Date(datetime);

    var o = {
        "M+": datetime.getMonth() + 1,                 //月份
        "d+": datetime.getDate(),                    //日
        "h+": datetime.getHours(),                   //小时
        "m+": datetime.getMinutes(),                 //分
        "s+": datetime.getSeconds(),                 //秒
        "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
        "S": datetime.getMilliseconds()             //毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }

    return fmt;
}

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 2019-06-26 11:12:00
