layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    //编辑角色ID
    var id = $("#id").val();

    var xtree = new layuiXtree({
        elem: 'xtree',  //(必填) 放置xtree的容器id，不带#号
        form: form,    //(必填) layui 的 from
        data: ctx + '/menus/queryXtree?roleId=' + id + '&timestamp=' + (new Date()).valueOf(), //(必填) 数据接口，需要返回指定结构json字符串
        ckall: true,   //启动全选功能，默认false
        isopen: false,
        ckallback: function () {
            //全选框状态改变后执行的回调函数
        }
    });

    form.on("submit(submit_but)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        var menus = "";
        var list = xtree.GetChecked();

        for (var i = 0; i < list.length; i++) {
            menus += list[i].value + ",";

            if (xtree.GetParent(list[i].value) != null) {
                menus += xtree.GetParent(list[i].value).value + ",";

                if (xtree.GetParent(xtree.GetParent(list[i].value).value) != null) {
                    menus += xtree.GetParent(xtree.GetParent(list[i].value).value).value + ",";
                }
            }
        }

        // 去除字符串末尾的‘,’
        menus = menus.substring(0, menus.length - 1);

        // 将菜单内容写入隐藏域
        data.field.menuIds = menus;

        $.ajax({
            type: "post",
            url: ctx + "/roles/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                top.layer.close(index);

                if (d.code != '200') {
                    top.layer.msg("(" + d.msg + ")");
                } else {
                    layer.closeAll("iframe");
                }
            },
            error: function () {
                flag = true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        return false;
    });
})

// @author 官天野
// @version v1.0
// @email: guan409932398@qq.com
// @date 20190611
