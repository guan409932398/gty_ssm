layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl', 'laydate', "treeGrid"], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var table = layui.table;
    var laypage = layui.laypage;
    var laydate = layui.laydate;
    var treeGrid = layui.treeGrid;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    var treeTable = treeGrid.render({
        id: 'treeTable',
        elem: '#treeTable',
        url: ctx + '/menus/query',
        cellMinWidth: 100,
        treeId: 'id', //树形id字段名称
        treeUpId: 'parentId', //树形父id字段名称
        treeShowName: 'title', //以树形式显示的字段
        cols: [[
            {field: 'id', title: ' ', templet: "#radioTpl", unresize: true},
            {field: 'title', title: '菜单名'},
            {field: 'icon', title: '图标', templet: '#iconTpl'},
            {field: 'href', title: '链接'},
            {field: 'perms', title: '权限标识'},
            {field: 'sorting', title: '排序'}
        ]],
        page: false,
        response: {
            statusCode: 200
        }
    });

    //添加
    $("#add_btn").click(function () {
        var menuid = $("input[name='id']:checked").val();

        if (menuid == undefined) {
            menuid = 'GTY-QQ:409932398';
        }

        layer.open({
            title: "信息新增",
            type: 2,
            area: ['800px', '600px'],
            content: ctx + "/menus/goAdd?parentId=" + menuid,
            end: function () {
                treeGrid.reload("treeTable", {})
            }
        });
    });

    //修改
    $("#edit_btn").click(function () {
        var menuid = $("input[name='id']:checked").val();

        if (menuid == undefined) {
            layer.msg("请选择要操作的菜单！", {icon: 5});
            return;
        }

        layer.open({
            title: "信息新增",
            type: 2,
            area: ['800px', '600px'],
            content: ctx + "/menus/goEditor?id=" + menuid,
            end: function () {
                treeGrid.reload("treeTable", {})
            }
        });
    });

    //删除
    $("#del_btn").click(function () {
        var menuid = $("input[name='id']:checked").val();

        if (menuid == undefined) {
            layer.msg("请选择要操作的菜单！", {icon: 5});
            return;
        }

        layer.confirm('删除后无法恢复，请谨慎操作', function (index) {
            $.ajax({
                url: ctx + '/menus/del?id=' + menuid,
                type: "post",
                success: function (d) {
                    if (d.code == 200) {
                        layer.msg("删除成功！", {icon: 1});
                        treeGrid.reload("treeTable", {});
                    } else {
                        layer.msg(d.msg, {icon: 5});
                    }
                }
            });
        });
    });
})

// 格式化时间
function formatTime(datetime, fmt) {
    if (datetime == null || datetime == 0) {
        return "";
    }

    if (parseInt(datetime) == datetime) {
        if (datetime.length == 10) {
            datetime = parseInt(datetime) * 1000;
        } else if (datetime.length == 13) {
            datetime = parseInt(datetime);
        }
    }

    datetime = new Date(datetime);

    var o = {
        "M+": datetime.getMonth() + 1,                 // 月份
        "d+": datetime.getDate(),                    // 日
        "h+": datetime.getHours(),                   // 小时
        "m+": datetime.getMinutes(),                 // 分
        "s+": datetime.getSeconds(),                 // 秒
        "q+": Math.floor((datetime.getMonth() + 3) / 3), // 季度
        "S": datetime.getMilliseconds()             // 毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }

    return fmt;
}

// @author 官天野
// @qq 409932398
// @email guan409932398@qq.com
// @phone 18510113058
// @date 2019-03-13 10:31:27
