<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>列表</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />
        <link rel="stylesheet" href="${ctx }/css/base.css">
        
        <script>
            var ctx = "${ctx}";
        </script>
    </head>

    <body class="childrenBody">
        <blockquote class="layui-elem-quote list_search">
            <div class="layui-btn-group TableTools" style="margin-left: 10px">
				<button class="layui-btn" id="add_btn">添加菜单</button>
				<button class="layui-btn" id="edit_btn">编辑菜单</button>
				<button class="layui-btn layui-btn-danger" id="del_btn">删除菜单</button>
				<button class="layui-btn layui-btn-primary">（不选中为添加顶级菜单，选中添加子菜单）</button>
			</div>
        </blockquote>

        <!-- tree grid -->
        <table class="layui-hidden" id="treeTable" lay-filter="treeTable"></table>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/sys/menus/menus_query.js"></script>
        
        <script type="text/html" id="iconTpl">
			{{#  if(d.icon === null){ }}
   			
			{{#  } else{ }}
				<i class="layui-icon">{{ d.icon }}</i>  
			{{#  } }}
		</script>
		
		<script type="text/html" id="radioTpl">
			{{#  if(d.id != '1'){ }}
				<input type="radio" name="id" value="{{d.id}}" title=" " lay-filter="radiodemo">
			{{#  } }}
		</script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @qq 409932398 -->
<!-- @email guan409932398@qq.com -->
<!-- @phone 18510113058 -->
<!-- @date 2019-03-13 10:31:27 -->

