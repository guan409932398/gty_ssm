<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }" >
            <input type="hidden" id="spread" name="spread" value="${po.spread }" >
            <input type="hidden" id="parentId" name="parentId" value="${empty po.parentId ? 'GTY-QQ:409932398' : po.parentId }" >
            
            <div class="layui-form-item">
                <label class="layui-form-label">父节点</label>
                <div class="layui-input-block">
                	<label style="height: 38px;line-height: 38px;">${empty po.parentName || po.parentName eq '' ? '顶级菜单' : po.parentName}</label>
                </div>
            </div>
			
            <div class="layui-form-item">
                <label class="layui-form-label">菜单名</label>
                <div class="layui-input-block">
                	<input type="text" id="title" class="layui-input" placeholder="请输入菜单名" name="title" value="${po.title }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">图标</label>
                <div class="layui-input-block">
                	<input type="text" id="icon" class="layui-input" placeholder="请输入图标" name="icon" value="${po.icon }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">资源地址</label>
                <div class="layui-input-block">
                	<input type="text" id="href" class="layui-input" placeholder="请输入资源地址" name="href" value="${po.href }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">排序号</label>
                <div class="layui-input-block">
                	<input type="text" id="sorting" class="layui-input" placeholder="请输入排序号" name="sorting" value="${po.sorting }" maxlength="20" >
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">权限标识</label>
                <div class="layui-input-block">
                    <textarea id="perms" name="perms" placeholder="请输入权限标识" class="layui-textarea">${po.perms }</textarea>
                </div>
            </div>
            
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/sys/menus/menus_editor.js"></script>
        
        <script type="text/javascript">
            // 回车提交表单
            document.onkeydown = function (e) { 
                var theEvent = window.event || e; 
                var code = theEvent.keyCode || theEvent.which; 

                if (code == 13) { 
                    return false;
                } 
            }  
        </script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @qq 409932398 -->
<!-- @email guan409932398@qq.com -->
<!-- @phone 18510113058 -->
<!-- @date 2019-03-13 10:31:27 -->

