layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl'], function () {
    var form = layui.form, table = layui.table;
    layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery,
        active = {
            search: function () {
                var name = $("#name");
                var code = $("#code");
                var upperId = $('#upperId');

                table.reload('tableList', {
                    page: {
                        curr: 1
                    },
                    where: {
                        name: name.val(),
                        code: code.val(),
                        upperId: upperId.val()
                    }
                });
            }
        };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/dictionary/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'name', title: '字典名称'},
            {field: 'code', title: '字典代码'},
            {title: '操作', toolbar: '#barEdit'}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf(),
            index: $('#index').val(),
            show_field: $('#show_field').val(),
            value_field: $('#value_field').val(),
            upperId: $('#upperId').val()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(test)', function (obj) {
        var data = obj.data;

        if (obj.event === 'sel') {
            var index = parent.layer.getFrameIndex(window.name);

            $("#layui-layer-iframe" + $('#index').val(), parent.document).contents().find('#' + $('#valueField').val()).val(data.code);
            $("#layui-layer-iframe" + $('#index').val(), parent.document).contents().find('#' + $('#showField').val()).val(data.name);

            parent.layer.close(index);
        }
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
});