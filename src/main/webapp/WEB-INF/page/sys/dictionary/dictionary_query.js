layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl'], function () {
    var form = layui.form, table = layui.table;
    layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery,
        active = {
            search: function () {
                var name = $("#name");
                var code = $("#code");
                var upperId = $('#upperId');

                table.reload('tableList', {
                    page: {
                        curr: 1
                    },
                    where: {
                        name: name.val(),
                        code: code.val(),
                        upperId: upperId.val()
                    }
                });
            }
        };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/dictionary/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'name', title: '字典名称'},
            {field: 'code', title: '字典代码'},
            {title: '操作', toolbar: '#barEdit'}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(test)', function (obj) {
        var data = obj.data;

        if (obj.event === 'del') {
            layer.confirm('该功能为系统支撑功能，如非专业人员请勿操作。', function (index) {
                $.ajax({
                    url: ctx + '/dictionary/del?code=' + data.code,
                    type: "post",
                    success: function (d) {
                        if (d.code == 200) {
                            $(".search_btn").click();
                        } else {
                            layer.msg("权限不足，联系超管！", {icon: 5});
                        }
                    }
                });

                layer.close(index);
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2,
                title: "信息维护",
                area: ['850px', '450px'],
                content: ctx + "/dictionary/goEditor?id=" + data.id,
                end: function () {
                    table.reload('tableList', {});
                }
            })
        } else if (obj.event === 'lower') {
            $('#name').val('');
            $('#code').val('');
            $('#upperId').val(data.code);

            hideUpperBut();

            $(".search_btn").click();
        }
    });

    //添加配置
    $(".add_btn").click(function () {
        layer.open({
            title: "添加信息",
            type: 2,
            area: ['850px', '450px'],
            content: ctx + "/dictionary/goAdd?code=" + $('#upperId').val(),
            end: function () {
                table.reload('tableList', {});
            }
        });
    });

    $(".upper_btn").click(function () {
        var upperId = $('#upperId').val();

        $.ajax({
            url: ctx + '/dictionary/detailByCode?code=' + upperId,
            type: "post",
            success: function (result) {
                if (result.code == 200) {
                    $('#name').val('');
                    $('#code').val('');
                    $('#upperId').val(result.data.upperId);

                    hideUpperBut();

                    $(".search_btn").click();
                } else {
                    layer.msg('操作异常，请刷新后重试。', {icon: 5});
                }
            }
        });
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    hideUpperBut();
});

function hideUpperBut() {
    var upper = $('#upperId').val();

    if (upper == '' || upper == '_root_') {
        $('.upper_btn').hide();
    } else {
        $('.upper_btn').show();
    }
}
