<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>字典列表</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
		<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
		<script>
			var ctx = "${ctx}";
		</script>
	</head>
	<body class="childrenBody">
		<blockquote class="layui-elem-quote list_search">
			<form  class="layui-form">
				<input id="index" name="index" type="hidden" value="${entity.index }">
				<input id="showField" name="showField" type="hidden" value="${entity.showField }">
				<input id="valueField" name="valueField" type="hidden" value="${entity.valueField }">

				<div class="layui-inline">
					<input type="text" id="name" class="layui-input" name="name" placeholder="请输入字典名称" value="">
					<input type="hidden" id="upperId" name="upperId" value="${entity.upperId }">
				</div>
				<div class="layui-inline">
					<input type="text" id="code" class="layui-input" name="code" placeholder="请输入字典代码" value="">
				</div>
				<a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search"><i class="layui-icon">&#xe615;</i>查询</a>
			</form>
		</blockquote>
		
		<!-- 数据表格 -->
		<table id="tableList" lay-filter="test"></table>
		
		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript" src="${ctx }/page/sys/dictionary/dictionary_query_sel.js"></script>

		<script type="text/html" id="barEdit">
			<a class="layui-btn layui-btn-xs" lay-event="sel">选择</a>
		</script>
	</body>
</html>