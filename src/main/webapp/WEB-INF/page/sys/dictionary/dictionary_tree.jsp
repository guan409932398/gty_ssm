<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>字典列表</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
		<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/list.css" media="all" />
		
		<!-- ztree -->
		<link rel="stylesheet" href="${ctx }/plugins/zTree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<script type="text/javascript" src="${ctx }/plugins/uimaker/js/jquery.js"></script>
		<script type="text/javascript" src="${ctx }/plugins/zTree/js/jquery.ztree.core.js"></script>
		
		<script>
			var ctx = "${ctx}";
		</script>
		
		<SCRIPT type="text/javascript">
		 	var zNodes = ${nodes};
		 	
			var setting = {
					check: {
						enable: true,
						nocheckInherit: true
					},
					data: {
						simpleData: {
							enable: true
						}
					},
					callback: {
						onClick: clickTreeNode
					}
			};
			
			$(document).ready(function(){
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			});
			
			function clickTreeNode(event, treeId, treeNode, clickFlag) {
				//if(treeNode.id != null && treeNode.id != ''){
		            //$('#dic_upper').val(treeNode.code);
                    //alert(treeId + ":" + treeNode.name + ":" + treeNode.code + ":" + treeNode.id);
				//}
			}
		</SCRIPT>
	</head>
	<body class="childrenBody">
		<div style="width: 25%;float: left;">
			<ul id="treeDemo" class="ztree"></ul>
		</div>
		<div style="width: 75%;float: left;">
			<blockquote class="layui-elem-quote list_search">
				<form  class="layui-form">
					<div class="layui-inline">
						<input type="text" id="dic_name" class="layui-input dic_name" name="dic_name" placeholder="请输入字典名称" value="">
						<input type="hidden" id="dic_upper" name="dic_upper" value="">
					</div>
					<div class="layui-inline">
						<input type="text" id="dic_code" class="layui-input dic_code" name="dic_code" placeholder="请输入字典代码" value="">
					</div>
					<a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search">查询</a>
				</form>
			
				
				<div class="layui-inline">
					<shiro:hasPermission name="sys:dictionary:editor">
						<a class="layui-btn layui-btn-normal add_btn">
							<i class="layui-icon">&#xe608;</i> 添加字典
						</a>
					</shiro:hasPermission>
					
					<a class="layui-btn layui-btn-normal upper_btn" lay-submit="" data-type="search" >
						<i class="layui-icon">&lt;</i> 上一级
					</a>
					
					<a class="layui-btn layui-btn-normal lower_btn" lay-submit="" data-type="search" >
						<i class="layui-icon">&gt;</i> 下一级
					</a>
				</div>
				
			</blockquote>
			
			<!-- 数据表格 -->
			<table id="tableList" lay-filter="test"></table>
		</div>
	
		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript" src="${ctx }/page/sys/dictionary/dictionary_query.js"></script>
		
		<script type="text/html" id="barEdit">
  			<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  			<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
		</script>
		
		<script type="text/html" id="radioTpl">
  			<input type="radio" name="dic_id" value="{{d.dic_code}}" title=" " lay-filter="radiodemo">
		</script>
	</body>
</html>