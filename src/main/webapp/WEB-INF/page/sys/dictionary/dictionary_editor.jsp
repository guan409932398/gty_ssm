<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>字典维护</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		
		<script>  
	        var ctx = "${ctx}";  
	    </script>  
	    
		<style type="text/css">
			.layui-form-item .layui-inline {
				width: 33.333%;
				float: left;
				margin-right: 0;
			}
		</style>
	</head>
	
	<body class="childrenBody">
		<form class="layui-form" style="width: 80%;" id="this_form">
			<input type="hidden" id="id" name="id" value="${po.id }">
			<input type="hidden" id="upperId" name="upperId" value="${empty po.upperId ? 'GTY-QQ:409932398' : po.upperId }">
			<input type="hidden" id="image" name="image" value="${po.image }">
			
			<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
		
			<div class="layui-form-item">
				<label class="layui-form-label">字典名称</label>
				<div class="layui-input-block">
					<input type="text" id="name" class="layui-input" lay-verify="required" placeholder="请输入字典名称" name="name" value="${po.name }">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">字典代码</label>
				<div class="layui-input-block">
					<c:if test="${empty po.id}">
						<input type="text" id="code" class="layui-input" lay-verify="required" placeholder="请输入字典代码" name="code" value="${po.code }">
					</c:if>
					<c:if test="${not empty po.id}">
						<input type="hidden" id="code" name="code" value="${po.code }">
						<label style="height: 38px;line-height: 38px;">${po.code}</label>
					</c:if>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">字典排序号</label>
				<div class="layui-input-block">
					<input type="text" id="sort" class="layui-input" placeholder="" name="sort" value="${po.sort }">
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">字典图片</label>
				<div class="layui-input-block">
                	<button type="button" class="layui-btn layui-bg-blue" id="dic_image_upload_but" style="display: inline;">
						<i class="layui-icon">&#xe67c;</i> 上传图片
					</button>
					
					&nbsp;&nbsp;
                
                	<div class="layui-upload-list" id="dic_image_show" style="display: inline;">
						<c:if test="${not empty po.image }">
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'server' }">
								<a href="${ctx }/file/show?file_id=${po.image }" target="_blank" title="点击查看大图">
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
							
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'qiniu' }">
								<a href="${elfunc:getCon('qiniu_outLink')}${po.image }" target="_blank" title="点击查看大图">
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
							
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'aliyun' }">
								<a href="${elfunc:getCon('alioss_outLink')}${po.image }" target="_blank" title="点击查看大图">
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
						</c:if>
					</div>
                </div>
			</div>
			
            <div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea id="remark" name="remark" placeholder="请输入备注" class="layui-textarea">${po.remark }</textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript" src="${ctx }/page/sys/dictionary/dictionary_editor.js"></script>
	</body>
</html>