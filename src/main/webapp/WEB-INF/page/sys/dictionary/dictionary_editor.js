var $;
var $form;
var form;

var success = 0;
var fail = 0;
var imgurls = "";
var imgsName = "";

layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var laypage = layui.laypage, laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;
    var upload = layui.upload;

    form.on("submit(submit_but)", function (data) {
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        var msg = "发生错误！", flag = false;

        $.ajax({
            type: "post",
            url: ctx + "/dictionary/save",
            data: data.field,
            dataType: "json",
            success: function (d) {
                msg = d.msg;
            },
            error: function () {
                flag = true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        setTimeout(function () {
            if (!flag) {
                top.layer.close(index);
                top.layer.msg(msg);
                layer.closeAll("iframe");
            }
        }, 1000);

        return false;
    });

    // 用户头像
    upload.render({
        elem: '#dic_image_upload_but',
        url: ctx + '/file/upload',
        exts: 'jpg|jpeg|bmp|png',
        size: 10240,
        before: function (obj) {
            layer.load(2, {shade: [0.8]});
        }, done: function (res) {
            layer.closeAll('loading');

            if (res.code == 0) {
                $('#image').val(res.data);

                var oss_service_providers = $('#oss_service_providers').val();

                if (oss_service_providers == 'aliyun') {
                    $('#dic_image_show').html('<a href="' + $('#alioss_outLink').val() + res.data + '" target="_blank" title="点击查看大图"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
                } else if (oss_service_providers == 'qiniu') {
                    $('#dic_image_show').html('<a href="' + $('#qiniu_outLink').val() + res.data + '" target="_blank" title="点击查看大图"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
                } else if (oss_service_providers == 'server') {
                    $('#dic_image_show').html('<a href="' + ctx + "/file/show?file_id=" + res.data + '" target="_blank" title="点击查看大图"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
                }
            } else {
                return layer.msg('上传失败');
            }
        }, error: function () {
            layer.closeAll('loading');
            return layer.msg('上传失败');
        }
    });
})