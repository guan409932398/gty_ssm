<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }" >
			
            <div class="layui-form-item">
                <label class="layui-form-label">反馈标题</label>
                <div class="layui-input-block">
                	<input type="text" id="title" class="layui-input" placeholder="请输入反馈标题" name="title" value="${po.title }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">反馈类型</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_type" value="${po.type }" >

                    <select id="type" name="type" class="layui-select" >
                        <option value="">请选择反馈类型</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('feedback_category') and !empty elfunc:getDic('feedback_category').dics}">
                                <c:forEach items="${elfunc:getDic('feedback_category').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">反馈状态</label>
                <div class="layui-input-block">
                    <input type="hidden" id="state" value="${po.state }" >
                    <input type="radio" name="state" value="did_not_read" title="未阅" >
                    <input type="radio" name="state" value="have_read" title="已阅"  >
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">反馈内容</label>
                <div class="layui-input-block">
                    <textarea id="content" name="content" placeholder="请输入反馈内容" class="layui-textarea" rows="5">${po.content }</textarea>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">反馈人</label>
                <div class="layui-input-block">
                    <input type="hidden" id="userId" name="userId" value="${po.userId}" maxlength="32" >
                    <input type="text" id="userId_show" class="layui-input" placeholder="请选择反馈人" name="userId_show" value="${po.userName }" maxlength="32" style="width: 75%;float: left;" lay-verify="required" readonly>

                    <button id="selBut" class="layui-btn" style="float: left;margin-left:20px;width: 80px;"><i class="layui-icon">&#xe615;</i></button>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/common/feedback/feedback_editor.js"></script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-25 15:47:46 -->

