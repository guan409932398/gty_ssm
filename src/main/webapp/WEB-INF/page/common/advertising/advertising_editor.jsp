<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/css/base.css" />
        <link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="id" name="id" value="${po.id }" >
			
            <div class="layui-form-item">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-block">
                	<input type="text" id="title" class="layui-input" placeholder="请输入标题" name="title" value="${po.title }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">类别</label>
                <div class="layui-input-block">
                    <input type="hidden" id="_category" value="${po.category }" >

                    <select id="category" name="category" class="layui-select" >
                        <option value="">请选择广告类别</option>
                        <c:choose>
                            <c:when test="${!empty elfunc:getDic('ad_category') and !empty elfunc:getDic('ad_category').dics}">
                                <c:forEach items="${elfunc:getDic('ad_category').dics}" var="vpo" varStatus="num">
                                    <option value="${vpo.code}">${vpo.name }</option>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </select>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">排序</label>
                <div class="layui-input-block">
                	<input type="text" id="sort" class="layui-input" placeholder="请输入排序" name="sort" value="${po.sort }" >
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">图片</label>
                <div class="layui-input-block">
                	<input type="hidden" id="image" name="image" value="${po.image }">
                	
                	<button type="button" class="layui-btn layui-bg-blue" id="image_upload_but" style="display: inline;">
                		<i class="layui-icon">&#xe67c;</i> 上传
                	</button>

                	<div class="layui-upload-list" id="image_show" style="display: inline;">
                		<c:if test="${not empty po.image }">
                			<c:if test="${elfunc:getCon('oss_service_providers') eq 'server' }">
								<a href="${ctx }/file/show?file_id=${po.image }" target="_blank" title="点击查看大图"> 
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
							
							<c:if test="${elfunc:getCon('oss_service_providers') eq 'qiniu' }">
								<a href="${elfunc:getCon('qiniu_outLink')}${po.image }" target="_blank" title="点击查看大图"> 
									<label style="height: 38px;line-height: 38px;">
										${po.image }
									</label>
								</a>
							</c:if>
                		
                			<c:if test="${elfunc:getCon('oss_service_providers') eq 'aliyun' }">
	                			<a href="${elfunc:getCon('alioss_outLink')}${po.image }" target="_blank" title="点击查看"> 
	                				<label style="height: 38px;line-height: 38px;">
	                					${po.image } 
	                				</label>
	                			</a>
                			</c:if>
						</c:if>
                	</div>
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">广告内容</label>
                <div class="layui-input-block">
                    <input type="text" id="content" class="layui-input" placeholder="请输入广告内容" name="content" value="${po.content }">
                </div>
            </div>
            
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" placeholder="请输入备注" class="layui-textarea">${po.remark }</textarea>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${ctx }/page/common/advertising/advertising_editor.js"></script>
    </body>
</html>

<!-- @author 官天野 -->
<!-- @version v1.0 -->
<!-- @email: guan409932398@qq.com -->
<!-- @date 2019-06-24 15:12:41 -->

