<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html>
	<head>
		<title>canvas</title>
	</head>

	<body onload="draw();">
		<canvas id="canvas" width="150" height="150"></canvas>
	</body>

	<script type="application/javascript">
		function draw() {
			var canvas = document.getElementById("canvas");
			if (canvas.getContext) {
				var ctx = canvas.getContext("2d");

				ctx.fillStyle = "rgb(200,0,0)";
				ctx.fillRect (10, 10, 55, 50);

				ctx.fillStyle = "rgba(0, 0, 200, 0.5)";
				ctx.fillRect (30, 30, 55, 50);
			}
		}
	</script>
</html>