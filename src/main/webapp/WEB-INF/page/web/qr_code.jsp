<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html lang="zh">
	<head>
		<link href="${ctx }/css/qrcode_logo.css" rel="stylesheet" />
	</head>

	<body>
		<div id="container"></div>
		<div class="control left" style="float: left;">
			<a id="banner" href="#" target="_blank">jQuery.qrcode 0.14.0</a>
			<hr />
			<label for="render">渲染模式</label>
			<select id="render">
				<option value="canvas" selected="selected">canvas</option>
				<option value="image">image</option>
				<option value="div">div</option>
			</select>
			<hr />
			<label for="size">尺寸:</label>
			<input id="size" type="range" value="400" min="100" max="1000" step="50" />
			<label for="fill">二维码颜色</label>
			<input id="fill" type="color" value="#333333" />
			<label for="background">二维码背景色</label>
			<input id="background" type="color" value="#ffffff" />
			<label for="text">二维码内容</label>
			<textarea id="text">http://192.168.1.105/gty_ssm/common/goQrCodeContent?id=ZZZZZZZZZZZZZZZZZZZZZ</textarea>
			<hr />
			<label for="minversion">二维码密度:</label>
			<input id="minversion" type="range" value="6" min="1" max="10" step="1" />
			<label for="eclevel">误差校正等级</label>
			<select id="eclevel"><option value="L">L - Low (7%)</option><option value="M">M - Medium (15%)</option><option value="Q">Q - Quartile (25%)</option><option value="H" selected="selected">H - High (30%)</option></select>
			<label for="quiet">边款宽度:</label>
			<input id="quiet" type="range" value="1" min="0" max="4" step="1" />
			<label for="radius">圆角比例:</label>
			<input id="radius" type="range" value="50" min="0" max="50" step="10" />
		</div>

		<div class="control right" style="float: right;">
			<label for="mode">二维码模式</label>
			<select id="mode">
				<option value="0">0 - 普通模式</option>
				<option value="1">1 - 文字-条形覆盖</option>
				<option value="2">2 - 文字-居中覆盖</option>
				<option value="3">3 - 图片-条形覆盖</option>
				<option value="4" selected="selected">4 - 图片-居中覆盖</option>
			</select>
			<hr />
			<label for="msize">Logo大小:</label>
			<input id="msize" type="range" value="25" min="0" max="40" step="1" />
			<label for="mposx">水平位置:</label>
			<input id="mposx" type="range" value="50" min="0" max="100" step="1" />
			<label for="mposy">垂直位置:</label>
			<input id="mposy" type="range" value="50" min="0" max="100" step="1" />
			<hr />
			<label for="label">二维码文字</label>
			<input id="label" type="text" value="官天野" />
			<label for="font">二维码字体</label>
			<input id="font" type="text" value="仿宋" />
			<label for="fontcolor">文字颜色</label>
			<input id="fontcolor" type="color" value="#ff9818" />
			<hr />
			<label for="image">IMAGE</label>
			<input id="image" type="file" />
		</div>

		<img id="img-buffer" src="${ctx}/images/fxjc/dummy.png" />

		<script src="${ctx }/js/jquery-1.11.0.min.js" type="text/javascript"></script>
		<script src="${ctx }/js/jquery-qrcode.min.js"></script>
		<script src="${ctx }/js/qrcode_logo.js"></script>
	</body>
</html>