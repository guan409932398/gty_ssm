<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Three JS</title>

		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<!-- three js -->
		<script src="${ctx }/plugins/threejs/build/three.min.js"></script>
		<!-- 相机控制器 -->
		<script src="${ctx }/plugins/threejs/examples/js/controls/OrbitControls.js"></script>
		<!-- 模型加载器（无材质模型加载器） -->
		<script src="${ctx }/plugins/threejs/examples/js/loaders/OBJLoader.js"></script>
		<!-- 模型加载器（有材质模型加载器） -->
		<script src="${ctx }/plugins/threejs/examples/js/loaders/MTLLoader.js"></script>
	</head>

	<body onload="draw();"></body>

	<script type="application/javascript">
		// 场景
		var scene;
		// 相机
		var camera;
		// 渲染器
		var renderer;


		function draw() {
			// 初始化场景
			initScene();

			// 初始化图形
			initObject();
			// 初始化灯光
			initLight();

			// 初始化相机
			initCamera();
			// 初始化渲染器
			initRenderer();

			// 渲染（执行渲染）
			animate();

			// 创建相机控制器
			var controls=new THREE.OrbitControls(camera);
			// 监听事件
			controls.addEventListener("change",render);
		}

		//初始化场景
		function initScene(){
			// 场景（模型、灯光）
			scene = new THREE.Scene();
			// 设置天空盒
			scene.background = new THREE.CubeTextureLoader()
					.setPath('${ctx }/images/web/skyboxsun25deg/')
					.load(['px.jpg', 'nx.jpg', 'py.jpg', 'ny.jpg', 'pz.jpg', 'nz.jpg']);
		}

		// 初始化图形
		function initObject(){
			// 创建模型加载器(有材质模型加载器)
			var loader2 = new THREE.MTLLoader();

			loader2.load('${ctx }/images/3d/dodge/dodgeram.mtl', function(materials) {
				materials.preload();

				// model
				var objLoader = new THREE.OBJLoader();
				objLoader.setMaterials(materials);
				objLoader.load('${ctx }/images/3d/dodge/dodgeram.obj', function (object) {
					// 调整模型大小
					object.scale.set(2,2,2);
					// 模型沿X轴旋转
					object.rotateX(Math.PI/0.67);
					// 模型沿Y轴旋转
					object.rotateZ(Math.PI/0.605);
					// 将模型加载到场景
					scene.add(object);
				}, function(){

				}, function(){

				});
			});
		}

		// 初始化灯光
		function initLight(){
			// 添加灯光(PointLight: 点光源)
			var light = new THREE.PointLight(0xffffff);
			// 光源位置
			light.position.set(300,400,200);
			// 将光源加入到场景
			scene.add(light);
			// 加入环境光
			scene.add(new THREE.AmbientLight(0x333333));
		}

		// 初始化相机
		function initCamera(){
			// 相机（观察场景的视角）
			camera = new THREE.PerspectiveCamera(40, 800/400, 1, 1000);
			// 相机位置
			camera.position.set(-800,300,200);
			// 相机方向
			camera.lookAt(scene.position);
		}

		// 初始化渲染器
		function initRenderer() {
			renderer = new THREE.WebGLRenderer();

			// 设置渲染器尺寸
			// renderer.setSize(document.documentElement.clientWidth, document.documentElement.clientHeight);
			renderer.setSize(800,600);
			// 将渲染器加入文档
			document.body.appendChild(renderer.domElement);
		}

		// 渲染
		function render(){
			// 渲染（执行渲染）
			renderer.render(scene,camera);
		}

		// 动画渲染
		function animate(){
			// 每帧执行一次
			requestAnimationFrame(animate);
			render();
		}
	</script>
</html>