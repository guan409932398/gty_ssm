<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html lang="zh">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>可生成带指定图像效果js二维码插件</title>
		<style type="text/css">
			body { width: 100%; padding: 0; background-color: #FFFFF6; }
			h1, h2 { text-align: center; }
			input[type=text] { display: block; width: 380px; height: 30px; margin: 0 auto; font-size: 14pt; }
			input[type=file] { display: block; width: 380px; height: 30px; margin: 0 auto; }
			label { display: block; width: 380px; height: 30px; margin: 0 auto; }
			.group { width: 400px; height: 195px; margin: 0 auto; }
			.clear { clear: both; }
			#qr { float: left; width: 195px; height: 195px; margin: 0 auto; margin-right: 10px; }
			#image { float: left; width: 195px; height: 195px; margin: 0 auto; margin-top: 12px; }
			#combine { width: 195px; height: 195px; margin: 0 auto; }
		</style>
	</head>

	<body>
		<div class="jq22-container">
			<div class="jq22-content ">
				<h2>二维码内容</h2>
				<input id="value" type="text" value="http://192.168.1.105/gty_ssm/common/goQrCodeContent?id=ZZZZZZZZZZZZZZZZZZZZZ">
				<input id="file" type="file">

				<label>
					<input type="radio" value="threshold" name="filter" checked>黑白 &nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" value="color" name="filter">彩色
				</label>

				<div class="group">
					<div id="qr"></div>
					<div id="image"><img src="${ctx}/images/fxjc/dummy.png" width="171"></div>
					<div class="clear"></div>
				</div>

				<h2>转换图</h2>
				<div id="combine"></div>
			</div>
		</div>

		<script src="${ctx }/js/qrcode.js"></script>
		<script src="${ctx }/js/qart.min.js"></script>
		<script src="${ctx }/js/jquery-1.11.0.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var value = 'http://longteng.3todo.com/gty_ssm/common/goQrCodeContent?id=ZZZZZZZZZZZZZZZZZZZZZ';
				var filter = 'threshold';
				var imagePath = '${ctx}/images/fxjc/dummy.png';

				var self = this;

				function makeQR() {
					var qr = qrcode.QRCode(10, 'H');
					qr.addData(value);
					qr.make();
					document.getElementById('qr').innerHTML = qr.createImgTag(3);
				}

				function makeQArt() {
					new QArt({
						value: value,
						imagePath: imagePath,
						filter: filter
					}).make(document.getElementById('combine'));
				}

				function getBase64(file, callback) {
					var reader = new FileReader();
					reader.readAsDataURL(file);
					reader.onload = function () {
						callback(reader.result);
					};
				}

				$('#value').keyup(function() {
					value = $(this).val();
					makeQR();
					makeQArt();
				});

				$('#file').change(function() {
					getBase64(this.files[0], function(base64Img) {
						var regex = /data:(.*);base64,(.*)/gm;
						var parts = regex.exec(base64Img);
						imagePath = parts[0];
						$('#image img').attr('src', imagePath);
						makeQArt();
					});
				});

				$('input[type=radio]').click(function() {
					filter = $(this).val();
					makeQArt();
				});

				makeQR();
				makeQArt();
			});
		</script>
	</body>
</html>