<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title>建设项目信息</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">
		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />

		<script>
	        var ctx = "${ctx}";
	    </script>

		<style type="text/css">
			.layui-form-item .layui-inline {
				width: 33.333%;
				float: left;
				margin-right: 0;
			}
		</style>
	</head>

	<body class="childrenBody">
		<blockquote class="layui-elem-quote layui-text" style="margin-top: 10px;">
			建设项目基本情况
		</blockquote>

        <table class="layui-table">
			<tr>
				<td style="background-color: #F0F0F0; width: 170px;">国民经济行业类型</td>
				<td>${po.companyProject.industryEName }</td>
				<td style="background-color: #F0F0F0; width: 170px;">环境影响评价行业类别</td>
				<td>${po.companyProject.industryName }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">建设性质</td>
				<td>${po.companyProject.projectNature eq 'create' ? '新建（迁建）' : po.companyProject.projectNature eq 'change' ? '改扩建' : po.companyProject.projectNature eq 'reform' ? '技术改造' : '未知类型' }</td>
				<td style="background-color: #F0F0F0;">工程性质</td>
				<td>${po.companyProject.engineeringNature eq 'linear' ? '线性功能' : po.companyProject.engineeringNature eq 'nonlinear' ? '非线性工程' : '未知类型' }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">项目总投资</td>
				<td>${po.companyProject.totalInvest }万元</td>
				<td style="background-color: #F0F0F0;">项目环保投资</td>
				<td>${po.companyProject.environmentInvest }万元</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">计划开工时间</td>
				<td id="startTime"></td>
				<td style="background-color: #F0F0F0;">预计投产时间</td>
				<td id="productionTime"></td>
			</tr>

            <tr>
                <td style="background-color: #F0F0F0;">填表单位</td>
                <td>${po.companyProject.companyName }</td>
				<td style="background-color: #F0F0F0;">填表人</td>
				<td>${po.companyProject.applicant }</td>
            </tr>
            <tr>
                <td style="background-color: #F0F0F0;">项目经办人</td>
                <td colspan="3">${po.companyProject.handler }</td>
            </tr>
			<tr>
				<td style="background-color: #F0F0F0;">建设地点</td>
				<td colspan="3">
					<c:choose>
						<c:when test="${!empty po.projectAddressList }">
							<c:forEach items="${po.projectAddressList }" var="vpo" varStatus="num">
								${vpo.province}${vpo.city}${vpo.county}:${vpo.addr}
							</c:forEach>
						</c:when>
						<c:otherwise>
							暂无建设地点信息
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>

		<blockquote class="layui-elem-quote layui-text" style="margin-top: 10px;">
			建设单位/环评文件编制单位情况
		</blockquote>

		<table class="layui-table">
			<tr>
				<td style="background-color: #F0F0F0; width: 160px;">建设单位名称</td>
				<td>${po.companyProject.companyName }</td>
				<td style="background-color: #F0F0F0; width: 160px;">编制单位名称</td>
				<td>${po.evaluateUnit.name }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">建设单位法人</td>
				<td>${po.companyDetail.legalPerson}</td>
				<td style="background-color: #F0F0F0;">编制单位法人</td>
				<td>${po.evaluateUnit.legalPerson }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">建设单位联系人</td>
				<td>${po.companyDetail.technicalLeader}</td>
				<td style="background-color: #F0F0F0;">编制单位联系人</td>
				<td>${po.evaluateUnit.technicalLeader }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">建设单位联系电话</td>
				<td>${po.companyDetail.telephone}</td>
				<td style="background-color: #F0F0F0;">编制单位联系电话</td>
				<td>${po.evaluateUnit.telephone }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">联系人手机号</td>
				<td>${po.companyDetail.legalPersonPhone}</td>
				<td style="background-color: #F0F0F0;">编制单位联系人手机号<font style="color: red;">&nbsp;&nbsp;*</font></td>
				<td>${po.evaluateUnit.legalPersonPhone }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">&nbsp;</td>
				<td>&nbsp;</td>
				<td style="background-color: #F0F0F0;">编制单位地址</td>
				<td>${po.evaluateUnit.address }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;">&nbsp;</td>
				<td>&nbsp;</td>
				<td style="background-color: #F0F0F0;">编制主持人</td>
				<td>${po.evaluateUnit.projectPrincipal }</td>
			</tr>
			<tr>
				<td style="background-color: #F0F0F0;  width: 140px;">&nbsp;</td>
				<td>&nbsp;</td>
				<td style="background-color: #F0F0F0;  width: 140px;">编制主持人电话</td>
				<td>${po.evaluateUnit.phoneStaff }</td>
			</tr>
		</table>
	</body>

	<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
	<script type="text/javascript">
		layui.config({
			base : "js/"
		}).use(['jquery'],function(){
			var $ = layui.jquery;
			$("#startTime").html(formatTime('${po.companyProject.startTime }','yyyy-MM-dd'));
			$("#productionTime").html(formatTime('${po.companyProject.productionTime }','yyyy-MM-dd'));
		});

		//格式化时间
		function formatTime(UNIX_timestamp,fmt){
			if(UNIX_timestamp == '' || UNIX_timestamp == null){
				return '';
			}

			var datetime = new Date(UNIX_timestamp * 1000);

			if (parseInt(datetime)==datetime) {
				if (datetime.length==10) {
					datetime=parseInt(datetime)*1000;
				} else if(datetime.length==13) {
					datetime=parseInt(datetime);
				}
			}

			datetime=new Date(datetime);

			var o = {
				"M+" : datetime.getMonth()+1,                 //月份
				"d+" : datetime.getDate(),                    //日
				"h+" : datetime.getHours(),                   //小时
				"m+" : datetime.getMinutes(),                 //分
				"s+" : datetime.getSeconds(),                 //秒
				"q+" : Math.floor((datetime.getMonth()+3)/3), //季度
				"S"  : datetime.getMilliseconds()             //毫秒
			};

			if(/(y+)/.test(fmt)){
				fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));
			}

			for(var k in o)
				if(new RegExp("("+ k +")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
			return fmt;
		}
	</script>
</html>