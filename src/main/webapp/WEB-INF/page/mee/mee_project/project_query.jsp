<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>配置列表</title>

		<meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${ctx }/layui/css/layui.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/font_eolqem241z66flxr.css" media="all" />
		<link rel="stylesheet" href="${ctx }/css/list.css" media="all" />

		<script>
			var ctx = "${ctx}";
		</script>
	</head>

	<body class="childrenBody">
		<blockquote class="layui-elem-quote list_search">
			<form  class="layui-form">
				<input type="hidden" id="index" name="index" value="${po.index }">

				<div class="layui-inline">
					<select id="provinces" name="provinces" class="layui-select" >
						<option value="">请选择省份</option>
						<c:choose>
							<c:when test="${!empty elfunc:getDic('mee_provinces') and !empty elfunc:getDic('mee_provinces').dics}">
								<c:forEach items="${elfunc:getDic('mee_provinces').dics}" var="vpo" varStatus="num">
									<option value="${vpo.code}">${vpo.name }</option>
								</c:forEach>
							</c:when>
						</c:choose>
					</select>
				</div>

				<a class="layui-btn search_btn" lay-submit="" data-type="search" lay-filter="search">查询</a>
			</form>
		</blockquote>

		<!-- 数据表格 -->
		<table id="tableList" lay-filter="tableList"></table>

		<script type="text/javascript" src="${ctx }/layui/layui.js"></script>
		<script type="text/javascript" src="${ctx }/page/mee/mee_project/project_query.js"></script>

		<script type="text/html" id="barEdit">
  			<a class="layui-btn layui-btn-xs" lay-event="detail">明细</a>
		</script>

		<script type="text/html" id="esTpl">
			{{#  if(d.examineStatus == 'newly'){ }}
				草稿
			{{#  } else if(d.examineStatus == 'pending') { }}
				待受理
			{{#  } else if(d.examineStatus == 'unAccept') { }}
				未受理
			{{#  } else if(d.examineStatus == 'doing') { }}
				审核中
			{{#  } else if(d.examineStatus == 'selfCorrecting') { }}
				待自校
			{{#  } else if(d.examineStatus == 'succ') { }}
				已通过
			{{#  } else if(d.examineStatus == 'fail') { }}
				未通过
			{{#  } else if(d.examineStatus == 'waitIndustryMsg') { }}
				待填写行业特征表单
			{{#  } else if(d.examineStatus == 'APP_1') { }}
                待受理
			{{#  } else if(d.examineStatus == 'APP_2') { }}
			    补充材料
            {{#  } else if(d.examineStatus == 'APP_3') { }}
			    不予受理
            {{#  } else if(d.examineStatus == 'APP_4') { }}
			    企业更新信息
            {{#  } else if(d.examineStatus == 'APP_5') { }}
			    接受申请
            {{#  } else if(d.examineStatus == 'APP_6') { }}
			    审批通过
            {{#  } else if(d.examineStatus == 'APP_7') { }}
			    审批不通过
            {{#  } else if(d.examineStatus == 'APP_8') { }}
			    办结归档
            {{#  } else { }}
				未知状态
			{{#  } }}
		</script>
	</body>
</html>