layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laypage', 'table', 'laytpl'], function () {
    var $ = layui.jquery;
    var laypage = layui.laypage;
    var form = layui.form, table = layui.table;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;

    active = {
        search: function () {
            var provinces = $("#provinces");

            table.reload('tableList', {
                page: {
                    curr: 1
                },
                where: {
                    provinces: provinces.val()
                }
            });
        }
    };

    //数据表格
    table.render({
        id: 'tableList',
        elem: '#tableList',
        url: ctx + '/meeProject/query',
        cellMinWidth: 80,
        limit: 10,
        limits: [10, 20, 30, 40],
        cols: [[
            {title: '序号', type: 'numbers'},
            {field: 'name', title: '建设项目名称'},
            {field: 'industryName', title: '所属行业'},
            {field: 'docApprovalAuthorityNameBak', title: '审批单位'},
            {field: 'examineStatus', title: '状态', templet: '#esTpl'},
            {
                field: 'createAt',
                title: '创建日期',
                templet: '<div>{{ formatTime(d.createAt,"yyyy-MM-dd")}}</div>',
                width: 110,
                align: 'center'
            },
            {title: '操作', toolbar: '#barEdit'}
        ]],
        page: true,
        where: {
            timestamp: (new Date()).valueOf()
        },
        response: {
            statusName: 'code',
            statusCode: '200',
            msgName: 'msg'
        }
    });

    //监听工具条
    table.on('tool(tableList)', function (obj) {
        var data = obj.data;

        if (obj.event === 'detail') {
            var index = layer.open({
                type: 2,
                title: '建设项目：' + data.name,
                area: ['750px', '600px'],
                content: ctx + "/meeProject/goDetail?key=" + data.id + "&provinces=" + $("#provinces").val(),
                end: function () {

                }
            });

            layer.full(index);
        }
    });

    //查询
    $(".search_btn").click(function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})

//格式化时间
function formatTime(UNIX_timestamp, fmt) {
    if (UNIX_timestamp == '' || UNIX_timestamp == null) {
        return '';
    }

    var datetime = new Date(UNIX_timestamp * 1000);

    if (parseInt(datetime) == datetime) {
        if (datetime.length == 10) {
            datetime = parseInt(datetime) * 1000;
        } else if (datetime.length == 13) {
            datetime = parseInt(datetime);
        }
    }

    datetime = new Date(datetime);

    var o = {
        "M+": datetime.getMonth() + 1,                 //月份
        "d+": datetime.getDate(),                    //日
        "h+": datetime.getHours(),                   //小时
        "m+": datetime.getMinutes(),                 //分
        "s+": datetime.getSeconds(),                 //秒
        "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
        "S": datetime.getMilliseconds()             //毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
