<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset='utf-8'>
	    <title>欢迎页</title>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>
	    <style>
	        *{margin:0;padding:0;-webkit-touch-callout:none;-webkit-user-select:text;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}@media only screen and (min-width:371px){html{font-size:14px!important}}@media only screen and (min-width:401px){html{font-size:15px!important}}@media only screen and (min-width:428px){html{font-size:16px!important}}@media only screen and (min-width:481px){html{font-size:16px!important}}@media only screen and (min-width:569px){html{font-size:17px!important}}a,input{outline:0;-webkit-appearance:none;border-radius:0;border:0;-webkit-tap-highlight-color:rgba(255,0,0,0);background-color:transparent}input::-webkit-input-placeholder,textarea::-webkit-input-placeholder{color:#666;outline:0}input:-moz-placeholder,textarea:-moz-placeholder{color:#666;outline:0}input::-moz-placeholder,textarea::-moz-placeholder{color:#666;outline:0}input:-ms-input-placeholder,textarea:-ms-input-placeholder{color:#666;outline:0}button{outline:0}.container{display:flex;flex-direction:column;justify-content:center;align-items:center;width:100%;height:100%;background:#fff;position:absolute}.center_container{display:flex;flex-direction:column;justify-content:center;align-items:center}.title_container{display:flex;align-items:center}.title_mark{font-size:1.4rem;font-weight:700}.title{font-size:1.8rem;font-weight:700;margin-left:.5rem;margin-right:.5rem}.subtitle_container{font-size:1rem;margin-top:1rem}.right_container{position:absolute;top:3rem;right:2rem}.img_mark2{position:absolute;top:3rem;left:2rem}.img_mark3{position:absolute;bottom:3rem;right:2rem}.img_mark4{position:absolute;bottom:3rem;left:2rem}
	    </style>
	    
	    <script>  
	        var ctx = "${ctx}";  
	    </script>
	</head>

	<body>
	    <div class="container">
	        <div class="center_container">
	            <div class="title_container">
	                <span class="title_mark">-</span>
	                <p class="title">${elfunc:getCon('system_name')}</p>
	                <span class="title_mark">-</span>
	            </div>
	            <div class="right_container">
	                <img class="right_img2" src="${ctx }/images/welcome_3.png" />
	                <img class="right_img1" src="${ctx }/images/welcome_1.png" />
	            </div>
	            <img class="img_mark2" src="${ctx }/images/welcome_2.png" />
	            <img class="img_mark3" src="${ctx }/images/welcome_3.png" />
	            <img class="img_mark4" src="${ctx }/images/welcome_4.png" />
	        </div>
	    </div>
	</body>
</html>