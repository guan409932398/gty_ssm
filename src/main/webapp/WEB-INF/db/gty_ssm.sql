/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50562
Source Host           : localhost:3306
Source Database       : gty_ssm

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2019-07-16 18:08:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_cfsa_suggest
-- ----------------------------
DROP TABLE IF EXISTS `tb_cfsa_suggest`;
CREATE TABLE `tb_cfsa_suggest` (
  `name` varchar(512) DEFAULT NULL COMMENT '标准名称（bzname）',
  `make_or_amend` varchar(32) DEFAULT NULL COMMENT '制订或修订（zdhxg，make：制定，amend：修订）',
  `be_revised_standard_no` varchar(512) DEFAULT NULL COMMENT '被修订标准号（bxdbzh）',
  `standard_category` varchar(32) DEFAULT NULL COMMENT '标准类别（bzlb）',
  `drafting_unit_id` varchar(32) DEFAULT NULL COMMENT '项目提出单位_单位ID（comid）',
  `drafting_unit_name` varchar(512) DEFAULT NULL COMMENT '项目提出单位_单位名称（comname）',
  `drafting_unit_address` varchar(512) DEFAULT NULL COMMENT '项目提出单位_地址（comaddress）',
  `contact_id` varchar(32) DEFAULT NULL COMMENT '项目提出单位_联系人ID（userid）',
  `contact_name` varchar(512) DEFAULT NULL COMMENT '项目提出单位_联系人（username）',
  `contact_phone` varchar(512) DEFAULT NULL COMMENT '项目提出单位_联系电话（userphone）',
  `contact_email` varchar(512) DEFAULT NULL COMMENT '项目提出单位_电子邮箱（useremail）',
  `candidate_unit_name` varchar(512) DEFAULT NULL COMMENT '候选起草单位_单位名称（hxqc_comname）',
  `candidate_unit_contact_name` varchar(512) DEFAULT NULL COMMENT '候选起草单位_联系人（hxqc_lxr）',
  `candidate_unit_contact_phone` varchar(512) DEFAULT NULL COMMENT '候选起草单位_联系电话（hxqc_lxr_phone）',
  `complete_time` varchar(64) DEFAULT NULL COMMENT '候选起草单位_完成项目所需时限（hxqc_wcsxsx）',
  `ready_solve_food_safety_issues` mediumtext COMMENT '拟解决的食品安全问题（njjdspaqwt）',
  `background_and_reasons` mediumtext COMMENT '立项背景和理由（lxbjhly）',
  `technical_indicators_related_information` mediumtext COMMENT '主要技术指标已开展的风险监测和风险评估情况（zyjszbhfxpg）',
  `scope_and_content` mediumtext COMMENT '标准范围和主要技术内容（bzfwhzyjsnr）',
  `international_equivalent_information` mediumtext COMMENT '国际同类标准和国内相关法规标准情况（gjtlbzhgnxgfgbzqk）',
  `cost_budget` decimal(16,4) DEFAULT NULL COMMENT '项目成本预算（xmcbys）',
  `expenditure_plan` mediumtext COMMENT '经费使用计划（jfsyjh）',
  `create_user_id` varchar(32) DEFAULT NULL COMMENT '创建人（log_userid）',
  `opinion_state` varchar(32) DEFAULT NULL COMMENT '意见状态（status）',
  `submit_time` bigint(20) DEFAULT NULL COMMENT '提交时间（submit_date）',
  `submit_user_id` varchar(32) DEFAULT NULL COMMENT '提交操作人ID（submit_userid）',
  `turn_standard` varchar(32) DEFAULT NULL COMMENT '是否转成正式标准（inusing）',
  `turn_user_id` int(11) DEFAULT NULL COMMENT '转成正式项目的人员编号（turn_userid）',
  `turn_date` bigint(20) DEFAULT NULL COMMENT '转成正式项目的时间（turn_date）',
  `reasons_rejection` mediumtext COMMENT '退回原因（th_reason）',
  `id` varchar(32) NOT NULL COMMENT '数据ID（suggest_guid）',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建日期（log_date）',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '数据修改时间（modifydate）',
  `record_state` varchar(32) DEFAULT NULL COMMENT '是否删除（del_mark）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='立项建议征集';

-- ----------------------------
-- Records of tb_cfsa_suggest
-- ----------------------------
INSERT INTO `tb_cfsa_suggest` VALUES ('1234', 'make', '', 'cfsa_standard_category_999013', 'dcc1e31feb4f4b4fac012c3d9858abcf', '', '', 'dcc1e31feb4f4b4fac012c3d9858abcf', '18701308123', null, '', '1234', '1234', '1234', '1234', '1234', '1234', '1234', '1234', '1234', '12.0000', '1234', 'dcc1e31feb4f4b4fac012c3d9858abcf', 'create', null, null, null, null, null, null, 'ca7626f6aedc41ef8a4e174fc569a7d2', '1561964358', '1561964767', 'active');

-- ----------------------------
-- Table structure for tb_common_advertising
-- ----------------------------
DROP TABLE IF EXISTS `tb_common_advertising`;
CREATE TABLE `tb_common_advertising` (
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `category` varchar(32) DEFAULT NULL COMMENT '类别',
  `sort` int(255) DEFAULT NULL COMMENT '排序',
  `image` varchar(1000) DEFAULT NULL COMMENT '图片[img]',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `id` varchar(32) NOT NULL COMMENT '广告ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `record_state` varchar(32) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_common_advertising
-- ----------------------------

-- ----------------------------
-- Table structure for tb_common_feedback
-- ----------------------------
DROP TABLE IF EXISTS `tb_common_feedback`;
CREATE TABLE `tb_common_feedback` (
  `title` varchar(200) DEFAULT NULL COMMENT '反馈标题',
  `type` varchar(32) DEFAULT NULL COMMENT '反馈类型',
  `state` varchar(32) DEFAULT NULL COMMENT '反馈状态',
  `content` mediumtext COMMENT '反馈内容',
  `user_id` varchar(32) DEFAULT NULL COMMENT '反馈人ID ',
  `id` varchar(32) NOT NULL COMMENT '反馈ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `record_state` varchar(32) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_common_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for tb_common_notice
-- ----------------------------
DROP TABLE IF EXISTS `tb_common_notice`;
CREATE TABLE `tb_common_notice` (
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `content` mediumtext COMMENT '内容',
  `type` varchar(32) DEFAULT NULL COMMENT '类型',
  `id` varchar(32) NOT NULL COMMENT '公告ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `record_state` varchar(32) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_common_notice
-- ----------------------------

-- ----------------------------
-- Table structure for tb_ebook_book
-- ----------------------------
DROP TABLE IF EXISTS `tb_ebook_book`;
CREATE TABLE `tb_ebook_book` (
  `title` varchar(32) DEFAULT NULL COMMENT '书籍标题',
  `author` varchar(32) DEFAULT NULL COMMENT '作者',
  `state` varchar(32) DEFAULT NULL COMMENT '书籍状态',
  `category` varchar(32) DEFAULT NULL COMMENT '类别',
  `words` int(11) DEFAULT NULL COMMENT '字数',
  `proofread` varchar(32) DEFAULT NULL COMMENT '校对（checked：已校对，unchecked：未校对）',
  `tag` varchar(200) DEFAULT NULL COMMENT '标签',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `id` varchar(32) NOT NULL COMMENT '书籍ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `record_state` varchar(32) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='书籍表';

-- ----------------------------
-- Table structure for tb_ebook_chapter
-- ----------------------------
DROP TABLE IF EXISTS `tb_ebook_chapter`;
CREATE TABLE `tb_ebook_chapter` (
  `title` varchar(100) DEFAULT NULL COMMENT '章节标题',
  `sort` int(11) DEFAULT NULL COMMENT '章节序号',
  `word` int(255) DEFAULT NULL COMMENT '字数',
  `content` mediumtext COMMENT '章节内容',
  `bookId` varchar(32) DEFAULT NULL COMMENT '书籍ID',
  `proofread` varchar(32) DEFAULT NULL COMMENT '校对（checked：已校对，unchecked：未校对）',
  `id` varchar(32) NOT NULL COMMENT '章节ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `record_state` varchar(32) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`),
  KEY `fk_bookid` (`bookId`),
  CONSTRAINT `fk_bookid` FOREIGN KEY (`bookId`) REFERENCES `tb_ebook_book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='章节表';

-- ----------------------------
-- Table structure for tb_fxjc_fxproject
-- ----------------------------
DROP TABLE IF EXISTS `tb_fxjc_fxproject`;
CREATE TABLE `tb_fxjc_fxproject` (
  `name` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `model` varchar(255) DEFAULT NULL COMMENT '产品型号',
  `color` varchar(255) DEFAULT NULL COMMENT '产品颜色',
  `production_date` bigint(20) DEFAULT NULL COMMENT '生产日期',
  `lot_no` varchar(255) DEFAULT NULL COMMENT '批量编号',
  `execution_standard` varchar(255) DEFAULT NULL COMMENT '执行标准',
  `quality_grade` varchar(255) DEFAULT NULL COMMENT '质量等级',
  `inspector` varchar(255) DEFAULT NULL COMMENT '检验员',
  `id` varchar(32) NOT NULL COMMENT '记录ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建日期',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改日期',
  `record_state` varchar(255) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='方兴产品';

-- ----------------------------
-- Records of tb_fxjc_fxproject
-- ----------------------------
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563202803', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_third_group', '0742f6586d17467aafe40a428b5114af', '1563202805', '1563202805', 'active');
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563200597', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_second_group', '47f5853c0df14f7288d1ac721cc17390', '1563200599', '1563200939', 'active');
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563203081', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_third_group', '4a54d247ed8f46aab57566fb5b3f5133', '1563203084', '1563203084', 'active');
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563190835', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_first_group', '968a5f96bf2742618e7223800325d978', '1563190843', '1563194406', 'generate');
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563201975', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_first_group', 'ae3363b9202348a5988428af3a66b2a5', '1563201978', '1563202800', 'active');
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563203174', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_second_group', 'c2041ab798224deeafb259f55e6f3b25', '1563203177', '1563203177', 'active');
INSERT INTO `tb_fxjc_fxproject` VALUES ('fxjc_project_roofing_slate', 'fxjc_model_1045T', 'fxjc_color_blue', '1563199569', null, 'Q/370683FX001-2018', 'fxjc_level_qualified', 'fxjc_second_group', 'c24d7d60355a4dda8a8a2df4f5cae54c', '1563199572', '1563199586', 'generate');

-- ----------------------------
-- Table structure for tb_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_config`;
CREATE TABLE `tb_sys_config` (
  `code` varchar(32) DEFAULT NULL COMMENT '配置代码',
  `name` varchar(255) DEFAULT NULL COMMENT '配置名称',
  `category` varchar(32) DEFAULT NULL COMMENT '数据类别',
  `type` varchar(32) DEFAULT NULL COMMENT '数据类型',
  `values` varchar(2000) DEFAULT NULL COMMENT '配置值',
  `remark` text COMMENT '备注',
  `id` varchar(32) NOT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_config_code` (`code`),
  KEY `index_config_type` (`type`),
  KEY `index_config_category` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_sys_config
-- ----------------------------
INSERT INTO `tb_sys_config` VALUES ('qr_code_prefix', '二维码前缀', 'system_about', 'config', 'longteng.3todo.com', '', '165fe088531245ca97944f5c099e3736', '1563194917', '1563194917', 'active');
INSERT INTO `tb_sys_config` VALUES ('company_fax', '公司传真', '', 'about', '52165519', '', '2bdd8976f748489eabd6ce7635aaebb1', '1561531925', '1561531925', 'active');
INSERT INTO `tb_sys_config` VALUES ('sms_service_providers', '短信服务商标识', '', 'config', 'aliyun', 'aliyun：阿里云，juhe：聚合', '4ef7d44a8d8011a993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('company_address', '公司地址', null, 'about', '山东省莱州市玉海街7150号', '', '4ef7d44a8d8011b993a9c85b76505693', '1560765973', '1563195416', 'active');
INSERT INTO `tb_sys_config` VALUES ('qiniu_outLink', '七牛外链地址', 'qiniu_oss', 'config', 'http://longtengimg.3todo.com/', '', '4ef7d44a8d8011c993a9c85b76505693', '1560765973', '1561363129', 'active');
INSERT INTO `tb_sys_config` VALUES ('customer_service_phone', '客服电话', null, 'about', '18510113058', '', '4ef7d44a8d8011d993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('system_name', '系统名称', null, 'about', '方兴建材有限公司管理系统', '', '4ef7d44a8d8011e993a9c85b76505693', '1560765973', '1563195474', 'active');
INSERT INTO `tb_sys_config` VALUES ('alipay_appid', '阿里支付APPID', 'ali_pay', 'config', '', '', '4ef7d44a8d8011e993b9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('mee_project_detail_url', '环保部_建设项目明细_url', 'mee_project', 'config', 'http://114.251.10.204/declare/signApi/', 'getProjectInfoOne', '4ef7d44a8d8011e993c9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('sms_switch', '短信开关', '', 'switch', 'open', ' 投产中该项必须打开', '4ef7d44a8d8011e993d9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('app_token_control', 'APP接口Token验证', '', 'switch', null, '', '4ef7d44a8d8011e993e9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('qiniu_secretKey', '七牛secretKey', 'qiniu_oss', 'config', 'hPgfzbMPhUDDDFMyGHOWJ4Q6aHexePKllcfnjcLF', '', '4ef7d44a8d8011e993f9c85b76505693', '1560765973', '1561363145', 'active');
INSERT INTO `tb_sys_config` VALUES ('mee_project_query_url', '环保部_建设项目列表_url', 'mee_project', 'config', 'http://114.251.10.204/declare/signApi/', 'getProjectInfoList', '4ef7d44a8d8011e993g9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('qiniu_bucket', '七牛bucket', 'qiniu_oss', 'config', 'todo-active', '', '4ef7d44a8d8011e993h9c85b76505693', '1560765973', '1561364506', 'active');
INSERT INTO `tb_sys_config` VALUES ('qiniu_accessKey', '七牛accessKey', 'qiniu_oss', 'config', 'dzmEoXzeEdq7Qq0yYGSKGv0XOX3GHKxOpPWJkfgc', '', '4ef7d44a8d8011e993i9c85b76505693', '1560765973', '1561364592', 'active');
INSERT INTO `tb_sys_config` VALUES ('sms_model', '阿里短信模版编号', 'ali_sms', 'config', '', '', '4ef7d44a8d8011e993j9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('authorization_registration', '自动注册（第三方授权）', '', 'switch', null, '', '4ef7d44a8d8011e993k9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('system_Introduction', '系统介绍', null, 'about', '系统支持IE8以上浏览器、360、谷歌浏览器<br/>\n【午17-18时暂停访问】<br/>', '', '4ef7d44a8d8011e993l9c85b76505693', '1560765973', '1561532347', 'active');
INSERT INTO `tb_sys_config` VALUES ('alipay_public_key', '支付宝公钥', 'ali_pay', 'config', '', '注意是支付宝公钥，不是应用公钥', '4ef7d44a8d8011e993m9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('aliyun_sms_accessKey', '阿里云短信accessKey', 'ali_sms', 'config', '', '', '4ef7d44a8d8011e993n9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alioss_bucket', '阿里对象存储bucket', 'ali_oss', 'config', '', '', '4ef7d44a8d8011e993o9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('api_switch', 'API访问开关', '', 'switch', 'open', '', '4ef7d44a8d8011e993p9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('wxpay_key', '微信支付商户号', 'wx_pay', 'config', '', '', '4ef7d44a8d8011f993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alipay_private_key', '阿里支付应用私钥', 'ali_pay', 'config', '', '', '4ef7d44a8d8011g993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alioss_url', '阿里对象存储接口地址', 'ali_oss', 'config', 'https://oss-cn-qingdao.aliyuncs.com', '', '4ef7d44a8d8011h993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('wxpay_url', '微信支付接口地址', 'wx_pay', 'config', 'https://api.mch.weixin.qq.com/pay/unifiedorder', '', '4ef7d44a8d8011i993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('aliyun_sms_accessKeySecret', '阿里云短信accessKeySecret', 'ali_sms', 'config', '', '', '4ef7d44a8d8011j993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('aliyun_sms_signName', '阿里云短信签名', 'ali_sms', 'config', '', '', '4ef7d44a8d8011k993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alioss_outLink', '阿里对象存储外链地址', 'ali_oss', 'config', '', '', '4ef7d44a8d8011l993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('wxpay_notify_url', '微信支付回调地址', 'wx_pay', 'config', '', '', '4ef7d44a8d8011m993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('oss_service_providers', '对象存储服务商', 'system_about', 'config', 'qiniu', 'qiniu：七牛，aliyun：阿里云，server：服务器', '4ef7d44a8d8011n993a9c85b76505693', '1560765973', '1561363072', 'active');
INSERT INTO `tb_sys_config` VALUES ('wxpay_mchid', '微信支付MCHID', 'wx_pay', 'config', '', '', '4ef7d44a8d8011o993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alipay_url', '阿里支付接口地址', 'ali_pay', 'config', 'https://openapi.alipay.com/gateway.do', '', '4ef7d44a8d8011p993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('upload_path', '文件上传路径', '', 'config', 'C:\\\\temp\\\\', 'linux:  /images/', '4ef7d44a8d8011q993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alioss_secret_key', '阿里对象存储SECRET_KEY', 'ali_oss', 'config', '', '', '4ef7d44a8d8011r993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('automatic_registration', '自动注册开关（帐号、手机号注册）', '', 'switch', 'open', '', '4ef7d44a8d8011s993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('mee_ekey', '环保项目_EKEY', 'system_about', 'config', '8HkocpYLeG1LNi5m', '', '4ef7d44a8d8011t993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('accouont_generation_type', '帐号生成规则', 'system_about', 'config', 'number_10', 'number_10： 10位置随机数字\nphone： 以注册手机号做为用户帐号', '4ef7d44a8d8011u993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('alioss_access_key', '阿里对象存储ACCESS_KEY', 'ali_oss', 'config', '', '', '4ef7d44a8d8011v993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('wxpay_appid', '微信支付APPID', 'wx_pay', 'config', '', '', '4ef7d44a8d8011w993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('juhe_sms_key', '聚合短信Key', 'juhe_sms', 'config', '', '', '4ef7d44a8d8011x993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('company_name', '公司名称', null, 'about', '方兴建材有限公司', '', '4ef7d44a8d8011y993a9c85b76505693', '1560765973', '1563195453', 'active');
INSERT INTO `tb_sys_config` VALUES ('alipay_notify_url', '阿里支付回调地址', 'ali_pay', 'config', '', '', '4ef7d44a8d8011z993a9c85b76505693', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('company_phone', '公司电话', '', 'about', '0535-2291030', '', '8a108a4882d74df2945b6fef215c3273', '1561531903', '1563195436', 'active');
INSERT INTO `tb_sys_config` VALUES ('mee_company_detail_url', '环保部_公司信息_url', 'mee_project', 'config', 'http://114.251.10.204/baseinfo/signApi/', 'getCompanyDetail', 'ca8dfb28689d4ad9ac73f17214660ebe', '1560765973', '1560765973', 'active');
INSERT INTO `tb_sys_config` VALUES ('icp_record', '备案号', '', 'about', '京ICP备12013786号-1', '', 'da30b8765693478c8c315d386ca15f7c', '1561531188', '1561531633', 'active');
INSERT INTO `tb_sys_config` VALUES ('company_website', '公司官网', '', 'about', 'www.fangxingjiancai.com', '', 'ef873486f8d647f19c270c1882848ad5', '1563193310', '1563193310', 'active');

-- ----------------------------
-- Table structure for tb_sys_department
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_department`;
CREATE TABLE `tb_sys_department` (
  `name` varchar(200) DEFAULT NULL COMMENT '部门名称',
  `type` varchar(32) DEFAULT NULL COMMENT '部门类型',
  `level` varchar(200) DEFAULT NULL COMMENT '级别串',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '上级ID',
  `sort` int(255) DEFAULT NULL COMMENT '排序号',
  `id` varchar(32) NOT NULL COMMENT '部门ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `record_state` varchar(32) DEFAULT NULL COMMENT '记录状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公司部门表';

-- ----------------------------
-- Records of tb_sys_department
-- ----------------------------

-- ----------------------------
-- Table structure for tb_sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_dictionary`;
CREATE TABLE `tb_sys_dictionary` (
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `code` varchar(32) DEFAULT NULL COMMENT '代码',
  `mate` varchar(32) DEFAULT NULL COMMENT '配对码',
  `upper_id` varchar(32) DEFAULT NULL COMMENT '上级ID',
  `state` varchar(32) DEFAULT NULL COMMENT '代码状态',
  `image` varchar(255) DEFAULT NULL COMMENT '图片ID',
  `sort` double DEFAULT NULL COMMENT '排序号',
  `remark` varchar(255) DEFAULT NULL COMMENT '自定义字段1',
  `id` varchar(32) NOT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典表';

-- ----------------------------
-- Records of tb_sys_dictionary
-- ----------------------------
INSERT INTO `tb_sys_dictionary` VALUES ('陕西省', '969fc2785ab349cc90130cf5ce7cee5e', null, 'mee_provinces', null, '', '27', '', '05b066e7ee80400abe392e778d2cfe03', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('新疆维吾尔族自治区', '6457c6b08b264f1b85721bc800253840', null, 'mee_provinces', null, '', '31', '', '08ac51a83ef74ad9bee25bf1d5dd693f', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品安全国家标准立项协议书', 'cfsa_project_agreement', null, 'cfsa_doc_template', null, '', '99', 'suggest.doc', '0ee9c456c74f45068ff8565cfc1a86b4', '1561970948', '1561970975', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('白色', 'fxjc_color_white', null, 'fxjc_color', null, '', '2', '', '0f2a0b2d04b84fad9b13f7ab79b3e16c', '1563183816', '1563188931', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品相关产品', 'cfsa_standard_category_999006', null, 'cfsa_standard_category', null, '', '96', '999006', '17531463eb04457fa5cf37adc04e60e8', '1561947560', '1561965305', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('宁夏省', 'b3f2ade7f5684844b5abdc6c0292b0e3', null, 'mee_provinces', null, '', '30', '', '17b055271765467a944d8c3bdabf504e', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('河南省', 'd8085075574b4af2b6ca48a128cc6c16', null, 'mee_provinces', null, '', '16', '', '1867b9d573f04724b24ae898902ba892', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('广告类别', 'ad_category', null, 'GTY-QQ:409932398', null, '', null, '', '1935c2162df348cca75c44c3829d38c7', '1561361822', '1561361822', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('四川省', '28ac5852a84b4413953f5f525ffab4ca', null, 'mee_provinces', null, '', '23', '', '1d43f87f47794d6884d46ae5230b7cd4', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴_颜色', 'fxjc_color', null, 'GTY-QQ:409932398', null, '', null, '', '20310099f57c40949f77d24d97db278a', '1563182980', '1563182980', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品安全国家标准类别', 'cfsa_standard_category', null, 'GTY-QQ:409932398', null, '', null, '', '20921473eed546dca1bd871bc022cc26', '1561947437', '1561947437', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('山东省', '926c0b5f720211e9becb0011131e1201', null, 'mee_provinces', null, '', '15', '', '2097001b883940a793f7f3d22d3ac600', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴_质检员', 'fxjc_quality_inspector', null, 'GTY-QQ:409932398', null, '', null, '', '2108d3cf019d4afb8d8485ce4ce9e298', '1563180060', '1563180060', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴_质量等级', 'fxjc_quality_grade', null, 'GTY-QQ:409932398', null, '', null, '', '21ab457689754789b2ec1317cfe5e05c', '1563180440', '1563180440', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('青春校园', 'youth_campus', null, 'ebook_category', null, '', '4', '', '258cdde6d48641b3a6a6928df243e86e', '1561084486', '1561084508', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('青海省', '59f86d9dd1c946d985ccd570f485563e', null, 'mee_provinces', null, '', '29', '', '26af7a46cabc4b119842a2f880079924', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴瓦板', 'fxjc_project_roofing_slate', null, 'fxjc_product_name', null, '', '1', 'default', '27a536588e1b41f99903d199837080be', '1563184024', '1563188981', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('中国红', 'fxjc_color_china_red', null, 'fxjc_color', null, '', '4', '', '2eecdcd087354abdb504f4d14da4f23d', '1563183883', '1563188940', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('通用标准', 'cfsa_standard_category_999012', null, 'cfsa_standard_category', null, '', '90', '999012', '2f9879641fd6407699c3d71b89b0fc0e', '1561947722', '1561965263', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('海南省', 'f9ad50cf00814b34ab865357a313f8e3', null, 'mee_provinces', null, '', '21', '', '2fa7874b981e4077bcd6df464e990609', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('压缩包密码', 'zip_password', null, 'GTY-QQ:409932398', null, '', null, '', '31a57aa68c2a4ba5a9f18569351d46fa', '1562308691', '1562308691', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('营养与特殊膳食食品', 'cfsa_standard_category_999007', null, 'cfsa_standard_category', null, '', '95', '999007', '3af48535d3eb48a28aa8275ce63dab49', '1561947576', '1561965312', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('蓝色', 'fxjc_color_blue', null, 'fxjc_color', null, '', '1', 'default', '435cb813b9da411fbb04c26d497da885', '1563183742', '1563188926', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('江西省', '169e66e71521488983cbb4e5b940f2db', null, 'mee_provinces', null, '', '14', '', '49c7b95b22e94b4482f3ed3e9879d618', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品产品', 'cfsa_standard_category_999004', null, 'cfsa_standard_category', null, '', '98', '999004', '4c292ebdd6854256a087f5febe611e0b', '1561947505', '1561965273', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('家庭情感', 'family_emotion', null, 'ebook_category', null, '', '2', '', '4d5ae28b4dda40dfae318424205cd771', '1561084350', '1561084621', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('Q/370683FX001-2018', 'Q/370683FX001-2018', null, 'fxjc_execution_standard', null, '', '1', 'default', '4e75fe9c580d4c619544f6a3c4a6cc1e', '1563184244', '1563184244', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('asf5cf5hj2fd56', 'asf5cf5hj2fd56', null, 'zip_password', null, '', null, 'https://www.567pan.com/file-251702.html', '4e83047dcc5b4979a9c64dcc10d92a93', '1562309653', '1562309653', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('sv55fh0d5', 'sv55fh0d5', null, 'zip_password', null, '', null, 'https://www.567pan.com/file-251693.html', '525fb37a3e0049aeb21ff0b1ffdc06cb', '1562309675', '1562309675', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('农兽残', 'cfsa_standard_category_999011', null, 'cfsa_standard_category', null, '', '91', '999011', '55fa441ae7034993a55cd4a1baad0e52', '1561947687', '1561965339', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('1040T', 'fxjc_model_1040T', null, 'fxjc_model', null, '', '6', '', '5834fd317bc84a878c1c6507db023e4c', '1563184159', '1563184159', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('890T', 'fxjc_model_890T', null, 'fxjc_model', null, '', '5', '', '62fde231ccf346e98400850f332b8f43', '1563184140', '1563184140', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('省份密钥（环保）', 'mee_provinces', null, 'GTY-QQ:409932398', null, '', null, '', '68e2c4c9b843468a8122405b0aaad624', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('检验方法与规程专业（微生物）', 'cfsa_standard_category_999009', null, 'cfsa_standard_category', null, '', '93', '999009', '69924271ea3b47b29d238c030961954e', '1561947643', '1561965326', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('系统反馈', 'system_feedback', null, 'feedback_category', null, '', '1', '', '6a008c4261034a72bde9ca459bbb4761', '1561449716', '1561449716', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('1075T', 'fxjc_model_1075T', null, 'fxjc_model', null, '', '2', '', '6bb1d2e4cc974b4ebf385206e3380d56', '1563184087', '1563184087', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('服装服饰', 'vt_costume', null, 'video_type', null, '', '1', '', '6bdd486c8d8111a993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('七牛云_对象存储', 'qiniu_oss', null, 'config_category', null, '', '40', '', '6bdd486c8d8111b993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('家电数码', 'vt_digital', null, 'video_type', null, '', '3', '', '6bdd486c8d8111c993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('视频类型', 'video_type', null, 'GTY-QQ:409932398', null, '', null, '', '6bdd486c8d8111d993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('阿里_短信', 'ali_sms', null, 'config_category', null, '', '21', '', '6bdd486c8d8111e993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('环信', 'easemob', null, 'config_category', null, '', '60', '', '6bdd486c8d8111f993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('古玩玉石', 'vt_antique', null, 'video_type', null, '', '2', '', '6bdd486c8d8111g993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('微信_支付', 'wx_pay', null, 'config_category', null, '', '30', '', '6bdd486c8d8111h993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('酒水饮料', 'vt_drinks', null, 'video_type', null, '', '5', '', '6bdd486c8d8111i993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('家居用品', 'tv_household', null, 'video_type', null, '', '4', '', '6bdd486c8d8111j993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('美妆洗护', 'vt_beauty', null, 'video_type', null, '', '6', '', '6bdd486c8d8111k993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('配置类别', 'config_category', null, 'GTY-QQ:409932398', null, '', '0', '', '6bdd486c8d8111l993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('阿里_支付', 'ali_pay', null, 'config_category', null, '', '20', '', '6bdd486c8d8111m993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品生鲜', 'fv_fresh', null, 'video_type', null, '', '7', '', '6bdd486c8d8111n993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('聚合_短信', 'juhe_sms', null, 'config_category', null, '', '50', '', '6bdd486c8d8111o993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('系统相关', 'system_about', null, 'config_category', null, '', '10', '', '6bdd486c8d8111p993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('阿里_对象存储', 'ali_oss', null, 'config_category', null, '', '22', '', '6bdd486c8d8111q993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('图书教育', 'fv_books', null, 'video_type', null, '', '8', '', '6bdd486c8d8111r993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('珠宝配饰', 'fv_jewelry', null, 'video_type', null, '', '9', '', '6bdd486c8d8111s993a9c85b76505693', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('福建省', '0962852477b64652958b3c292a7ec4f2', null, 'mee_provinces', null, '', '13', '', '6c6dbffdf7324cbf84cefd897ba89e6a', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('重庆市', '6d60c74edac945eabd6a4588f23e115c', null, 'mee_provinces', null, '', '22', '', '71bb5dd3234040dbae4058e4350a15da', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('广西省', '5333f83fd5704775ae46b7b8134d822f', null, 'mee_provinces', null, '', '20', '', '72bb144e396646fb8c15302275c23ae1', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴_执行标准', 'fxjc_execution_standard', null, 'GTY-QQ:409932398', null, '', null, '', '7453c873fa7f4d7ba1b921e1fbccdf4d', '1563180390', '1563180390', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('安徽省', 'c006a2f04ca94aa586ee40d0a323a8d0', null, 'mee_provinces', null, '', '12', '', '7523997d54ad46ed82397a1f8c7cf7fd', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('三组', 'fxjc_third_group', null, 'fxjc_quality_inspector', null, '', '3', '', '7a806f2de741474b9fe8b09ab8366630', '1563184448', '1563184448', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('甘肃省', '852dee3f720211e9becb0011131e1201', null, 'mee_provinces', null, '', '0.4', '', '80433d5aa2f546ebbdf99731528b96d5', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('1045T', 'fxjc_model_1045T', null, 'fxjc_model', null, '', '1', 'default', '8293f1e2c553430490b4ec78d3792dac', '1563184075', '1563184174', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('girlse8.net', 'girlse8.net', null, 'zip_password', null, '', null, '', '861dcbdef4a749598eaa78cfda1ea8b9', '1562549970', '1562549970', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('900T', 'fxjc_model_900T', null, 'fxjc_model', null, '', '4', '', '871d3e26203749e0b6059978b38c6da2', '1563184122', '1563184122', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴_产品名称', 'fxjc_product_name', null, 'GTY-QQ:409932398', null, '', null, '', '8736c4970ca747d397279c878c2eab64', '1563180110', '1563180110', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('贵州省', '8161c15cf9894c2284e742db6901b053', null, 'mee_provinces', null, '', '24', '', '8855716cdceb428eb0922a7ee5879ac5', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('DDGREGEGR456', 'DDGREGEGR456', null, 'zip_password', null, '', null, 'http://www.mm222.cn/file-571896.html\nhttp://www.mm222.cn/file-571901.html\n\nhttp://www.mm222.cn/file-435013.html', '8b3f523a2e0c43ecb8d821bfd7e2c334', '1562552008', '1562552129', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('江苏省', '74f08fc1720211e9becb0011131e1201', null, 'mee_provinces', null, '', '0.3', '', '8d370f2fda254ed18819ca0bbe7546da', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('婚姻情感', 'marriage_emotion', null, 'ebook_category', null, '', '6', '', '8e8ca30c2c554a4e86011d6a0ecace9d', '1561084605', '1561084611', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('合格', 'fxjc_level_qualified', null, 'fxjc_quality_grade', null, '', '1', 'default', '8faf815895504a86b1c8fa0ea0465f45', '1563184293', '1563188964', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('960T', 'fxjc_model_960T', null, 'fxjc_model', null, '', '3', '', '991cd0905df34535b899c7d24b3ff925', '1563184105', '1563184105', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('湖南省', '3ab6a1f9154b4ce1a66df2d80c2ea2ce', null, 'mee_provinces', null, '', '18', '', '99e05db6b8fd48ce93142205fef4b3a8', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品安全国家标准系统模版', 'cfsa_doc_template', null, 'GTY-QQ:409932398', null, '', null, '', 'a1e857b51c27409a86c7c84019bb541c', '1561970860', '1561970860', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('生产经营规范', 'cfsa_standard_category_999005', null, 'cfsa_standard_category', null, '', '97', '999005', 'a2f0d5e88e5040fbaf0624e6074f6020', '1561947523', '1561965296', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('湖北省', '64cff7b64cd041caad19bb375655c93a', null, 'mee_provinces', null, '', '17', '', 'abf16c3e4e39424dad4bd981ce4d5740', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('北京市', 'cc2d12f0e41941688fadb4814ffd0363', null, 'mee_provinces', null, '', '1', '', 'b1438377c05d4e62b21407f09b702f56', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('1024', '1024', null, 'zip_password', null, '', null, 'http://www.mm222.cn/file-581844.html\nhttp://www.mm222.cn/file-586056.html', 'b282165cf40a46a4a20a7f652732a078', '1562639489', '1562832219', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('黑龙江省', '8c7d8ad1720211e9becb0011131e1201', null, 'mee_provinces', null, '', '8', '', 'b2a256b0f48f471d9699bc5fd5b1c7ce', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('检验方法与规程专业（理化）', 'cfsa_standard_category_999008', null, 'cfsa_standard_category', null, '', '94', '999008', 'b2fb735e942942e9af76e3b0e402e509', '1561947620', '1561965319', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('云南省', '9c767c2ff2ca41f59c9849bb56758a38', null, 'mee_provinces', null, '', '25', '', 'b36295deffea418dbc588b1ff8c7f3fc', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('方兴_产品型号', 'fxjc_model', null, 'GTY-QQ:409932398', null, '', null, '', 'b3a5e4f0a50349aba507378a3f843763', '1563183118', '1563183118', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('小说类别', 'ebook_category', null, 'GTY-QQ:409932398', null, '', null, '', 'b56119e7cf734bbfade7cf8e782ae4fb', '1561084255', '1561084255', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('浙江省', '7cc720d3270d4204866cd0b4f7afccc5', null, 'mee_provinces', null, '', '11', '', 'b8ad73714550406699427581c5445b6a', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('asdgsdfg2048', 'asdgsdfg2048', null, 'zip_password', null, '', null, 'http://www.mm222.cn/file-587576.html', 'baaec17fac564884890ec697ea7628c8', '1562831695', '1562831695', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('河北省', '4fee4d2d720211e9becb0011131e1201', null, 'mee_provinces', null, '', '0.1', '', 'be6fafe1d40e432189e8b5020ade7b97', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('中海蓝', 'fxjc_color_china_sea_blue', null, 'fxjc_color', null, '', '5', '', 'c48de44e9668454ebeccf73d8d5dad36', '1563183939', '1563188945', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('营养强化剂', 'cfsa_standard_category_999013', null, 'cfsa_standard_category', null, '', '89', '999013', 'c4ffe031026f413bae8b2f06df16ea72', '1561947739', '1561965256', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('上海市', '159e5aed304543ba8f7dce6fe55ac68f', null, 'mee_provinces', null, '', '9', '', 'c9ba2fbf3cda4ce3945aa7f7d536240e', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('都市生活', 'city_life', null, 'ebook_category', null, '', '5', '', 'ca8d103d4d854abba49dad3fe930c3d5', '1561084522', '1561084522', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('灰色', 'fxjc_color_gray', null, 'fxjc_color', null, '', '3', '', 'cc61c0eee97d4c53a8d7291f2db84c39', '1563183845', '1563188935', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('辽宁省', '6ccda970720211e9becb0011131e1201', null, 'mee_provinces', null, '', '0.2', '', 'd5112791a550477b866cd1404775cd47', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('二组', 'fxjc_second_group', null, 'fxjc_quality_inspector', null, '', '2', '', 'd53b092f8ae940d5b12b4ee744d46b28', '1563184394', '1563184394', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('反馈类别', 'feedback_category', null, 'GTY-QQ:409932398', null, '', null, '', 'd55e03188a654f38b273f4187c5aa6cd', '1561449686', '1561449686', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('广东省', '9d0572d055594be78f1cbd0d12eede80', null, 'mee_provinces', null, '', '19', '', 'd96451a341604622a49c15e9bcca9d5c', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('新疆建设兵团', '5b8d10466808443e958c1d5ab3c1fc5a', null, 'mee_provinces', null, '', '32', '', 'deac5ac6f38245de9d6f2d4704aa5117', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('一组', 'fxjc_first_group', null, 'fxjc_quality_inspector', null, '', '1', '', 'e08f7f46026d437c9190aff2be4cf5e4', '1563184351', '1563184356', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('gZu2(b1I0-Dc&3t6', 'gZu2(b1I0-Dc&3t6', null, 'zip_password', null, '', null, 'http://www.mm222.cn/file-575470.html\nhttp://www.mm222.cn/file-583210.html\nhttp://www.mm222.cn/file-586519.html\n\nhttp://www.xun-niu.com/down2-188301.html\n', 'e1c70dba85484a17b9e00ce7c0eb11d1', '1562308701', '1562832210', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('食品添加剂', 'cfsa_standard_category_999003', null, 'cfsa_standard_category', null, '', '99', '999003', 'e7a95f7979b145849d70672fcc2f0e92', '1561947489', '1561965284', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('检验方法与规程专业（毒理）', 'cfsa_standard_category_999010', null, 'cfsa_standard_category', null, '', '92', '999010', 'ec5bffeb98f94cc6a7b5bbc7df08ab9a', '1561947662', '1561965332', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('吉林省', 'de6612b0f337400295f7842723db22b0', null, 'mee_provinces', null, '', '7', '', 'ecc1fded666c4c87a080cc124a853085', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('内蒙古省', 'ccadd7ee14fb418ba6fee8c41011e5fb', null, 'mee_provinces', null, '', '5', '', 'f1ae0d9f92dc49a591a8fcfec4ba6441', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('武侠玄幻', 'fantasy_martial', null, 'ebook_category', null, '', '1', '', 'f3ba79fd338d440fa00a4c7d958ac87d', '1561084312', '1561084312', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('现代情感', 'modern_emotion', null, 'ebook_category', null, '', '3', '', 'f93212fe1813451abcf6ec85f9c655d3', '1561084380', '1561084387', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('首页轮播图', 'home_page_image', null, 'ad_category', null, '', '1', '', 'f97a7df7b8c04d6fb781254eaf9829a9', '1561361934', '1561361934', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('山西省', 'cf394a135b884614bb1e0c66694a154e', null, 'mee_provinces', null, '', '4', '', 'f98f904d26994ba9b9eacb6500f638ad', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('天津市', '3be02b11493343babaf09bd27659c79f', null, 'mee_provinces', null, '', '2', '', 'f99882294e3048d9b4fae795971f5f81', '1560765976', '1560765976', 'active');
INSERT INTO `tb_sys_dictionary` VALUES ('西藏自治区', '9ba702700bc0426a89d3a52cfc705878', null, 'mee_provinces', null, '', '26', '', 'fbe994755c014041a25e2fe3a8698ece', '1560765976', '1560765976', 'active');

-- ----------------------------
-- Table structure for tb_sys_file
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_file`;
CREATE TABLE `tb_sys_file` (
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `table` varchar(32) DEFAULT NULL COMMENT '附件来源表',
  `key` varchar(32) DEFAULT NULL COMMENT '附件来源键',
  `path` varchar(255) DEFAULT NULL COMMENT '附件地址',
  `state` int(32) DEFAULT NULL COMMENT '附件地址',
  `sort` int(11) DEFAULT NULL COMMENT '附件排序号',
  `thumb` varchar(255) DEFAULT NULL,
  `id` varchar(32) NOT NULL COMMENT '附件ID',
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for tb_sys_menus
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_menus`;
CREATE TABLE `tb_sys_menus` (
  `title` varchar(32) NOT NULL COMMENT '菜单名',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `href` varchar(255) DEFAULT NULL COMMENT '资源地址',
  `perms` varchar(500) DEFAULT NULL COMMENT '权限',
  `spread` varchar(32) NOT NULL COMMENT 'true：展开，false：不展开',
  `parent_id` varchar(32) NOT NULL COMMENT '父节点',
  `sorting` bigint(20) DEFAULT NULL COMMENT '排序号',
  `id` varchar(32) NOT NULL COMMENT '菜单ID',
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单';

-- ----------------------------
-- Records of tb_sys_menus
-- ----------------------------
INSERT INTO `tb_sys_menus` VALUES ('产品信息（职）', '', 'fxproject/goQuery', '', 'false', 'a8b29fcbe30f48119fcb823f4ec534d8', '80', '0512cec3a39a4adc8b91a9afcde88c2b', '1563196434', '1563196672', 'active');
INSERT INTO `tb_sys_menus` VALUES ('反馈管理', '&#xe611;', 'feedback/goQuery', '', 'false', '7868ac6198a047558ecffa2370099a91', '70', '09fee8e1395142b89169f6b51c7765f8', '1561449441', '1561449441', 'active');
INSERT INTO `tb_sys_menus` VALUES ('后台首页', '', 'page/main.html', '', 'false', 'GTY-QQ:409932398', '9999', '1', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('运维管理', '', '', '', 'false', 'GTY-QQ:409932398', '0', '147', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('SQL监控', '&#xe670;', 'common/druid', '', 'false', '147', '0', '153', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('组织机构', '', 'department/goQuery', '', 'false', '95', '90', '17d8caa39aa84257907734ac8095b17f', '1561541165', '1561541192', 'active');
INSERT INTO `tb_sys_menus` VALUES ('缓存管理', '', 'common/refresh', '', 'false', '147', '66', '188', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('菜单管理', '', 'menus/goQuery', null, 'false', '147', '99', '2', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('关于系统', '', 'config/goAboutUs', '', 'false', '95', '63', '215', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('字典管理', '', 'dictionary/goQuery', '', 'false', '147', '88', '224', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('配置管理', '', 'config/goQuery', '', 'false', '147', '77', '225', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('开关管理', '&#xe631;', 'config/goSwitchQuery', '', 'false', '147', '76', '227', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('角色管理', '&#xe66f;', 'roles/goQuery', '', 'false', '95', '89', '228', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('用户管理', '&#xe613;', 'user/goQuery', '', 'false', '95', '98', '229', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('资源搜索', '', 'meeProject/goQuery', null, 'false', '66d4470f0b7d4335824eba26ba4c2ea6', '99', '46b09db25df24a2ba985cf9361b89d7c', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('电子书管理', '', '', '', 'false', 'GTY-QQ:409932398', '50', '4fd49dc6699d46f882da47436f0b7a4d', '1560996506', '1560996514', 'active');
INSERT INTO `tb_sys_menus` VALUES ('环保司', '', '', '', 'false', 'GTY-QQ:409932398', '90', '66d4470f0b7d4335824eba26ba4c2ea6', '1560765983', '1561369005', 'active');
INSERT INTO `tb_sys_menus` VALUES ('二维码生成-图片', '', 'web/goQrCodeImg', '', 'false', '7868ac6198a047558ecffa2370099a91', '0', '74b518321d9146ab8ef054f1d1cdecc2', '1562030257', '1562807952', 'active');
INSERT INTO `tb_sys_menus` VALUES ('通用功能', '&#xe656;', '', '', 'false', 'GTY-QQ:409932398', '45', '7868ac6198a047558ecffa2370099a91', '1561360669', '1561360669', 'active');
INSERT INTO `tb_sys_menus` VALUES ('广告管理', '', 'advertising/goQuery', '', '', '7868ac6198a047558ecffa2370099a91', '90', '93121739d4b04de0a41ff8668bb7adfe', '1561360705', '1561360834', 'active');
INSERT INTO `tb_sys_menus` VALUES ('系统管理', '', '', '', 'false', 'GTY-QQ:409932398', '40', '95', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('查询', '', '', 'common:druid:view', 'false', '153', '1', '9e3ab9b753714ae7b78dba9c1dafdeff', '1560765983', '1560765983', 'active');
INSERT INTO `tb_sys_menus` VALUES ('立项建议征集', '', '', '', 'false', 'GTY-QQ:409932398', '200', 'a7f7b0d5b6174beea76ae8bcc3af4ea9', '1561945449', '1561945509', 'active');
INSERT INTO `tb_sys_menus` VALUES ('方兴建材', '', '', '', 'false', 'GTY-QQ:409932398', '300', 'a8b29fcbe30f48119fcb823f4ec534d8', '1563181303', '1563196444', 'active');
INSERT INTO `tb_sys_menus` VALUES ('书籍管理', '', 'book/goQuery', '', 'false', '4fd49dc6699d46f882da47436f0b7a4d', '99', 'a9afbed44a564bbb9e98dfbc8ce3065f', '1560996545', '1560996575', 'active');
INSERT INTO `tb_sys_menus` VALUES ('TreeJs', '&#xe65d;', 'common/goThreeJs', '', 'false', '7868ac6198a047558ecffa2370099a91', '-1', 'b4405167c819487abb6cdd9c496afc67', '1562661646', '1562661646', 'active');
INSERT INTO `tb_sys_menus` VALUES ('立项建议申请', '', 'suggest/goQuery', '', 'false', 'a7f7b0d5b6174beea76ae8bcc3af4ea9', '99', 'ceeb2dc3326e4dc5b5a918d774fe6d9d', '1561945500', '1561948654', 'active');
INSERT INTO `tb_sys_menus` VALUES ('二维码生成', '', 'web/goQrCode', '', 'false', '7868ac6198a047558ecffa2370099a91', '1', 'e2927f1be70d47af80c1136e77298d31', '1562030232', '1562807939', 'active');
INSERT INTO `tb_sys_menus` VALUES ('公告管理', '&#xe667;\r\n', 'notice/goQuery', '', 'false', '7868ac6198a047558ecffa2370099a91', '80', 'ee62886ba31542e2b80fd52ef8669f56', '1561360733', '1561360880', 'active');
INSERT INTO `tb_sys_menus` VALUES ('产品信息（管）', '', 'fxproject/goManageQuery', '', 'false', 'a8b29fcbe30f48119fcb823f4ec534d8', '99', 'f0812824abc149e197e9e5179cb8e770', '1563181385', '1563196664', 'active');

-- ----------------------------
-- Table structure for tb_sys_roles
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_roles`;
CREATE TABLE `tb_sys_roles` (
  `name` varchar(32) NOT NULL COMMENT '角色名',
  `remark` varchar(255) DEFAULT NULL COMMENT '角色备注',
  `id` varchar(32) NOT NULL COMMENT '角色编号',
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_sys_roles
-- ----------------------------
INSERT INTO `tb_sys_roles` VALUES ('方兴建材', '方兴建材', '935836b281af409d8cfa2196aafe555a', '1560765986', '1562055236', 'active');
INSERT INTO `tb_sys_roles` VALUES ('食药监-超管', '', 'd3622d8003fc4202adb8a5c5f5c2d96d', '1561685714', '1561945815', 'active');
INSERT INTO `tb_sys_roles` VALUES ('超级管理员', '超级管理员', 'GTY-QQ:409932398', '1560765986', '1560765986', 'active');

-- ----------------------------
-- Table structure for tb_sys_roles_menus
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_roles_menus`;
CREATE TABLE `tb_sys_roles_menus` (
  `menu_id` varchar(32) NOT NULL,
  `role_id` varchar(32) NOT NULL,
  `id` varchar(32) NOT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rm_menu_menuid` (`menu_id`),
  KEY `fk_rm_role_roleid` (`role_id`),
  CONSTRAINT `fk_rm_menu_menuid` FOREIGN KEY (`menu_id`) REFERENCES `tb_sys_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rm_role_roleid` FOREIGN KEY (`role_id`) REFERENCES `tb_sys_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_sys_roles_menus
-- ----------------------------
INSERT INTO `tb_sys_roles_menus` VALUES ('1', '935836b281af409d8cfa2196aafe555a', '27960928a43d4809a1c18bdb06fa384b', '1562055237', '1562055237', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('1', 'd3622d8003fc4202adb8a5c5f5c2d96d', '46be4be61cf048c0baac9bdc53fbda71', '1561945815', '1561945815', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('74b518321d9146ab8ef054f1d1cdecc2', '935836b281af409d8cfa2196aafe555a', '49cddd62853940e9ad5b2c0cdb93dbb3', '1562055237', '1562055237', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('46b09db25df24a2ba985cf9361b89d7c', '935836b281af409d8cfa2196aafe555a', '5001c722e95d4f37a02e1f8ad7a3160c', '1562055237', '1562055237', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('66d4470f0b7d4335824eba26ba4c2ea6', '935836b281af409d8cfa2196aafe555a', '61b8d733c24747f49fcded55256d3f47', '1562055237', '1562055237', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('ceeb2dc3326e4dc5b5a918d774fe6d9d', 'd3622d8003fc4202adb8a5c5f5c2d96d', '9c5203bf3bf2405e9477371b74eb13f8', '1561945815', '1561945815', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('a7f7b0d5b6174beea76ae8bcc3af4ea9', 'd3622d8003fc4202adb8a5c5f5c2d96d', 'a69b14b8374548c0a36669d0418bd439', '1561945815', '1561945815', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('e2927f1be70d47af80c1136e77298d31', '935836b281af409d8cfa2196aafe555a', 'e04d8e7994d945508bb25600bbc642bf', '1562055237', '1562055237', 'active');
INSERT INTO `tb_sys_roles_menus` VALUES ('7868ac6198a047558ecffa2370099a91', '935836b281af409d8cfa2196aafe555a', 'edc8612e69bd4a5c9bca6b64ac0d5951', '1562055237', '1562055237', 'active');

-- ----------------------------
-- Table structure for tb_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_user`;
CREATE TABLE `tb_sys_user` (
  `account` varchar(32) DEFAULT NULL COMMENT '用户帐号',
  `password` varchar(64) NOT NULL COMMENT '用户密码',
  `nickname` varchar(32) DEFAULT NULL COMMENT '用户昵称',
  `real_name` varchar(32) DEFAULT NULL COMMENT '真实姓名（联系人）',
  `phone` varchar(32) NOT NULL COMMENT '用户手机号',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `image` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `gender` varchar(32) DEFAULT NULL COMMENT '性别',
  `age` varchar(32) DEFAULT NULL COMMENT '年龄',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `desc` varchar(2000) DEFAULT NULL COMMENT '简介',
  `category` varchar(32) DEFAULT NULL COMMENT '用户类别',
  `type` varchar(32) DEFAULT NULL COMMENT '用户类型',
  `state` varchar(32) DEFAULT NULL COMMENT '用户状态',
  `user_role` varchar(32) DEFAULT NULL COMMENT '用户角色ID',
  `referral_user` varchar(32) DEFAULT NULL COMMENT '推荐人',
  `id` varchar(32) NOT NULL COMMENT '用户ID',
  `create_time` bigint(20) DEFAULT NULL,
  `modify_time` bigint(20) DEFAULT NULL,
  `record_state` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_phone` (`phone`),
  UNIQUE KEY `user_account` (`account`),
  KEY `fk_user_role_roleid` (`user_role`),
  CONSTRAINT `fk_user_role_roleid` FOREIGN KEY (`user_role`) REFERENCES `tb_sys_roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of tb_sys_user
-- ----------------------------
INSERT INTO `tb_sys_user` VALUES ('fxjc', '4b0d02fad304c09dd76293fbcb90f886', '明明姐', null, '13853255336', null, '', 'woman', '18', null, '', null, null, 'active', '935836b281af409d8cfa2196aafe555a', null, '1254822a927b464fbb0546ff04946b5b', '1560765993', '1561685836', 'active');
INSERT INTO `tb_sys_user` VALUES ('zhangzhe', 'e10adc3949ba59abbe56e057f20f883e', '张哲', null, '18701308123', null, '', null, '', null, '', null, null, 'active', 'd3622d8003fc4202adb8a5c5f5c2d96d', null, 'dcc1e31feb4f4b4fac012c3d9858abcf', '1561685930', '1561685930', 'active');
INSERT INTO `tb_sys_user` VALUES ('admin', 'e10adc3949ba59abbe56e057f20f883e', '官天野', null, '18510113058', null, '', 'man', '', null, '', null, 'merchant', 'active', 'GTY-QQ:409932398', null, 'GTY-QQ:409932398', '1560765993', '1560765993', 'active');
