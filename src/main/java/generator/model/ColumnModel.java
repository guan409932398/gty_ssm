package generator.model;

import lombok.*;

import java.io.Serializable;

/**
 * 数据库列对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190615
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ColumnModel implements Serializable {
    private static final long serialVersionUID = 1L;

    // 列英文名(首字母大写)
    private String colEnName;
    // 列英文名(小写)
    private String colEnNameLower;
    // 列数据库名(小写)
    private String colDbNameLower;
    // 列中文名
    private String colCnName;
    // 列类型
    private String colType;
    // 列长度
    private Integer colLength;
    // 是否为图片
    private boolean colIsImg;
}
