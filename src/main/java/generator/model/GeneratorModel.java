package generator.model;

import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 代码生成基本信息
 *
 * @author 官天野 QQ:409932398
 * @version v1.0
 * @date 2019-06-15 14:26:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneratorModel implements Serializable {
    private static final long serialVersionUID = 1L;

    // 公司英文名
    private String companyEnName;
    // 作者
    private String author;
    // 作者邮箱
    private String email;

    // 模块名
    private String modelName;
    // 数据表名
    private String tableName;

    // 功能英文名(首字母大写)
    private String funDbName;
    // 功能英文名(首字母大写)
    private String funEnName;
    // 功能英文名(小写)
    private String funEnNameLower;
    // 功能中文名
    private String funCnName;

    // 功能对应表主键(首字母大写)
    private String pkEnName;
    // 功能对应表主键(小写)
    private String pkEnNameLower;
    // 功能对应表数据库列明
    private String pkDbNameLower;

    // 列集合
    private List<ColumnModel> cols;
    // 创建时间
    private String createTime;

    public GeneratorModel(String companyEnName, String author, String email, String modelName, String tableName) {
        this.companyEnName = companyEnName;
        this.author = author;
        this.email = email;
        this.modelName = modelName;
        this.tableName = tableName;
        this.setCreateTime(DateUtil.now());
    }
}
