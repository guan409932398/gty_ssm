package generator.util;

import com.gty.utils.TxtUtils;
import generator.AiCode;
import generator.model.ColumnModel;
import generator.model.GeneratorModel;
import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TableUtil {

    // table 查询语句
    private final static String TABLE_SEARCH_SQL = "SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.TABLES WHERE table_schema=? and table_name=?";

    // column 字段
    private final static String COLUMN_SQL = "SHOW FULL COLUMNS FROM ";

    public static void InitData(GeneratorModel gm) throws Exception {
        getTable(gm);
        getColumns(gm);
    }

    /**
     * 获取表信息
     */
    private static void getTable(GeneratorModel gm) throws Exception {
        // 与数据库的连接
        Connection conn = null;

        try {
            // 与数据库的连接
            conn = DBUtil.getConnection();
            PreparedStatement stmt = conn.prepareStatement(TABLE_SEARCH_SQL);

            stmt.setString(1, AiCode.DB_SCHEMA);
            stmt.setString(2, gm.getTableName());

            ResultSet rs = stmt.executeQuery();

            // 从查询结果中取出需要的信息
            while (rs.next()) {
                gm.setFunDbName(gm.getTableName().toLowerCase());
                gm.setFunEnNameLower(TxtUtils.underlineToCame(getFunctionName(gm.getFunDbName())));
                gm.setFunEnName(TxtUtils.convertFirstCharUpper(gm.getFunEnNameLower()));

                String funCnName = rs.getString("TABLE_COMMENT");
                gm.setFunCnName(StringUtils.isBlank(funCnName) ? gm.getTableName() : funCnName);

                // 判断是否填写表注释
                if (StringUtils.isBlank(gm.getFunCnName())) {
                    throw new Exception(">>>>>>>>>>" + gm.getTableName() + "未写表注释");
                }

                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(">>>>>>>>>> 数据库查询异常");
        } finally {
            DBUtil.closeConnection(conn);
        }
    }

    /**
     * 获取表字段信息
     */
    private static void getColumns(GeneratorModel gm) throws Exception {
        // 与数据库的连接
        Connection conn = null;

        try {
            // 与数据库的连接
            conn = DBUtil.getConnection();
            PreparedStatement stmt = conn.prepareStatement(TABLE_SEARCH_SQL);
            ResultSet rs = stmt.executeQuery(COLUMN_SQL + gm.getTableName());

            List<ColumnModel> cols = new ArrayList<>();

            boolean existCreateTime = false;
            boolean existModifyTime = false;
            boolean existRecordState = false;

            // 从查询结果中取出需要的信息
            while (rs.next()) {
                ColumnModel vo = new ColumnModel();

                if ("PRI".equals(rs.getString("Key"))) {
                    String pkEnName = rs.getString("Field");

                    if (StringUtils.isBlank(pkEnName)) {
                        throw new Exception(">>>>>>>>>>" + gm.getTableName() + "主键不存在");
                    } else if (!"id".equals(pkEnName)) {
                        throw new Exception(">>>>>>>>>>因底层封装" + gm.getTableName() + "表主键必须为id");
                    } else if (StringUtils.isNotBlank(gm.getPkEnName())) {
                        throw new Exception(">>>>>>>>>>" + gm.getTableName() + "存在联合主键，代码生成器现不支持联合主键生成");
                    } else {
                        gm.setPkDbNameLower(pkEnName.toLowerCase());
                        gm.setPkEnNameLower(TxtUtils.underlineToCame(gm.getPkDbNameLower()));
                        gm.setPkEnName(TxtUtils.convertFirstCharUpper(gm.getPkEnNameLower()));
                    }
                }

                vo.setColDbNameLower(rs.getString("Field").toLowerCase());
                vo.setColEnNameLower(TxtUtils.underlineToCame(vo.getColDbNameLower()));
                vo.setColEnName(TxtUtils.convertFirstCharUpper(vo.getColEnNameLower()));
                vo.setColType(getType(rs.getString("Type")));
                vo.setColLength(getLength(rs.getString("Type")));
                setComment(gm, vo, rs.getString("Comment"));

                if ("create_time".equals(vo.getColDbNameLower())) {
                    existCreateTime = true;
                }

                if ("modify_time".equals(vo.getColDbNameLower())) {
                    existModifyTime = true;
                }

                if ("record_state".equals(vo.getColDbNameLower())) {
                    existRecordState = true;
                }

                cols.add(vo);
            }

            if (!existCreateTime) {
                throw new Exception(">>>>>>>>>>" + gm.getTableName() + "缺失create_time字段");
            }

            if (!existModifyTime) {
                throw new Exception(">>>>>>>>>>" + gm.getTableName() + "缺失modify_time字段");
            }

            if (!existRecordState) {
                throw new Exception(">>>>>>>>>>" + gm.getTableName() + "缺失record_state字段");
            }

            gm.setCols(cols);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(">>>>>>>>>> 数据库查询异常");
        } finally {
            DBUtil.closeConnection(conn);
        }
    }

    /**
     * 根据TableName获取FunctionName
     */
    private static String getFunctionName(String tableName) throws Exception {
        // 删除表标识
        String tabName = tableName.replace("tb_", "");
        // 删除模块标识
        String funcionName = tabName.substring(tabName.indexOf("_") + 1);

        // 将有含义的功能名首字母大写
        while (funcionName.contains("_")) {
            int location = funcionName.indexOf("_");

            // 判断是否以_结尾
            if (location + 1 == funcionName.length()) {
                throw new Exception(">>>>>>>>>>" + tableName + "命名不合规，以_结尾");
            } else {
                StringBuilder sb = new StringBuilder(funcionName);

                sb.replace(location, location + 2, ("" + sb.charAt(location + 1)).toUpperCase());

                funcionName = sb.toString();
            }
        }

        return funcionName;
    }

    /**
     * 获取类型
     */
    private static String getType(String type) throws Exception {
        if (type.contains("varchar")) {
            return "String";
        } else if (type.contains("char")) {
            return "String";
        } else if (type.contains("text")) {
            return "String";
        } else if (type.contains("datetime")) {
            return "Date";
        } else if (type.contains("date")) {
            return "Date";
        } else if (type.contains("bigint")) {
            return "Long";
        } else if (type.contains("int")) {
            return "Integer";
        } else if (type.contains("decimal")) {
            return "BigDecimal";
        } else if (type.contains("double")) {
            return "Double";
        } else if (type.contains("float")) {
            return "Float";
        } else if (type.contains("mediumtext")) {
            return "String";
        } else {
            throw new Exception("未能找到匹配类型：" + type);
        }
    }

    /**
     * 获取长度
     */
    private static Integer getLength(String type) {
        if (type.contains("(")) {
            String length = type.substring(type.indexOf("(") + 1, type.indexOf(")"));
            return Integer.parseInt(length.split(",")[0]);
        } else {
            return null;
        }
    }

    /**
     * 获取注释
     */
    private static void setComment(GeneratorModel gm, ColumnModel vo, String comment) throws Exception {
        // 判断是否写字段注释
        if (StringUtils.isBlank(comment)) {
            throw new Exception(">>>>>>>>>>" + gm.getTableName() + "中" + vo.getColEnNameLower() + "字段未写字段注释！");
        }

        // 判断是否为图片
        if (comment.contains("[img]")) {
            vo.setColIsImg(true);
            vo.setColCnName(comment.replace("[img]", ""));
        } else {
            vo.setColCnName(comment);
        }
    }
}
