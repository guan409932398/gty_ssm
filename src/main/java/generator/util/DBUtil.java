package generator.util;

import generator.AiCode;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBUtil {
    // 连接数据库的路径
    private static String url;
    // 连接数据库的用户名
    private static String user;
    // 连接数据库的密码
    private static String pwd;

    // 静态块
    static {
        try {
            // 获取地址
            url = AiCode.DB_URL;
            // 获取用户名
            user = AiCode.DB_USER;
            // 获取密码
            pwd = AiCode.DB_PASSWORD;
            // 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取一个连接
     */
    public static Connection getConnection() throws Exception {
        try {
            /*
             * 通过DriverManager创建一个数据库的连接 并返回
             */
            return DriverManager.getConnection(url, user, pwd);
        } catch (Exception e) {
            e.printStackTrace();
            // 通知调用者，创建连接出错
            throw e;
        }
    }

    /**
     * 关闭给定的连接
     */
    public static void closeConnection(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试是否连接成功
     */
    public static void main(String[] args) throws Exception {
        System.out.println(getConnection());
    }
}
