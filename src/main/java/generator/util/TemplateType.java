package generator.util;

/**
 * 模版对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190615
 */
public enum TemplateType {
    ENTITY("\\src\\main\\java\\generator\\template\\model\\", "entity.ftl", "\\src\\main\\java\\com\\", "Entity.java"),
    MAPPER("\\src\\main\\java\\generator\\template\\mapper\\", "mapper.ftl", "\\src\\main\\java\\com\\", "Mapper.java"),
    SERVICE("\\src\\main\\java\\generator\\template\\service\\", "service.ftl", "\\src\\main\\java\\com\\", "Service.java"),
    SERVICE_IMPL("\\src\\main\\java\\generator\\template\\service\\", "serviceImpl.ftl", "\\src\\main\\java\\com\\", "ServiceImpl.java"),
    CONTROLLER("\\src\\main\\java\\generator\\template\\controller\\", "controller.ftl", "\\src\\main\\java\\com\\", "Controller.java"),

    QUERY("\\src\\main\\java\\generator\\template\\page\\", "query.ftl", "\\src\\main\\webapp\\WEB-INF\\page\\", "_query.jsp"),
    QUERY_JS("\\src\\main\\java\\generator\\template\\page\\", "queryJs.ftl", "\\src\\main\\webapp\\WEB-INF\\page\\", "_query.js"),
    EDITOR("\\src\\main\\java\\generator\\template\\page\\", "editor.ftl", "\\src\\main\\webapp\\WEB-INF\\page\\", "_editor.jsp"),
    EDITOR_JS("\\src\\main\\java\\generator\\template\\page\\", "editorJs.ftl", "\\src\\main\\webapp\\WEB-INF\\page\\", "_editor.js"),
    DETAIL("\\src\\main\\java\\generator\\template\\page\\", "detail.ftl", "\\src\\main\\webapp\\WEB-INF\\page\\", "_detail.jsp");

    // 模版文件夹
    private String dir;
    // 模版名称
    private String name;
    // 模版前缀
    private String prefix;
    // 模版后缀
    private String suffix;

    TemplateType(String dir, String name, String prefix, String suffix) {
        this.dir = dir;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String getDir() {
        return dir;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }
}
