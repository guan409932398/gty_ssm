package generator;

import generator.model.GeneratorModel;
import generator.service.impl.GeneratorSerImpl;
import generator.util.TableUtil;
import generator.util.TemplateType;

/**
 * @author 官天野 QQ:409932398
 * @version v2.0
 * @title: 代码生成器
 * @email guan409932398@qq.com
 * @date 2019-01-24 14:26:23
 * @attention </br>
 * 1. 只支持mysql</br>
 * 2. 生成表主键必须为varchar(32)类型<br/>
 * 3. 生成表只允许单列主键 <br/>
 * 4. MODEL_NAME 不允许出现特殊字符<br/>
 * 5. 表名必须为：【前缀_功能名】命名，前缀：【tb_模块名_】<br/>
 * 6. 表名中的功能名: 尽量为单个单词（例: tb_oa_meetinffg , tb_shop_goods）<br/>
 * 7. 表中必须存在 create_time(datetime) 字段（用于生成默认排序）<br/>
 * 8. 列中文名尽量4-5个字，否则生成出页面的样式会错位。<br/>
 * @see <br>
 * v1.0 create file <br/>
 * v2.0 变更为模版模式生成代码，依赖freemarker.jar<br/>
 */
public class AiCode {
    // 模块名称
    public final static String MODEL_NAME = "fxjc";
    // 表名
    public final static String TABLE_NAME = "tb_fxjc_fxproject";
    // 数据库地址
    public final static String DB_URL = "jdbc:mysql://localhost:3306/gty_ssm?characterEncoding=utf-8";
    // 数据库用户名
    public final static String DB_USER = "root";
    // 数据库密码
    public final static String DB_PASSWORD = "root";
    // 数据库模式
    public final static String DB_SCHEMA = "gty_ssm";

    public static void main(String[] args) throws Exception {
        // 创建功能模型对象
        GeneratorModel gm = new GeneratorModel("gty", "官天野", "guan409932398@qq.com", MODEL_NAME, TABLE_NAME);
        // 初始化数据
        TableUtil.InitData(gm);

        // 生成 entity
        new GeneratorSerImpl().generate(gm, TemplateType.ENTITY);
        // 生成 mapper
        new GeneratorSerImpl().generate(gm, TemplateType.MAPPER);
        // 生成 service
        new GeneratorSerImpl().generate(gm, TemplateType.SERVICE);
        // 生成 service impl
        new GeneratorSerImpl().generate(gm, TemplateType.SERVICE_IMPL);
        // 生成 controller
        new GeneratorSerImpl().generate(gm, TemplateType.CONTROLLER);
        // 生成 query
        new GeneratorSerImpl().generate(gm, TemplateType.QUERY);
        // 生成 query js
        new GeneratorSerImpl().generate(gm, TemplateType.QUERY_JS);
        // 生成 editor
        new GeneratorSerImpl().generate(gm, TemplateType.EDITOR);
        // 生成 editor js
        new GeneratorSerImpl().generate(gm, TemplateType.EDITOR_JS);
        // 生成 detail
        new GeneratorSerImpl().generate(gm, TemplateType.DETAIL);
    }
}