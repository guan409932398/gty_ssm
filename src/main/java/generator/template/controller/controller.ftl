package com.${companyEnName }.${modelName }.controller;

import com.${companyEnName }.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.${companyEnName }.global.result.Result;
import com.${companyEnName }.${modelName }.model.${funEnName }Entity;
import com.${companyEnName }.${modelName }.service.${funEnName }Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * ${funCnName } Controller
 *
 * @author ${author }
 * @version v1.0
 * @email: ${email }
 * @date ${createTime }
 */
@Controller
@RequestMapping("/${funEnNameLower }")
public class ${funEnName }Controller {

    @Autowired
    private ${funEnName }Service service;

    /**
     * 跳转${funCnName }新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/${modelName }/${funEnNameLower }/${funEnNameLower }_editor";
    }

    /**
     * 跳转${funCnName }编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/${modelName }/${funEnNameLower }/${funEnNameLower }_editor";
    }

    /**
     * 跳转${funCnName }列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/${modelName }/${funEnNameLower }/${funEnNameLower }_query";
    }

    /**
     * 跳转${funCnName }明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/${modelName }/${funEnNameLower }/${funEnNameLower }_detail";
    }

    /**
     * ${funCnName }保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(${funEnName }Entity entity) throws CustomException {
        return Result.success(service.save(entity));
    }
    
    /**
     * ${funCnName }删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * ${funCnName }明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = ${funEnName }Entity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }
    
    /**
     * ${funCnName }列表
     */
    @RequestMapping("/query")
    @JSON(type = ${funEnName }Entity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(${funEnName }Entity entity) {
        Map<String, Object> map = service.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }
}
