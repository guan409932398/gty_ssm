layui.config({
    base : "js/"
}).use(['form','layer','jquery','laydate','upload'],function(){
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var laydate = layui.laydate;
    var layer = parent.layer === undefined ? layui.layer : parent.layer;
    
    form.on("submit(submit_but)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        var msg="发生错误！",flag=false;

        $.ajax({
            type: "post",
            url: ctx + "/${funEnNameLower }/save",
            data:data.field,
            dataType:"json",
            success:function(d){
            	top.layer.close(index);
            	
            	if(d.code != '200'){
            		top.layer.msg("(" + d.msg + ")");
            	}else{
            		layer.closeAll("iframe");
            	}
			},
            error:function() {
                flag=true;
                top.layer.close(index);
                $("#this_form")[0].reset();
                layer.msg("发生错误，请检查输入！");
            }
        });

        return false;
    });
    
    <#list cols as col>
    	<#if ((col.colIsImg)?string("true","flase")) == 'true'>
	upload.render({
    	elem: '#${col.colEnNameLower }_upload_but',
    	url: ctx + '/file/upload',
    	exts: 'jpg|jpeg|bmp|png',
    	size: 10240,
    	before: function(obj){
    		layer.load(2, {shade: [0.8]});
    	},done: function(res){
    		layer.closeAll('loading');
    		
	        if(res.code == 200){
		    	$('#${col.colEnNameLower }').val(res.data);
		    	
		    	var oss_service_providers = $('#oss_service_providers').val();
		    	
		    	if(oss_service_providers == 'aliyun'){
		    		$('#${col.colEnNameLower }_show').html('<a href="' + $('#alioss_outLink').val() + res.data + '" target="_blank" title="点击查看"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
	        	} else if(oss_service_providers == 'qiniu'){
	        		$('#${col.colEnNameLower }_show').html('<a href="' + $('#qiniu_outLink').val() + res.data + '" target="_blank" title="点击查看"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
	        	} else if(oss_service_providers == 'server'){
	        		$('#${col.colEnNameLower }_show').html('<a href="' + ctx + "/file/show?fileId=" + res.data + '" target="_blank" title="点击查看"><label style="height: 38px;line-height: 38px;">' + res.data + '</label></a>');
	        	}
	        }else{
	        	return layer.msg('上传失败');
	        }
    	},error: function(){
    		layer.closeAll('loading');
    		return layer.msg('上传失败');
    	}
    });
    	</#if >
    </#list>
    
    <#list cols as col>
    	<#if col.colType == 'Date' >
    laydate.render({
        elem: '#${col.colEnNameLower }',
        format: 'yyyy-MM-dd'
	});
    	</#if>
    </#list>

    //$("input[name=radio_sample][value=value_true]").attr("checked", $('#_radio_sample').val() == 'value_true' ? true : false);
    //$("input[name=radio_sample][value=value_false]").attr("checked", $('#_radio_sample').val() == 'value_false' ? true : false);
    //layui.form.render('radio');
    
    //$('#select_sample').val($('#_select_sample').val());
	//layui.form.render('select');
})

// @author ${author }
// @version v1.0
// @email: ${email }
// @date ${createTime }
