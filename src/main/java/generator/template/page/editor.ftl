<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>维护</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="${'$'}{ctx }/css/base.css" />
        <link rel="stylesheet" href="${'$'}{ctx }/layui/css/layui.css" media="all" />

        <script>
            var ctx = "${'$'}{ctx}";
        </script>
    </head>

	<body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
        	<input type="hidden" id="oss_service_providers" value="${'$'}{elfunc:getCon('oss_service_providers')}">
        	<input type="hidden" id="qiniu_outLink" value="${'$'}{elfunc:getCon('qiniu_outLink')}">
        	<input type="hidden" id="alioss_outLink" value="${'$'}{elfunc:getCon('alioss_outLink')}">
        
            <input type="hidden" id="${pkEnNameLower }" name="${pkEnNameLower }" value="${'$'}{po.${pkEnNameLower } }" >
			
            <#list cols as col>
            	<#if col.colDbNameLower != 'create_time' && col.colDbNameLower != 'modify_time' && col.colDbNameLower != 'record_state' && col.colDbNameLower != 'id' >
            <div class="layui-form-item">
                <label class="layui-form-label">${col.colCnName }</label>
                <div class="layui-input-block">
                	<#if col.colType == 'BigDecimal' >
                	<input type="text" id="${col.colEnNameLower }" class="layui-input" placeholder="请输入${col.colCnName }" name="${col.colEnNameLower }" value="${'$'}{po.${col.colEnNameLower } }" maxlength="${col.colLength }" lay-verify="number">
                	<#elseif col.colType == 'Date' >
                	<input type="text" id="${col.colEnNameLower }" class="layui-input" placeholder="请输入${col.colCnName }" name="${col.colEnNameLower }" value="<fmt:formatDate value="${'$'}{po.${col.colEnNameLower } }" pattern="yyyy-MM-dd"/>">
                	<#elseif col.colType == 'String' >
                		<#if ((col.colIsImg)?string("true","flase")) == 'true'>
                	<input type="hidden" id="${col.colEnNameLower }" name="${col.colEnNameLower }" value="${'$'}{po.${col.colEnNameLower } }">
                	
                	<button type="button" class="layui-btn layui-bg-blue" id="${col.colEnNameLower }_upload_but" style="display: inline;">
                		<i class="layui-icon">&#xe67c;</i> 上传
                	</button>

                	<div class="layui-upload-list" id="${col.colEnNameLower }_show" style="display: inline;">
                		<c:if test="${'$'}{not empty po.${col.colEnNameLower } }">
                			<c:if test="${'$'}{elfunc:getCon('oss_service_providers') eq 'server' }">
								<a href="${'$'}{ctx }/file/show?file_id=${'$'}{po.${col.colEnNameLower } }" target="_blank" title="点击查看大图"> 
									<label style="height: 38px;line-height: 38px;">
										${'$'}{po.${col.colEnNameLower } }
									</label>
								</a>
							</c:if>
							
							<c:if test="${'$'}{elfunc:getCon('oss_service_providers') eq 'qiniu' }">
								<a href="${'$'}{elfunc:getCon('qiniu_outLink')}${'$'}{po.${col.colEnNameLower } }" target="_blank" title="点击查看大图"> 
									<label style="height: 38px;line-height: 38px;">
										${'$'}{po.${col.colEnNameLower } }
									</label>
								</a>
							</c:if>
                		
                			<c:if test="${'$'}{elfunc:getCon('oss_service_providers') eq 'aliyun' }">
	                			<a href="${'$'}{elfunc:getCon('alioss_outLink')}${'$'}{po.${col.colEnNameLower } }" target="_blank" title="点击查看"> 
	                				<label style="height: 38px;line-height: 38px;">
	                					${'$'}{po.${col.colEnNameLower } } 
	                				</label>
	                			</a>
                			</c:if>
						</c:if>
                	</div>
                		<#else >
	                		<#if (col.colLength)!??>
	                			<#if col.colLength lt 500 >
                	<input type="text" id="${col.colEnNameLower }" class="layui-input" placeholder="请输入${col.colCnName }" name="${col.colEnNameLower }" value="${'$'}{po.${col.colEnNameLower } }">
	                    		<#else>
                    <textarea id="${col.colEnNameLower }" name="${col.colEnNameLower }" placeholder="请输入${col.colCnName }" class="layui-textarea">${'$'}{po.${col.colEnNameLower } }</textarea>
	                    		</#if>
	                    	<#else>
                    <input type="text" id="${col.colEnNameLower }" class="layui-input" placeholder="请输入${col.colCnName }" name="${col.colEnNameLower }" value="${'$'}{po.${col.colEnNameLower } }" maxlength="${col.colLength }" >
	                    	</#if>
                		</#if>
                	<#else>
                	<input type="text" id="${col.colEnNameLower }" class="layui-input" placeholder="请输入${col.colCnName }" name="${col.colEnNameLower }" value="${'$'}{po.${col.colEnNameLower } }" >
                	</#if>
                </div>
            </div>
            
            	</#if>
            </#list>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit_but">立即提交</button>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="${'$'}{ctx }/layui/layui.js"></script>
        <script type="text/javascript" src="${'$'}{ctx }/page/${modelName }/${funEnNameLower }/${funEnNameLower }_editor.js"></script>
        
        <script type="text/javascript">
            // 回车提交表单
            document.onkeydown = function (e) { 
                var theEvent = window.event || e; 
                var code = theEvent.keyCode || theEvent.which; 

                if (code == 13) { 
                    return false;
                } 
            }  
        </script>
    </body>
</html>

<!-- @author ${author } -->
<!-- @version v1.0 -->
<!-- @email: ${email } -->
<!-- @date ${createTime } -->

