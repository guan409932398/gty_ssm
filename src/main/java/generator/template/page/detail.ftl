<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>明细</title>
        
        <meta charset="utf-8">
        <meta name="renderer" content="webkit">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <link rel="stylesheet" href="${'$'}{ctx }/layui/css/layui.css" media="all" />
        <link rel="stylesheet" type="text/css" href="${'$'}{ctx }/plugins/Tags/css/tag.css" />
        <link rel="stylesheet" href="${'$'}{ctx }/css/font_eolqem241z66flxr.css" media="all" />

        <script>
            var ctx = "${'$'}{ctx}";
        </script>

        <style type="text/css">
            .layui-form-item .layui-inline {width: 33.333%; float: left; margin-right: 0;}
            .labStyle {height: 38px;line-height: 38px; }
        </style>

    </head>

    <body class="childrenBody">
        <form class="layui-form" style="width: 80%;" id="this_form">
            <input type="hidden" id="${pkEnNameLower }" class="layui-input" name="${pkEnNameLower }" value="${'$'}{po.${pkEnNameLower } }" maxlength="32" >

            <#list cols as col>
                <#if col.colDbNameLower != 'create_time' && col.colDbNameLower != 'modify_time' && col.colDbNameLower != 'record_state' && col.colDbNameLower != 'id' >
            <div class="layui-form-item">
                <label class="layui-form-label">${col.colCnName }</label>
                <div class="layui-input-block">
                	<#if col.colType == 'Date' >
                	<label class="labStyle" ><fmt:formatDate value="${'$'}{po.${col.colEnNameLower } }" pattern="yyyy-MM-dd"/></label>
                	<#else>
                	<label class="labStyle" >${'$'}{po.${col.colEnNameLower } }</label>
                	</#if>
                </div>
            </div>
            
                </#if>
            </#list>
        </form>

        <script type="text/javascript" src="${'$'}{ctx }/layui/layui.js"></script>
    </body>
</html>

<!-- @author ${author } -->
<!-- @version v1.0 -->
<!-- @email: ${email } -->
<!-- @date ${createTime } -->