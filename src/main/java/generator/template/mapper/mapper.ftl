package com.${companyEnName }.${modelName }.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.${companyEnName }.${modelName }.model.${funEnName }Entity;

/**
 * ${funCnName } Mapper
 *
 * @author ${author }
 * @version v1.0
 * @email: ${email }
 * @date ${createTime }
 */
public interface ${funEnName }Mapper extends BaseMapper<${funEnName }Entity> {
}
