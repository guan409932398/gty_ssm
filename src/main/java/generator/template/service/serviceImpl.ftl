package com.${companyEnName }.${modelName }.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.${companyEnName }.${modelName }.mapper.${funEnName }Mapper;
import com.${companyEnName }.${modelName }.model.${funEnName }Entity;
import com.${companyEnName }.${modelName }.service.${funEnName }Service;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ${funCnName } Service Impl
 *
 * @author ${author }
 * @version v1.0
 * @email: ${email }
 * @date ${createTime }
 */
@Service
public class ${funEnName }ServiceImpl extends ServiceImpl<${funEnName }Mapper, ${funEnName }Entity> implements ${funEnName }Service {
    // Mapper对象
    @Autowired
    private ${funEnName }Mapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(${funEnName }Entity entity) {
        if (StringUtils.isBlank(entity.get${pkEnName }())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(${funEnName }Entity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<${funEnName }Entity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
    * 组装查询条件
    */
    private EntityWrapper<${funEnName }Entity> getWrapper(${funEnName }Entity entity) {
        EntityWrapper<${funEnName }Entity> wrapper = new EntityWrapper<>();

        wrapper.orderBy("create_time");

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
