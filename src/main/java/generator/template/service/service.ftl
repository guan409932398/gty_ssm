package com.${companyEnName }.${modelName }.service;

import com.baomidou.mybatisplus.service.IService;
import com.${companyEnName }.global.exception.CustomException;
import com.${companyEnName }.${modelName }.model.${funEnName }Entity;

import java.util.Map;

/**
 * ${funCnName } Service
 *
 * @author ${author }
 * @version v1.0
 * @email: ${email }
 * @date ${createTime }
 */
public interface ${funEnName }Service extends IService<${funEnName }Entity> {

    /**
     * 保存对象
     */
    boolean save(${funEnName }Entity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(${funEnName }Entity entity);
}
