package com.${companyEnName }.${modelName }.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.${companyEnName }.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

<#list cols as col>
    <#if col.colType='BigDecimal'>
import java.math.BigDecimal;
        <#break>
    </#if>
</#list>
<#list cols as col>
    <#if col.colType='Date'>
import java.util.Date;
        <#break>
    </#if>
</#list>

/**
* ${funCnName } Entity
*
* @author ${author }
* @version v1.0
* @email: ${email }
* @date ${createTime }
*/
@TableName("${funDbName }")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ${funEnName }Entity extends BasePO {
    private static final long serialVersionUID = 1L;

    <#list cols as col>
        <#if col.colDbNameLower != 'create_time' && col.colDbNameLower != 'modify_time' && col.colDbNameLower != 'record_state' && col.colDbNameLower != 'id' >
    // ${col.colCnName }
    @TableField("${col.colDbNameLower }")
    <#if col.colType == 'Date' >
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    </#if>
    private ${col.colType } ${col.colEnNameLower };
        </#if>
    </#list>
}
