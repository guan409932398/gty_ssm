package generator.service.impl;

import cn.hutool.core.io.FileUtil;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import generator.model.GeneratorModel;
import generator.service.GeneratorSer;
import generator.util.TemplateType;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

/**
 * 代码生成
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public class GeneratorSerImpl implements GeneratorSer {

    /**
     * 根据数据模版生成控制层文件
     */
    @Override
    public void generate(GeneratorModel gm, TemplateType tempType) throws Exception {
        Configuration configure = new Configuration();
        configure.setDefaultEncoding("utf-8");

        // 加载模板文件
        configure.setDirectoryForTemplateLoading(new File(System.getProperty("user.dir") + tempType.getDir()));
        // 设置对象包装器
        configure.setObjectWrapper(new DefaultObjectWrapper());
        // 设置异常处理器
        configure.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
        // 加载需要装填的模板
        Template template = configure.getTemplate(tempType.getName());

        Writer writer = new FileWriter(getFilePath(tempType, gm));

        template.process(gm, writer);
    }

    /**
     * 创建文件夹及文件
     */
    private File getFilePath(TemplateType tempType, GeneratorModel gm) {
        StringBuilder sb = new StringBuilder(System.getProperty("user.dir"));

        switch (tempType) {
            case ENTITY:
                sb.append(tempType.getPrefix());

                sb.append(gm.getCompanyEnName());
                sb.append(File.separatorChar);
                sb.append(gm.getModelName());
                sb.append(File.separatorChar);
                sb.append("model");
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnName());

                sb.append(tempType.getSuffix());
                break;
            case MAPPER:
                sb.append(tempType.getPrefix());

                sb.append(gm.getCompanyEnName());
                sb.append(File.separatorChar);
                sb.append(gm.getModelName());
                sb.append(File.separatorChar);
                sb.append("mapper");
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnName());

                sb.append(tempType.getSuffix());
                break;
            case SERVICE:
                sb.append(tempType.getPrefix());

                sb.append(gm.getCompanyEnName());
                sb.append(File.separatorChar);
                sb.append(gm.getModelName());
                sb.append(File.separatorChar);
                sb.append("service");
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnName());

                sb.append(tempType.getSuffix());
                break;
            case SERVICE_IMPL:
                sb.append(tempType.getPrefix());

                sb.append(gm.getCompanyEnName());
                sb.append(File.separatorChar);
                sb.append(gm.getModelName());
                sb.append(File.separatorChar);
                sb.append("service");
                sb.append(File.separatorChar);
                sb.append("impl");
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnName());

                sb.append(tempType.getSuffix());
                break;
            case CONTROLLER:
                sb.append(tempType.getPrefix());

                sb.append(gm.getCompanyEnName());
                sb.append(File.separatorChar);
                sb.append(gm.getModelName());
                sb.append(File.separatorChar);
                sb.append("controller");
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnName());

                sb.append(tempType.getSuffix());
                break;
            case QUERY:
            case QUERY_JS:
            case EDITOR:
            case EDITOR_JS:
            case DETAIL:
                sb.append(tempType.getPrefix());

                sb.append(gm.getModelName());
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnNameLower());
                sb.append(File.separatorChar);
                sb.append(gm.getFunEnNameLower());

                sb.append(tempType.getSuffix());
                break;
            default:
        }

        String path = sb.toString();

        if (FileUtil.exist(path)) {
            FileUtil.del(path);
        }

        // 新建文件
        return FileUtil.touch(path);
    }
}
