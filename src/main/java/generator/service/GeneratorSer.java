package generator.service;

import generator.model.GeneratorModel;
import generator.util.TemplateType;

/**
 * @title: 代码生成服务接口
 * @author 官天野 QQ:409932398
 * @email guan409932398@qq.com
 * @date 2019-01-24 14:26:23
 * @version v1.0
 * @see <br>
 *      v1.0 create file <br/>
 */
public interface GeneratorSer {
	/**
	 * 根据数据模版生成文件
	 * 
	 * @param gm
	 */
	public void generate(GeneratorModel gm, TemplateType tempType) throws Exception;
}
