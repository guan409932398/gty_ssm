package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.MenuEntity;
import com.gty.sys.model.XtreeEntity;

import java.util.List;
import java.util.Map;

/**
 * 菜单表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface MenusService extends IService<MenuEntity> {
    /**
     * 保存对象
     */
    boolean save(MenuEntity entity) throws CustomException;

    /**
     * 删除菜单
     */
    boolean del(String id) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(MenuEntity entity);

    /**
     * 查询列表
     */
    List<MenuEntity> queryALLHierarchy(MenuEntity entity);

    /**
     * 根据角色菜单层级查询列表
     */
    List<MenuEntity> queryJoinRoleHierarchy(MenuEntity entity);

    /**
     * 查询列表
     */
    List<XtreeEntity> queryXtree(MenuEntity entity);
}
