package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.RoleMenuEntity;

/**
 * 角色菜单表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface RolesMenusService extends IService<RoleMenuEntity> {

    /**
     * 保存对象
     */
    boolean addAll(String role_id, String menu_ids);

    /**
     * 删除对象
     */
    boolean delByRoles(String role_id) throws CustomException;
}
