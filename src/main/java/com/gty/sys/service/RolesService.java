package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.RoleEntity;

import java.util.Map;

/**
 * 角色表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface RolesService extends IService<RoleEntity> {

    /**
     * 保存对象
     */
    boolean save(RoleEntity entity) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(RoleEntity entity);
}
