package com.gty.sys.service;

import com.gty.global.exception.CustomException;

/**
 * 通用功能 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface CommonService {

    /**
     * 发送短信
     */
    int sendSms(String phone, String smsModel) throws CustomException;

    /**
     * 获取OSStoken
     */
    String getOssToken(String ossType) throws CustomException;
}
