package com.gty.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.exception.CustomException;
import com.gty.sys.mapper.RolesMapper;
import com.gty.sys.model.RoleEntity;
import com.gty.sys.service.RolesMenusService;
import com.gty.sys.service.RolesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class RolesServiceImpl extends ServiceImpl<RolesMapper, RoleEntity> implements RolesService {
    // Mapper对象
    @Autowired
    private RolesMapper mapper;
    // 角色菜单
    @Autowired
    private RolesMenusService rmSer;

    /**
     * 保存对象
     */
    @Override
    public boolean save(RoleEntity entity) throws CustomException {
        boolean result;

        EntityWrapper<RoleEntity> ew = new EntityWrapper<>();
        ew.eq("name", entity.getName());

        // 判断是否存在重名的角色
        RoleEntity role = selectOne(ew);

        if (StringUtils.isBlank(entity.getId())) {
            // 判断是否存在重复角色
            if (role != null && StringUtils.isNotBlank(role.getId())) {
                throw new CustomException("角色名已存在");
            }

            result = insert(entity);
        } else {
            // 判断是否存在重复角色
            if (role != null && StringUtils.isNotBlank(role.getId())
                    && !entity.getId().equals(role.getId())) {
                throw new CustomException("角色名已存在");
            }

            // 修改
            result = updateById(entity);
        }

        // 保存成功后修改角色菜单
        if (result) {
            if (StringUtils.isNotBlank(entity.getId())) {
                // 清空旧角色菜单
                rmSer.delByRoles(entity.getId());
            }

            if (StringUtils.isNotBlank(entity.getMenuIds())) {
                // 保存新角色菜单
                rmSer.addAll(entity.getId(), entity.getMenuIds());
            }
        }

        return result;
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(RoleEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<RoleEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<RoleEntity> getWrapper(RoleEntity entity) {
        EntityWrapper<RoleEntity> wrapper = new EntityWrapper<>();

        if (entity == null) {
            return wrapper;
        }

        if (StringUtils.isNotBlank(entity.getName())) {
            wrapper.like("name", entity.getName());
        }

        return wrapper;
    }
}
