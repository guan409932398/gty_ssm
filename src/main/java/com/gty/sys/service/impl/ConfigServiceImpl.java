package com.gty.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.sys.mapper.ConfigMapper;
import com.gty.sys.model.ConfigEntity;
import com.gty.sys.service.ConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 配置表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, ConfigEntity> implements ConfigService {

    // Mapper对象
    @Autowired
    private ConfigMapper mapper;

    /**
     * 更新缓存
     */
    public String refresh(String code) {
        if (StringUtils.isBlank(code)) {
            GlobalCache.CONFIG_CACHE.cleanUp();
            return "全局配置缓存（全部）已清理";
        } else {
            GlobalCache.CONFIG_CACHE.invalidate(code);
            return "全局配置缓存（" + code + "）已清理";
        }
    }

    /**
     * 保存对象
     */
    @Override
    public boolean save(ConfigEntity entity) throws CustomException {
        // 校验 code
        if (entity == null || StringUtils.isBlank(entity.getCode())) {
            throw new CustomException("code不能为空");
        } else {
            EntityWrapper<ConfigEntity> ew = new EntityWrapper<>();
            ew.eq("code", entity.getCode());
            ConfigEntity config = selectOne(ew);

            if (config != null && StringUtils.isNotBlank(config.getId()) && !config.getId().equals(entity.getId())) {
                throw new CustomException("配置代码重复!");
            } else if (StringUtils.isBlank(entity.getId())) {
                return insert(entity);
            } else {
                // 删除缓存
                GlobalCache.CONFIG_CACHE.refresh(entity.getCode());
                // 更新DB
                return updateById(entity);
            }
        }
    }

    /**
     * 删除对象
     */
    @Override
    public int delByCode(String confCode) {
        // 删除缓存
        GlobalCache.CONFIG_CACHE.invalidate(confCode);

        // 删除DB
        return mapper.deleteByMap(new HashMap<String, Object>() {{
            put("code", confCode);
        }});
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(ConfigEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<ConfigEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<ConfigEntity> getWrapper(ConfigEntity entity) {
        EntityWrapper<ConfigEntity> wrapper = new EntityWrapper<>();

        if (entity == null) {
            return wrapper;
        }

        if (StringUtils.isNotBlank(entity.getCode())) {
            wrapper.like("code", entity.getCode());
        }

        if (StringUtils.isNotBlank(entity.getName())) {
            wrapper.like("name", entity.getName());
        }

        if (StringUtils.isNotBlank(entity.getCategory())) {
            wrapper.eq("category", entity.getCategory());
        }

        if (StringUtils.isNotBlank(entity.getType())) {
            wrapper.eq("type", entity.getType());
        }

        if (StringUtils.isNotBlank(entity.getSearchKey())) {
            wrapper.in("code", entity.getSearchKey().split(","));
        }

        wrapper.orderBy("create_time");

        return wrapper;
    }
}

