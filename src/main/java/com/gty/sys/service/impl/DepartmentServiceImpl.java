package com.gty.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.sys.mapper.DepartmentMapper;
import com.gty.sys.model.DepartmentEntity;
import com.gty.sys.service.DepartmentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公司部门表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-26 11:12:00
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, DepartmentEntity> implements DepartmentService {
    // Mapper对象
    @Autowired
    private DepartmentMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(DepartmentEntity entity) {
        if (StringUtils.isBlank(entity.getId())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(DepartmentEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<DepartmentEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
    * 组装查询条件
    */
    private EntityWrapper<DepartmentEntity> getWrapper(DepartmentEntity entity) {
        EntityWrapper<DepartmentEntity> wrapper = new EntityWrapper<>();

        wrapper.orderBy("create_time");

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
