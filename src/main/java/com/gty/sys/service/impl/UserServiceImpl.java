package com.gty.sys.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.enums.SqlLike;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.shiro.ShiroUtils;
import com.gty.sys.mapper.UserMapper;
import com.gty.sys.model.SmsEntity;
import com.gty.sys.model.UserEntity;
import com.gty.sys.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 用户表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {
    // Mapper对象
    @Autowired
    private UserMapper mapper;

    /**
     * 用户注册
     */
    @Override
    public boolean register(UserEntity entity) throws CustomException {
        return register(entity, null);
    }

    /**
     * 用户注册
     */
    private boolean register(UserEntity entity, String back) throws CustomException {
        // 校验必填项数据
        if (StringUtils.isBlank(entity.getPhone())) {
            throw new CustomException("手机号不能为空");
        } else if (StringUtils.isBlank(entity.getPassword())) {
            throw new CustomException("密码不能为空");
        } else {
            // 用户手机号验重(用户手机号必须存在)
            if (StringUtils.isNotBlank(entity.getPhone())) {
                // 根据手机号查询库中用户手机
                EntityWrapper<UserEntity> ew = new EntityWrapper<>();
                ew.eq("phone", entity.getPhone());
                UserEntity user = selectOne(ew);

                // 验重
                if (user != null && StringUtils.isNotBlank(user.getId())) {
                    throw new CustomException("用户手机号已存在");
                } else {
                    // 校验手机验证码
                    if (!"close".equals(GlobalCache.CONFIG_CACHE.get("sms_switch")) && !"back".equals(back)) {
                        // 查询短信对象
                        SmsEntity sms = GlobalCache.SMS_CACHE.getIfPresent(entity.getPhone());

                        if ((sms == null) || (StringUtils.isBlank(sms.getSmsCode()))) {
                            throw new CustomException("验证码已过期或不存在！");
                        } else if (!entity.getSecurityCode().equalsIgnoreCase(sms.getSmsCode())) {
                            throw new CustomException("验证码错误！");
                        }
                    }
                }
            }

            // 用户帐号存在则进行验重
            if (StringUtils.isNotBlank(entity.getAccount())) {
                // 根据帐号查询库中用户
                EntityWrapper<UserEntity> ew = new EntityWrapper<>();
                ew.eq("account", entity.getAccount());
                UserEntity user = selectOne(ew);

                // 验重
                if (user != null && StringUtils.isNotBlank(user.getId())) {
                    throw new CustomException("用户帐号已存在");
                }
            } else {
                // 获取用户帐号生成规则规格
                String genType = GlobalCache.CONFIG_CACHE.get("accouont_generation_type");

                if ("number_10".equals(genType)) {
                    entity.setAccount(getOnlyAccount());
                } else if ("phone".equals(genType)) {
                    entity.setAccount(entity.getPhone());
                } else {
                    throw new CustomException("错误的帐号生成规则");
                }
            }

            // 用户状态不存在则默认设置
            if (StringUtils.isBlank(entity.getState())) {
                entity.setState("active");
            }

            // 对用户密码进行MD5摘要
            entity.setPassword(new MD5().digestHex(entity.getPassword()));

            return insert(entity);
        }
    }

    /**
     * 保存对象
     */
    @Override
    public boolean save(UserEntity entity) throws CustomException {
        return save(entity, null);
    }

    /**
     * 保存对象
     */
    @Override
    public boolean save(UserEntity entity, String back) throws CustomException {
        // 判断创建日期是否为空
        if (StringUtils.isBlank(entity.getId())) {
            // 设置一个随机密码
            entity.setPassword(UUID.randomUUID().toString().replaceAll("-", ""));
            return register(entity, back);
        } else {
            // 修改
            return updateById(entity);
        }
    }

    /**
     * 获取唯一账号
     */
    private String getOnlyAccount() {
        // 注意尽量不要设置为生成11位（手机号位数）的帐号，可能会存在冲突
        String account = RandomUtil.randomString(RandomUtil.BASE_NUMBER, 10);

        // 根据手机号查询库中用户手机
        EntityWrapper<UserEntity> ew = new EntityWrapper<>();
        ew.eq("account", account);

        UserEntity user = selectOne(ew);

        if (user == null || StringUtils.isBlank(user.getId())) {
            return account;
        } else {
            return getOnlyAccount();
        }
    }

    /**
     * 用户登录
     */
    @Override
    public UserEntity login(UserEntity entity) throws CustomException {
        // 获取Shiro中验证码
        String kaptcha = ShiroUtils.getKaptcha("kaptcha").toLowerCase();

        // 校验信息
        if (StringUtils.isBlank(kaptcha)) {
            throw new CustomException("验证码错误");
        } else if (StringUtils.isBlank(entity.getAccount())) {
            throw new CustomException("帐号不能为空");
        } else if (StringUtils.isBlank(entity.getPassword())) {
            throw new CustomException("密码不能为空");
        } else if (StringUtils.isBlank(entity.getSecurityCode())) {
            throw new CustomException("验证码不能为空");
        } else if (!kaptcha.equals(entity.getSecurityCode().toLowerCase())) {
            throw new CustomException("验证码错误");
        } else {
            // 用户校验
            Subject subject = ShiroUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(entity.getAccount(), entity.getPassword());
            subject.login(token);
            return (UserEntity) SecurityUtils.getSubject().getPrincipal();
        }
    }

    /**
     * 根据userid和旧密码修改密码
     */
    @Override
    public boolean changePwd(UserEntity entity) throws CustomException {
        if (StringUtils.isBlank(entity.getId())) {
            throw new CustomException("用户ID不能为空");
        } else if (StringUtils.isBlank(entity.getOldPassword())) {
            throw new CustomException("原密码不能为空");
        } else if (StringUtils.isBlank(entity.getPassword())) {
            throw new CustomException("新密码不能为空");
        } else {
            // 根据ID查询用户
            UserEntity user = mapper.selectById(entity.getId());

            if (StringUtils.isBlank(user.getPassword())) {
                throw new CustomException("该用户未初始化密码");
            } else if (!new MD5().digestHex(entity.getOldPassword()).equals(user.getPassword())) {
                // 旧密码不匹配
                throw new CustomException("旧密码不匹配");
            } else {
                // 修改值,加密新密码
                UserEntity modifyEntity = new UserEntity();
                modifyEntity.setPassword(new MD5().digestHex(entity.getPassword()));

                //修改条件
                EntityWrapper<UserEntity> ew = new EntityWrapper<>();
                ew.eq("id", entity.getId());

                // 更新新密码
                return update(modifyEntity, ew);
            }
        }
    }

    /**
     * 修改用户状态
     */
    @Override
    public boolean changeState(UserEntity entity) {
        // 修改值
        UserEntity modifyEntity = new UserEntity();
        modifyEntity.setState(entity.getState());

        //修改条件
        EntityWrapper<UserEntity> ew = new EntityWrapper<>();
        ew.eq("id", entity.getId());

        return update(modifyEntity, ew);
    }

    /**
     * 重置用户密码
     */
    @Override
    public String resetPwd(String id) throws CustomException {
        // 判断参数是否正确
        if (StringUtils.isBlank(id)) {
            throw new CustomException("id不能为空!");
        } else {
            // 修改值
            UserEntity modifyEntity = new UserEntity();

            String password = RandomUtil.randomString(RandomUtil.BASE_CHAR_NUMBER, 12);

            modifyEntity.setPassword(new MD5().digestHex(password));

            //修改条件
            EntityWrapper<UserEntity> ew = new EntityWrapper<>();
            ew.eq("id", id);

            return update(modifyEntity, ew) ? password : "";
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(UserEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<UserEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<UserEntity> getWrapper(UserEntity entity) {
        EntityWrapper<UserEntity> wrapper = new EntityWrapper();

        if (entity == null) {
            return wrapper;
        }

        if (StringUtils.isNotBlank(entity.getAccount())) {
            wrapper.like("account", entity.getAccount());
        }

        if (StringUtils.isNotBlank(entity.getNickname())) {
            wrapper.like("nickname", entity.getNickname());
        }

        if (StringUtils.isNotBlank(entity.getPhone())) {
            wrapper.like("phone", entity.getPhone(), SqlLike.RIGHT);
        }

        if (StringUtils.isNotBlank(entity.getState())) {
            wrapper.eq("state", entity.getState());
        }

        if (StringUtils.isNotBlank(entity.getType())) {
            wrapper.eq("type", entity.getType());
        }

        return wrapper;
    }
}
