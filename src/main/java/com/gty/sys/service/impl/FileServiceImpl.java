package com.gty.sys.service.impl;

import com.aliyun.oss.OSSClient;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.sys.mapper.FileMapper;
import com.gty.sys.model.FileEntity;
import com.gty.sys.service.FileService;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * 附件表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, FileEntity> implements FileService {
    private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    // Mapper对象
    @Autowired
    private FileMapper mapper;

    /**
     * 文件上传
     */
    @Override
    public List<String> upload(HttpServletRequest request) throws CustomException {
        String type = GlobalCache.CONFIG_CACHE.get("oss_service_providers");

        if ("server".equals(type)) {
            return serverUpload(request);
        } else if ("qiniu".equals(type)) {
            return qiniuUpload(request);
        } else if ("aliyun".equals(type)) {
            return aliUpload(request);
        } else {
            throw new CustomException("无效的对象存储服务商");
        }
    }

    /**
     * 服务器文件上传
     */
    private List<String> serverUpload(HttpServletRequest request) throws CustomException {
        // 从全局变量中获取上传路径
        String IMG_DIR = GlobalCache.CONFIG_CACHE.get("upload_path");

        if (StringUtils.isBlank(IMG_DIR)) {
            throw new CustomException("未设置文件上传路径");
        } else {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
            Set<Entry<String, MultipartFile>> entrySet = fileMap.entrySet();

            List<String> keys = new ArrayList<>();

            for (Entry<String, MultipartFile> entry : entrySet) {
                if (StringUtils.isNotBlank(entry.getValue().getOriginalFilename())) {
                    FileEntity entity = new FileEntity();

                    entity.setName(entry.getValue().getOriginalFilename().trim());
                    entity.setType(entity.getName().substring(entity.getName().lastIndexOf(".") + 1));
                    entity.setPath(IMG_DIR + UUID.randomUUID().toString() + "." + entity.getType());

                    File file = new File(entity.getPath());

                    try {
                        entry.getValue().transferTo(file);
                    } catch (IOException e) {
                        logger.error("文件上传失败(服务器)", e);
                        throw new CustomException("文件上传失败(服务器)");
                    }

                    save(entity);

                    keys.add(entity.getId());
                }
            }

            return keys;
        }
    }

    /**
     * 七牛云上传
     */
    private List<String> qiniuUpload(HttpServletRequest request) throws CustomException {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Set<Entry<String, MultipartFile>> entrySet = fileMap.entrySet();

        List<String> paths = new ArrayList<>();

        for (Entry<String, MultipartFile> entry : entrySet) {
            if (StringUtils.isNotBlank(entry.getValue().getOriginalFilename())) {
                String fileName = entry.getValue().getOriginalFilename().trim();
                String path = UUID.randomUUID().toString().replace("-", "") + "."
                        + fileName.substring(fileName.lastIndexOf(".") + 1);

                // 调用put方法上传
                Response res = null;

                try {
                    res = new UploadManager().put(entry.getValue().getBytes(), path,
                            getUpToken(GlobalCache.CONFIG_CACHE.get("qiniu_bucket")));
                } catch (IOException e) {
                    logger.error("文件上传失败(七牛云)", e);
                    throw new CustomException("文件上传失败(七牛云)");
                }

                if (null != res && res.statusCode == 200) {
                    paths.add(path);
                } else {
                    throw new CustomException("七牛上传异常");
                }
            }
        }

        return paths;
    }

    /**
     * 获取凭证
     */
    private String getUpToken(String bucketName) {
        return Auth.create(GlobalCache.CONFIG_CACHE.get("qiniu_accessKey"),
                GlobalCache.CONFIG_CACHE.get("qiniu_secretKey")).uploadToken(bucketName);
    }

    /**
     * 阿里云上传
     */
    private List<String> aliUpload(HttpServletRequest request) throws CustomException {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Set<Entry<String, MultipartFile>> entrySet = fileMap.entrySet();

        List<String> paths = new ArrayList<>();

        for (Entry<String, MultipartFile> entry : entrySet) {
            if (StringUtils.isNotBlank(entry.getValue().getOriginalFilename())) {
                String fileName = entry.getValue().getOriginalFilename().trim();
                String path = UUID.randomUUID().toString().replace("-", "") + "."
                        + fileName.substring(fileName.lastIndexOf(".") + 1);

                // 阿里云OSS初始化
                OSSClient ossClient = null;

                try {
                    ossClient = getOSSClient();

                    // 上传图片(bucket,文件名,文件流)
                    ossClient.putObject(GlobalCache.CONFIG_CACHE.get("alioss_bucket"), path,
                            new ByteArrayInputStream(entry.getValue().getBytes()));

                    paths.add(path);

                } catch (Exception e) {
                    logger.error("文件上传失败(阿里云)", e);
                    throw new CustomException("文件上传失败(阿里云)");
                } finally {
                    if (ossClient != null) {
                        // 关闭OSS
                        ossClient.shutdown();
                    }
                }
            }
        }

        return paths;
    }

    /**
     * 创建阿里OSS client
     */
    private static OSSClient getOSSClient() {
        return new OSSClient(GlobalCache.CONFIG_CACHE.get("alioss_url"),
                GlobalCache.CONFIG_CACHE.get("alioss_access_key"), GlobalCache.CONFIG_CACHE.get("alioss_secret_key"));
    }

    /**
     * 保存对象
     */
    @Override
    public boolean save(FileEntity entity) throws CustomException {
        // 判断文件名是否存在
        if (StringUtils.isBlank(entity.getName())) {
            throw new CustomException("文件名不存在!");
        }

        // 保存新数据
        if (StringUtils.isBlank(entity.getId())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(FileEntity entity) {
        Page<FileEntity> page = new Page<>(entity.getPage(), entity.getLimit());
        List<FileEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<FileEntity> getWrapper(FileEntity entity) {
        EntityWrapper<FileEntity> wrapper = new EntityWrapper<>();

        if (entity == null) {
            return wrapper;
        }

        if (StringUtils.isNotBlank(entity.getName())) {
            wrapper.like("name", entity.getName());
        }

        return wrapper;
    }
}
