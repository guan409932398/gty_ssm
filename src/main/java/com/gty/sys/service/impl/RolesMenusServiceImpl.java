package com.gty.sys.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.exception.CustomException;
import com.gty.sys.mapper.RolesMenusMapper;
import com.gty.sys.model.RoleMenuEntity;
import com.gty.sys.service.RolesMenusService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 角色菜单表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class RolesMenusServiceImpl extends ServiceImpl<RolesMenusMapper, RoleMenuEntity> implements RolesMenusService {
    // Mapper对象
    @Autowired
    private RolesMenusMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean addAll(String roleId, String menuIds) {
        String[] arrayMenuIDs = menuIds.split(",");

        if (arrayMenuIDs != null && arrayMenuIDs.length > 0) {

            Set<String> ids = new HashSet<>();
            // 去除重复ID
            for (String menuID : arrayMenuIDs) {
                ids.add(menuID);
            }

            List<RoleMenuEntity> list = new ArrayList<>();

            for (String menuID : ids) {
                RoleMenuEntity rme = new RoleMenuEntity();

                rme.setMenuId(menuID);
                rme.setRoleId(roleId);

                list.add(rme);
            }

            return insertBatch(list);
        } else {
            return false;
        }
    }

    /**
     * 删除对象
     */
    @Override
    public boolean delByRoles(String roleId) throws CustomException {
        // 判断参数是否正确
        if (StringUtils.isBlank(roleId)) {
            throw new CustomException("roleId不能为空!");
        } else {
            return deleteByMap(new HashMap<String, Object>() {{
                put("role_id", roleId);
            }});
        }
    }
}
