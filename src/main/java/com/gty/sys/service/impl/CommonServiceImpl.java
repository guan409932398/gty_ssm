package com.gty.sys.service.impl;

import java.io.IOException;

import cn.hutool.core.util.RandomUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.gty.global.exception.CustomException;
import com.gty.global.cache.GlobalCache;
import com.gty.sys.model.SmsEntity;
import com.gty.sys.service.CommonService;
import com.gty.utils.SmsUtils;
import com.gty.utils.ValidateUtil;
import com.qiniu.util.Auth;

/**
 * 通用功能 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class CommonServiceImpl implements CommonService {

    private static final Logger logger = Logger.getLogger(CommonServiceImpl.class);

    /**
     * 发送短信
     */
    @Override
    public int sendSms(String phone, String smsModel) throws CustomException {
        if (StringUtils.isBlank(phone)) {
            throw new CustomException("phone参数不能为空！");
        } else if (!ValidateUtil.isPhoneLegal(phone)) {
            throw new CustomException("请输入正确的phone参数！");
        } else if (StringUtils.isBlank(smsModel)) {
            throw new CustomException("smsModel参数不能为空！");
        } else {
            // 生成随机验证码
            String code = RandomUtil.randomString(RandomUtil.BASE_NUMBER, 6);
            // 将验证码存入缓存
            GlobalCache.SMS_CACHE.put(phone, new SmsEntity(phone, smsModel, code));

            if ("aliyun".equals(GlobalCache.CONFIG_CACHE.get("sms_service_providers"))) {
                SendSmsResponse response = null;

                try {
                    response = new SmsUtils().sendAliSms(GlobalCache.CONFIG_CACHE.get("aliyun_sms_accessKey"), GlobalCache.CONFIG_CACHE.get("aliyun_sms_accessKeySecret"), phone, GlobalCache.CONFIG_CACHE.get("aliyun_sms_signName"), GlobalCache.CONFIG_CACHE.get(smsModel), "{\"code\":\"" + code + "\"}");

                    if ("OK".equals(response.getCode())) {
                        return 1;
                    } else {
                        throw new CustomException("阿里云短信发送失败！");
                    }
                } catch (ClientException e) {
                    logger.error("阿里云短信发送失败！ response: " + response, e);
                    throw new CustomException("阿里云短信发送失败！");
                }
            } else if ("juhe".equals(GlobalCache.CONFIG_CACHE.get("sms_service_providers"))) {
                try {
                    boolean temp = new SmsUtils().sendJuheSms(phone, GlobalCache.CONFIG_CACHE.get(smsModel), "{\"code\":\"" + code + "\"}", GlobalCache.CONFIG_CACHE.get("juhe_sms_key"));

                    if (temp) {
                        return 1;
                    } else {
                        throw new CustomException("聚合短信发送失败！");
                    }
                } catch (IOException e) {
                    logger.error("聚合短信发送失败！", e);
                    throw new CustomException("聚合短信发送失败！");
                }
            } else {
                throw new CustomException("未知的短信服务商！");
            }
        }
    }

    /**
     * 获取OSStoken
     */
    @Override
    public String getOssToken(String ossType) throws CustomException {
        if (StringUtils.isBlank(ossType)) {
            throw new CustomException("oss服务商类型[ossType]不能为空！");
        } else if ("qiniu".equals(ossType)) {
            Auth auth = Auth.create(GlobalCache.CONFIG_CACHE.get("qiniu_accessKey"), GlobalCache.CONFIG_CACHE.get("qiniu_secretKey"));
            return auth.uploadToken(GlobalCache.CONFIG_CACHE.get("qiniu_bucket"));
        } else {
            throw new CustomException("不支持的oss服务商！");
        }
    }
}
