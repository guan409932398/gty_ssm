package com.gty.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.sys.mapper.DictionaryMapper;
import com.gty.sys.model.DictionaryEntity;
import com.gty.sys.service.DictionaryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字典表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, DictionaryEntity> implements DictionaryService {

    // Mapper对象
    @Autowired
    private DictionaryMapper mapper;

    /**
     * 更新缓存
     */
    public String refresh(String code) {
        if (StringUtils.isBlank(code)) {
            GlobalCache.DIC_CACHE.cleanUp();
            return "全局字典缓存（全部）已清理";
        } else {
            DictionaryEntity dic = GlobalCache.DIC_CACHE.get(code);

            if (dic != null && StringUtils.isNotBlank(dic.getId())) {
                // 更新上级节点
                GlobalCache.DIC_CACHE.refresh(dic.getUpperId());
                // 移除对象
                GlobalCache.CONFIG_CACHE.invalidate(code);
            }

            return "全局字典缓存（" + code + "）已清理";
        }
    }

    /**
     * 保存对象
     */
    @Override
    public boolean save(DictionaryEntity entity) throws CustomException {
        // 校验 code
        if (entity == null || StringUtils.isBlank(entity.getCode())) {
            throw new CustomException("code不能为空");
        } else {
            EntityWrapper<DictionaryEntity> ew = new EntityWrapper<>();
            ew.eq("code", entity.getCode());

            DictionaryEntity dic = selectOne(ew);

            if (dic != null && StringUtils.isNotBlank(dic.getId()) && !dic.getId().equals(entity.getId())) {
                throw new CustomException("字典代码重复!");
            } else if (StringUtils.isBlank(entity.getId())) {
                return insert(entity);
            } else {
                // 更新当前节点
                GlobalCache.DIC_CACHE.refresh(entity.getCode());
                // 更新上级节点
                GlobalCache.DIC_CACHE.refresh(entity.getUpperId());

                // 修改
                return updateById(entity);
            }
        }
    }

    /**
     * 查询对象
     */
    @Override
    public DictionaryEntity detailByCode(String code) {
        EntityWrapper<DictionaryEntity> ew = new EntityWrapper<>();
        ew.eq("code", code);

        // 获取当前字典
        DictionaryEntity entity = selectOne(ew);

        // 获取下级字典
        if (entity != null && StringUtils.isNotBlank(entity.getId())) {
            EntityWrapper<DictionaryEntity> ewl = new EntityWrapper<>();
            ewl.eq("upper_id", entity.getCode());
            ewl.orderBy("sort");
            entity.setDics(selectList(ewl));
        }

        return entity;
    }

    /**
     * 删除对象
     */
    @Override
    public boolean delByCode(String code) {
        DictionaryEntity dic = GlobalCache.DIC_CACHE.get(code);

        // 更新上级节点
        GlobalCache.DIC_CACHE.refresh(dic.getUpperId());
        // 移除对象
        GlobalCache.CONFIG_CACHE.invalidate(code);

        return deleteByMap(new HashMap<String, Object>() {{
            put("code", code);
        }});
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(DictionaryEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<DictionaryEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<DictionaryEntity> getWrapper(DictionaryEntity entity) {
        EntityWrapper<DictionaryEntity> wrapper = new EntityWrapper<>();

        if (entity == null) {
            return wrapper;
        }

        if (StringUtils.isNotBlank(entity.getName())) {
            wrapper.like("name", entity.getName());
        }

        if (StringUtils.isNotBlank(entity.getCode())) {
            wrapper.like("code", entity.getCode());
        }

        if (StringUtils.isNotBlank(entity.getUpperId())) {
            wrapper.eq("upper_id", entity.getUpperId());
        }


        wrapper.orderBy("sort");

        return wrapper;
    }
}
