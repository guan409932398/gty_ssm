package com.gty.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.global.exception.CustomException;
import com.gty.sys.mapper.MenusMapper;
import com.gty.sys.model.MenuEntity;
import com.gty.sys.model.XtreeEntity;
import com.gty.sys.service.MenusService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 菜单表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class MenusServiceImpl extends ServiceImpl<MenusMapper, MenuEntity> implements MenusService {
    // Mapper对象
    @Autowired
    private MenusMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(MenuEntity entity) throws CustomException {
        // 权限串、菜单名验重
        EntityWrapper<MenuEntity> ew = new EntityWrapper<>();
        ew.eq("perms", entity.getPerms());
        MenuEntity menu = selectOne(ew);

        if (StringUtils.isNotBlank(entity.getPerms()) && menu != null && StringUtils.isNotBlank(menu.getId()) && !menu.getId().equals(entity.getId())) {
            throw new CustomException("权限串已被使用");
        }

        // 写入默认菜单闭合状态
        entity.setSpread("false");

        if (StringUtils.isBlank(entity.getId())) {
            // 新增
            return insert(entity);
        } else {
            // 修改
            return updateById(entity);
        }
    }

    /**
     * 删除对象
     */
    @Override
    public boolean del(String id) throws CustomException {
        // 判断参数是否正确
        if (StringUtils.isBlank(id)) {
            throw new CustomException("id!");
        } else {
            // 判断是否存在子菜单
            EntityWrapper<MenuEntity> ew = new EntityWrapper<>();
            ew.eq("parent_id", id);

            List<MenuEntity> menus = selectList(ew);

            if (menus == null || menus.isEmpty()) {
                return deleteById(id);
            } else {
                throw new CustomException("存在子菜单，不能删除!");
            }

        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(MenuEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<MenuEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<MenuEntity> getWrapper(MenuEntity entity) {
        EntityWrapper<MenuEntity> wrapper = new EntityWrapper<>();

        if (entity == null) {
            return wrapper;
        }

        if (StringUtils.isNotBlank(entity.getParentId())) {
            wrapper.eq("parent_id", entity.getParentId());
        }

        return wrapper;
    }

    /**
     * 递归查询所有菜单
     */
    @Override
    public List<MenuEntity> queryALLHierarchy(MenuEntity entity) {
        EntityWrapper<MenuEntity> ew = new EntityWrapper<>();

        if (StringUtils.isNotBlank(entity.getParentId())) {
            ew.eq("parent_id", entity.getParentId());
        }

        ew.orderBy("sorting", false);

        List<MenuEntity> menus = selectList(ew);

        if (menus != null && !menus.isEmpty()) {
            for (MenuEntity menu : menus) {
                MenuEntity tempMenu = new MenuEntity();

                tempMenu.setParentId(menu.getId());

                menu.setChildNodes(queryALLHierarchy(tempMenu));
            }
        }

        return menus;
    }

    /**
     * 根据角色菜单层级查询列表
     */
    @Override
    public List<MenuEntity> queryJoinRoleHierarchy(MenuEntity entity) {

        List<MenuEntity> menus = mapper.queryJoinRole(entity.getRoleId(), entity.getParentId());

        if (menus != null && !menus.isEmpty()) {
            for (MenuEntity menu : menus) {
                MenuEntity tempMenu = new MenuEntity();
                tempMenu.setParentId(menu.getId());
                tempMenu.setRoleId(entity.getRoleId());

                menu.setChildNodes(queryJoinRoleHierarchy(tempMenu));
            }
        }

        return menus;
    }

    /**
     * 查询列表
     */
    @Override
    public List<XtreeEntity> queryXtree(MenuEntity entity) {
        // 查询所有菜单
        MenuEntity allMenu = new MenuEntity();
        allMenu.setParentId("GTY-QQ:409932398");

        List<MenuEntity> allMenus = queryALLHierarchy(allMenu);

        List<MenuEntity> roleMenus;
        Set<String> roleMenuIDs = null;

        // 根据角色ID查询角色菜单
        if (entity == null || StringUtils.isNotBlank(entity.getRoleId())) {
            roleMenus = mapper.queryJoinRole(entity.getRoleId(), null);

            // 获取角色菜单ID列表
            roleMenuIDs = new HashSet<>();

            if (roleMenus != null && !roleMenus.isEmpty()) {
                for (MenuEntity menu : roleMenus) {
                    roleMenuIDs.add(menu.getId());
                }
            }
        }

        return processMenus(allMenus, roleMenuIDs);
    }

    /***
     * 加工菜单
     */
    private List<XtreeEntity> processMenus(List<MenuEntity> allMenus, Set<String> roleMenuIDs) {
        List<XtreeEntity> xtrees = new ArrayList<>();

        if (allMenus != null && !allMenus.isEmpty()) {
            for (MenuEntity menu : allMenus) {
                XtreeEntity xtree = traverseMenu(menu, roleMenuIDs);

                if (xtree != null && StringUtils.isNotBlank(xtree.getValue())) {
                    xtrees.add(xtree);
                }
            }
        }

        return xtrees;
    }

    /**
     * 遍历菜单获取ID
     */
    private XtreeEntity traverseMenu(MenuEntity menu, Set<String> roleMenuIDs) {
        XtreeEntity xtree = new XtreeEntity();

        boolean isEmpty = (roleMenuIDs == null || roleMenuIDs.isEmpty()) ? true : false;

        if (menu != null && StringUtils.isNotBlank(menu.getId())) {
            // 写入名称
            xtree.setTitle(menu.getTitle());
            // 值
            xtree.setValue(menu.getId());
            // 选中状态
            xtree.setChecked(isEmpty ? false : roleMenuIDs.contains(menu.getId()) ? true : false);
            // 可用状态
            xtree.setDisabled(false);

            List<XtreeEntity> xtrees = new ArrayList<>();

            // 子节点
            if (menu.getChildNodes() != null && !menu.getChildNodes().isEmpty()) {
                for (MenuEntity entity : menu.getChildNodes()) {
                    XtreeEntity xtreeTemp = traverseMenu(entity, roleMenuIDs);

                    if (xtreeTemp != null && StringUtils.isNotBlank(xtreeTemp.getValue())) {
                        xtrees.add(xtreeTemp);
                    }
                }
            }

            // 子节点
            xtree.setData(xtrees);
        }

        return xtree;
    }
}
