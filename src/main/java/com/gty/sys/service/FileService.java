package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.FileEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 附件表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface FileService extends IService<FileEntity> {

    /**
     * 文件上传
     */
    List<String> upload(HttpServletRequest request) throws CustomException;

    /**
     * 保存对象
     */
    boolean save(FileEntity entity) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(FileEntity entity) throws CustomException;
}
