package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.DictionaryEntity;

import java.util.Map;

/**
 * 字典表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface DictionaryService extends IService<DictionaryEntity> {

    /**
     * 更新缓存
     */
    String refresh(String code) throws CustomException;

    /**
     * 保存对象
     */
    boolean save(DictionaryEntity entity) throws CustomException;

    /**
     * 查询对象
     */
    DictionaryEntity detailByCode(String code);

    /**
     * 删除对象
     */
    boolean delByCode(String code) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(DictionaryEntity entity);
}
