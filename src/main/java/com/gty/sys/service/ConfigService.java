package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.ConfigEntity;

import java.util.Map;

/**
 * 配置表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface ConfigService extends IService<ConfigEntity> {
    /**
     * 刷新缓存
     */
    String refresh(String code);

    /**
     * 保存对象
     */
    boolean save(ConfigEntity entity) throws CustomException;

    /**
     * 删除对象
     */
    int delByCode(String code) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(ConfigEntity entity);
}
