package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.UserEntity;

import java.util.Map;

/**
 * 用户表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 用户注册
     */
    boolean register(UserEntity entity) throws CustomException;

    /**
     * 用户登录
     */
    UserEntity login(UserEntity entity) throws CustomException;

    /**
     * 用户保存
     */
    boolean save(UserEntity entity) throws CustomException;


    /**
     * 用户保存
     */
    boolean save(UserEntity entity, String back) throws CustomException;

    /**
     * 根据userid和旧密码修改密码
     */
    boolean changePwd(UserEntity entity) throws CustomException;

    /**
     * 修改用户状态
     */
    boolean changeState(UserEntity entity) throws CustomException;

    /**
     * 重置用户密码
     */
    String resetPwd(String id) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(UserEntity entity);
}
