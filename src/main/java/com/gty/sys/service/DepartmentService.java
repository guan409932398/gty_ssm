package com.gty.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.sys.model.DepartmentEntity;

import java.util.Map;

/**
 * 公司部门表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-26 11:12:00
 */
public interface DepartmentService extends IService<DepartmentEntity> {

    /**
     * 保存对象
     */
    boolean save(DepartmentEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(DepartmentEntity entity);
}
