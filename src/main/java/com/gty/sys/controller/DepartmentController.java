package com.gty.sys.controller;

import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.sys.model.DepartmentEntity;
import com.gty.sys.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * 公司部门表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-26 11:12:00
 */
@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService service;

    /**
     * 跳转公司部门表新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/sys/department/department_editor";
    }

    /**
     * 跳转公司部门表编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/sys/department/department_editor";
    }

    /**
     * 跳转公司部门表列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/sys/department/department_query_tree";
    }

    /**
     * 跳转公司部门表明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/sys/department/department_detail";
    }

    /**
     * 公司部门表保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(DepartmentEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }
    
    /**
     * 公司部门表删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * 公司部门表明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = DepartmentEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }
    
    /**
     * 公司部门表列表
     */
    @RequestMapping("/query")
    @JSON(type = DepartmentEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(DepartmentEntity entity) {
        Map<String, Object> map = service.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }
}
