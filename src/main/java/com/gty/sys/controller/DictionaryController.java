package com.gty.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.gty.global.exception.CustomException;
import com.gty.global.result.Result;
import com.gty.sys.model.DictionaryEntity;
import com.gty.sys.service.DictionaryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * 字典表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/dictionary")
public class DictionaryController {

    @Autowired
    private DictionaryService service;

    /**
     * 跳转新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd(String code, Model model) {

        DictionaryEntity entity = new DictionaryEntity();
        entity.setUpperId(code);
        model.addAttribute("po", entity);

        return "page/sys/dictionary/dictionary_editor";
    }

    /**
     * 跳转编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/sys/dictionary/dictionary_editor";
    }

    /**
     * 跳转列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/sys/dictionary/dictionary_query";
    }

    /**
     * 跳转列表页面
     */
    @RequestMapping("/goSelQuery")
    public String goSelQuery(Model model, DictionaryEntity entity) {
        model.addAttribute("entity", entity);
        return "page/sys/dictionary/dictionary_query_sel";
    }

    /**
     * 保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Result save(DictionaryEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public Result del(String code) throws CustomException {
        return Result.success(service.delByCode(code));
    }

    /**
     * 查询明细
     */
    @RequestMapping("/detail")
    @ResponseBody
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * 查询明细
     */
    @RequestMapping("/detailByCode")
    @ResponseBody
    public Result detailByCode(String code) {
        EntityWrapper<DictionaryEntity> ew = new EntityWrapper<>();
        ew.eq("code", code);

        return Result.success(service.selectOne(ew));
    }

    /**
     * 查询结果集
     */
    @RequestMapping("/query")
    @ResponseBody
    public Result query(DictionaryEntity entity) {
        if (entity != null && StringUtils.isBlank(entity.getUpperId())) {
            entity.setUpperId("GTY-QQ:409932398");
        }

        Map<String, Object> map = service.query(entity);

        return Result.success(map.get("list"), (Long) map.get("count"));
    }

    /**
     * 更新缓存
     */
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    @ResponseBody
    public Result refresh(String code) throws CustomException {
        return Result.success(service.refresh(code));
    }
}
