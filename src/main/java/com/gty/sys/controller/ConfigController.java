package com.gty.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.gty.global.exception.CustomException;
import com.gty.global.result.Result;
import com.gty.sys.model.ConfigEntity;
import com.gty.sys.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 配置表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/config")
public class ConfigController {

    @Autowired
    private ConfigService service;

    /**
     * 跳转配置列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/sys/config/config_query";
    }

    /**
     * 跳转配置新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/sys/config/config_editor";
    }

    /**
     * 跳转配置编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/sys/config/config_editor";
    }

    /**
     * 跳转关于系统列表页面
     */
    @RequestMapping("/goAboutUs")
    public String goIndQuery() {
        return "page/sys/config/config_aboutus_query";
    }

    /**
     * 跳转关于系统编辑页面
     */
    @RequestMapping("/goAboutUsEditor")
    public String goAboutUsEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/sys/config/config_aboutus_editor";
    }

    /**
     * 跳转配置列表页面
     */
    @RequestMapping("/goSwitchQuery")
    public String goSwitchQuery() {
        return "page/sys/config/config_switch_query";
    }

    /**
     * 跳转配置新增页面
     */
    @RequestMapping("/goSwitchAdd")
    public String goSwitchAdd() {
        return "page/sys/config/config_switch_editor";
    }

    /**
     * 跳转配置编辑页面
     */
    @RequestMapping("/goSwitchEditor")
    public String goSwitchEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/sys/config/config_switch_editor";
    }

    /**
     * 保存信息
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Result save(ConfigEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public Result del(String code) throws CustomException {
        return Result.success(service.delByCode(code));
    }

    /**
     * 查询明细
     */
    @RequestMapping("/detail")
    @ResponseBody
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * 查询明细
     */
    @RequestMapping("/detailByCode")
    @ResponseBody
    public Result detailByCode(String code) {
        EntityWrapper<ConfigEntity> ew = new EntityWrapper<>();
        ew.eq("code", code);

        return Result.success(service.selectOne(ew));
    }

    /**
     * 查询结果集
     */
    @RequestMapping("/query")
    @ResponseBody
    public Result query(ConfigEntity entity) {
        Map<String, Object> map = service.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }

    /**
     * 更新缓存
     */
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    @ResponseBody
    public Result refresh(String code) {
        return Result.success(service.refresh(code));
    }
}
