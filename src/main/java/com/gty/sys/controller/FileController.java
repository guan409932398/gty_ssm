package com.gty.sys.controller;

import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.result.Result;
import com.gty.sys.model.DictionaryEntity;
import com.gty.sys.model.FileEntity;
import com.gty.sys.service.FileService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 附件表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService service;

    /**
     * 跳转配置新增页面
     */
    @RequestMapping("/show")
    public String goAdd(String id, Model model) {
        model.addAttribute("id", id);
        return "page/sys/file/file_show";
    }

    /**
     * 文件上传
     */
    @RequestMapping(value = "/upload")
    @ResponseBody
    public Result upload(HttpServletRequest request) throws Exception {
        return Result.success(service.upload(request));
    }

    /**
     * 文件删除
     */
    @RequestMapping(value = "/del")
    @ResponseBody
    public Result delete(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * 文件查询
     */
    @RequestMapping(value = "/query")
    @ResponseBody
    public Result query(FileEntity entity) throws CustomException {
        return Result.success(service.query(entity));
    }

    /**
     * 文件查询
     */
    @RequestMapping(value = "/detail")
    @ResponseBody
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * 文件下载
     */
    @RequestMapping(value = "/download")
    public void download(String id, HttpServletResponse response)
            throws IOException {
        // 根据文件主键获取文件对象
        FileEntity entity = service.selectById(id);

        if (entity != null && !StringUtils.isBlank(entity.getPath())) {
            String path = entity.getPath();

            ServletOutputStream out = null;
            FileInputStream ips = null;

            // 将文件以流方式读取并返回到前台
            try {
                File file = new File(path);

                if (file.exists()) {
                    // 获取图片存放路径
                    ips = new FileInputStream(file);
                    response.setContentType("multipart/form-data");
                    out = response.getOutputStream();

                    // 读取文件流
                    int len;
                    byte[] buffer = new byte[1024 * 10];

                    while ((len = ips.read(buffer)) != -1) {
                        out.write(buffer, 0, len);
                    }

                    out.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    out.close();
                }

                if (ips != null) {
                    ips.close();
                }
            }
        }
    }

    /**
     * 模版下载
     */
    @RequestMapping(value = "/downloadTemplate")
    public void downloadTemplate(String code, HttpServletResponse response)
            throws IOException {
        // 根据文件主键获取字典对象
        DictionaryEntity dic = GlobalCache.DIC_CACHE.get(code);

        if (dic != null && !StringUtils.isBlank(dic.getRemark())) {
            String path = this.getClass().getResource("/").getPath() + "template/" + dic.getRemark();

            ServletOutputStream out = null;
            FileInputStream ips = null;

            // 将文件以流方式读取并返回到前台
            try {
                File file = new File(path);

                if (file.exists()) {
                    // 获取图片存放路径
                    ips = new FileInputStream(file);
                    response.setContentType("multipart/form-data");
                    out = response.getOutputStream();

                    // 读取文件流
                    int len;
                    byte[] buffer = new byte[1024 * 10];

                    while ((len = ips.read(buffer)) != -1) {
                        out.write(buffer, 0, len);
                    }

                    out.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    out.close();
                }

                if (ips != null) {
                    ips.close();
                }
            }
        }
    }
}
