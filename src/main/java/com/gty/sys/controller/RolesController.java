package com.gty.sys.controller;

import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.sys.model.RoleEntity;
import com.gty.sys.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * 角色表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/roles")
public class RolesController {

    @Autowired
    private RolesService service;

    /**
     * 跳转新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/sys/roles/roles_editor";
    }

    /**
     * 跳转编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(RoleEntity entity, Model model) {
        model.addAttribute("po", service.selectById(entity.getId()));
        return "page/sys/roles/roles_editor";
    }

    /**
     * 跳转列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/sys/roles/roles_query";
    }

    /**
     * 跳转明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(RoleEntity entity, Model model) {
        model.addAttribute("po", service.selectById(entity.getId()));
        return "page/sys/roles/roles_detail";
    }

    /**
     * 保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(RoleEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(RoleEntity entity) {
        return Result.success(service.deleteById(entity.getId()));
    }

    /**
     * 明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = RoleEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(RoleEntity entity) {
        return Result.success(service.selectById(entity.getId()));
    }

    /**
     * 列表
     */
    @RequestMapping("/query")
    @JSON(type = RoleEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result query(RoleEntity entity) {
        Map<String, Object> map = service.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }
}
