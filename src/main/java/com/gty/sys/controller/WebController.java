package com.gty.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * WEB技术 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190710
 */
@Controller
@RequestMapping("/web")
public class WebController {
    /**
     * 跳二维码页面
     */
    @RequestMapping("/goQrCode")
    public String goQrCode() {
        return "page/web/qr_code";
    }

    /**
     * 跳二维码页面
     */
    @RequestMapping("/goQrCodeImg")
    public String goQrCodeImg() {
        return "page/web/qr_code_img";
    }

    /**
     * 跳二维码详情页面
     */
    @RequestMapping("/goQrCodeContent")
    public String goQrCodeContent(String id) {
        if ("ZZZZZZZZZZZZZZZZZZZZZ".equals(id)) {
            return "page/web/qr_code_content";
        } else {
            return "";
        }
    }

    /**
     * 跳ThreeJs页面
     */
    @RequestMapping("/goThreeJs")
    public String goThreeJs() {
        return "page/web/three_js";
    }

    /**
     * 跳ThreeJs页面
     */
    @RequestMapping("/goCanvas")
    public String goCanvas() {
        return "page/web/canvas";
    }
}
