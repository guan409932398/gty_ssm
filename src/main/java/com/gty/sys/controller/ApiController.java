package com.gty.sys.controller;

import cn.hutool.core.io.FileUtil;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.utils.MapUtil;
import org.pegdown.PegDownProcessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * API文档 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/api")
public class ApiController {

    // API文档
    private static Map<String, String> apis = new TreeMap<>();

    /**
     * API 列表
     */
    @RequestMapping("/query")
    public ModelAndView query(HttpServletRequest request) throws CustomException {
        if ("open".equals(GlobalCache.CONFIG_CACHE.get("api_switch"))) {
            ModelAndView mv = new ModelAndView();

            if (apis.isEmpty()) {
                String realPath = request.getServletContext().getRealPath("/api");

                List<File> files = FileUtil.loopFiles(realPath);

                if (!files.isEmpty()) {
                    for (File file : files) {
                        apis.put(file.getName(), file.getPath());
                    }
                }

                apis = MapUtil.sortMapByKey(apis);
            }

            mv.addObject("apis", apis);
            mv.setViewName("page/api/api_query");

            return mv;
        } else {
            throw new CustomException("API关闭访问权限!");
        }
    }

    /**
     * API 明细
     */
    @RequestMapping("/detail")
    public ModelAndView detail(String key) throws CustomException {
        if ("open".equals(GlobalCache.CONFIG_CACHE.get("api_switch"))) {
            ModelAndView mv = new ModelAndView();

            mv.addObject("content", new PegDownProcessor().markdownToHtml(FileUtil.readUtf8String(apis.get(key))));
            mv.setViewName("page/api/api_detail");

            return mv;
        } else {
            throw new CustomException("API关闭访问权限!");
        }
    }

    /**
     * API 刷新
     */
    @RequestMapping(value = "/refresh")
    @JSON(type = Result.class, filter = "count")
    public Result refresh() {
        apis.clear();
        return Result.success();
    }
}
