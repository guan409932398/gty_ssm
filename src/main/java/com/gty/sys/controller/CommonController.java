package com.gty.sys.controller;

import com.google.code.kaptcha.Producer;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.global.shiro.ShiroUtils;
import com.gty.sys.model.UserEntity;
import com.gty.sys.service.CommonService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;

/**
 * 通用功能 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private CommonService service;
    @Autowired
    private Producer captchaProducer = null;

    /**
     * 跳转后台主页
     */
    @RequestMapping("/index")
    public String index(HttpServletRequest request) {
        UserEntity user = (UserEntity) SecurityUtils.getSubject().getPrincipal();
        request.setAttribute("user", user);
        return "redirect:/index.jsp";
    }

    /**
     * 跳转推荐注册页面
     */
    @RequestMapping("/share")
    public String share(String sign, Model model) {
        model.addAttribute("sign", sign);
        return "page/share/share_regiser";
    }

    /**
     * 跳转欢迎页面
     */
    @RequestMapping("/goWelcome")
    public String goWelcome() {
        return "page/welcome";
    }

    /**
     * 跳转推荐注册页面
     */
    @RequestMapping("/goWebSocket")
    public String goWebSocket() {
        return "page/websocket";
    }

    /**
     * 刷新
     */
    @RequestMapping("/refuse")
    public String refuse() {
        return "refuse";
    }

    /**
     * 跳转数据库监控
     */
    @RequestMapping("/druid")
    @RequiresPermissions("common:druid:view")
    public String druid() {
        return "redirect:/druid/index.html";
    }

    /**
     * 获取图片验证码
     */
    @RequestMapping("/getSecurityCode")
    public void getSecurityCode(HttpServletResponse response) throws Exception {
        String text = captchaProducer.createText();
        BufferedImage image = captchaProducer.createImage(text);
        ShiroUtils.setSessionAttribute("kaptcha", text);
        ImageIO.write(image, "JPEG", response.getOutputStream());
    }

    /**
     * 获取短信验证码
     */
    @RequestMapping(value = "/sendSms")
    @JSON(type = Result.class, filter = "count")
    public Result sendSms(String phone, String smsModel) throws CustomException {
        return Result.success(service.sendSms(phone, smsModel));
    }

    /**
     * 获取OSSToken
     */
    @RequestMapping(value = "/getOssToken")
    @JSON(type = Result.class, filter = "count")
    public Result getOssToken(String ossType) throws CustomException {
        return Result.success(service.getOssToken(ossType));
    }

    /**
     * 跳转缓存页面
     */
    @RequestMapping("/refresh")
    public String refresh() {
        return "page/sys/cache/refresh";
    }
}
