package com.gty.sys.controller;

import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.global.shiro.ShiroUtils;
import com.gty.sys.model.MenuEntity;
import com.gty.sys.model.UserEntity;
import com.gty.sys.service.RolesService;
import com.gty.sys.service.UserService;
import com.gty.utils.RequestUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 用户表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService ser;
    @Autowired
    private RolesService rolesSer;

    /**
     * 跳转用户表新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd(Model model) {
        model.addAttribute("roles", rolesSer.selectList(null));
        return "page/sys/user/user_editor";
    }

    /**
     * 跳转用户表编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(UserEntity entity, Model model) {
        model.addAttribute("po", ser.selectById(entity.getId()));
        model.addAttribute("roles", rolesSer.selectList(null));
        return "page/sys/user/user_editor";
    }

    /**
     * 跳转用户表列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/sys/user/user_query";
    }

    /**
     * 跳转用户表明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(UserEntity entity, Model model) {
        model.addAttribute("po", ser.selectById(entity.getId()));
        return "page/sys/user/user_detail";
    }

    /**
     * 跳转用户表新增页面
     */
    @RequestMapping("/goSel")
    public String goSel(Model model, UserEntity entity) {
        model.addAttribute("entity", entity);
        return "page/sys/user/user_query_sel";
    }

    /**
     * 跳转用户表明细页面
     */
    @RequestMapping("/goCurrentDetail")
    public String goCurrentDetail(Model model) {
        UserEntity entity = (UserEntity) SecurityUtils.getSubject().getPrincipal();
        model.addAttribute("po", ser.selectById(entity.getId()));
        return "page/sys/user/user_detail";
    }

    /**
     * 跳转修改密码页面
     */
    @RequestMapping("/goChangeCurrentPwd")
    public String goChangeCurrentPwd() {
        return "page/sys/user/change_pwd";
    }

    /**
     * 登出
     */
    @RequestMapping(value = "/loginOut")
    public String loginOut() {
        ShiroUtils.logout();
        return "redirect:/login.jsp";
    }

    /**
     * 用户表保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(UserEntity entity, String back) throws CustomException {
        return Result.success(ser.save(entity, back));
    }

    /**
     * 用户表删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(UserEntity entity) {
        return Result.success(ser.deleteById(entity.getId()));
    }

    /**
     * 用户表明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = UserEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(UserEntity entity) {
        return Result.success(ser.selectById(entity.getId()));
    }

    /**
     * 用户表列表
     */
    @RequestMapping("/query")
    @JSON(type = UserEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result query(UserEntity entity) {
        Map<String, Object> map = ser.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }

    /**
     * 用户表列表
     */
    @RequestMapping("/queryJoinRole")
    @JSON(type = UserEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result queryJoinRole(UserEntity entity) {
        Map<String, Object> map = ser.query(entity);

        List<UserEntity> list = (List<UserEntity>) map.get("list");

        if (list != null && !list.isEmpty()) {
            list.forEach(user -> {
                if (StringUtils.isNotBlank(user.getUserRole())) {
                    user.setRoleName(rolesSer.selectById(user.getUserRole()).getName());
                }
            });
        }

        return Result.success(list, (Long) map.get("count"));
    }

    /**
     * 管理员登陆
     */
    @RequestMapping("/login")
    public ModelAndView login(HttpServletRequest request, UserEntity entity) {
        ModelAndView mv = new ModelAndView();

        // 判断那是否为手机登录
        // boolean isMobile = RequestUtil.isMobileDevice(request.getHeader("User-Agent"));

        try {
            ser.login(entity);
            mv.addObject("po", entity);

            // 手机登录直接跳转到手机页面
            //if (isMobile) {
            //    mv.setViewName("forward:/fxproject/goMobileQuery");
            //} else {
                mv.setViewName("forward:/common/index");
            //}
        } catch (CustomException ce) {
            mv.addObject("msg", ce.getMsg());
            mv.setViewName("forward:/login.jsp");
        } catch (Exception e) {
            mv.addObject("msg", e.getMessage());
            mv.setViewName("forward:/login.jsp");
        }

        return mv;
    }

    /**
     * 获取用户菜单
     */
    @RequestMapping(value = "/getUserMenus", produces = MediaType.APPLICATION_JSON_VALUE + ";charset=utf-8")
    @JSON(type = MenuEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public List<MenuEntity> getUserMenus() {
        UserEntity user = (UserEntity) SecurityUtils.getSubject().getPrincipal();
        return user.getUserMenus();
    }

    /**
     * 跳转修改密码页面
     */
    @RequestMapping(value = "/changeCurrentPwd", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count,timestamp")
    public Result changeCurrentPwd(UserEntity entity) throws CustomException {
        UserEntity user = (UserEntity) SecurityUtils.getSubject().getPrincipal();
        entity.setId(user.getId());

        // 修改成功后移除作用域，重新登陆
        if (ser.changePwd(entity)) {
            SecurityUtils.getSubject().logout();
        }

        return Result.success();
    }

    /**
     * 封禁/解封用户
     *
     * @return
     */
    @RequestMapping(value = "/lock", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result lock(UserEntity entity) throws CustomException {
        return Result.success(ser.changeState(entity));
    }

    /**
     * 重置用户密码
     *
     * @return
     */
    @RequestMapping(value = "/resetPwd", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result resetPwd(String id) throws CustomException {
        return Result.success(ser.resetPwd(id));
    }
}
