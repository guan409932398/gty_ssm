package com.gty.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.sys.model.MenuEntity;
import com.gty.sys.model.XtreeEntity;
import com.gty.sys.service.MenusService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/menus")
public class MenusController {

    @Autowired
    private MenusService service;

    /**
     * 跳转菜单新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd(MenuEntity entity, Model model) {
        // 判断是否需要查询上级节点ID
        if (entity != null && StringUtils.isNotBlank(entity.getParentId()) && !"GTY-QQ:409932398".equals(entity.getParentId())) {
            MenuEntity menus = service.selectById(entity.getParentId());
            entity.setParentName(menus.getTitle());
        } else {
            entity.setParentName("");
        }

        model.addAttribute("po", entity);
        return "page/sys/menus/menus_editor";
    }

    /**
     * 跳转菜单编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(MenuEntity vo, Model model) {
        MenuEntity menus = service.selectById(vo.getId());

        // 判断是否需要查询上级节点ID
        if (menus != null && StringUtils.isNotBlank(menus.getParentId()) && !"GTY-QQ:409932398".equals(menus.getParentId())) {
            MenuEntity menu = service.selectById(menus.getParentId());
            menus.setParentName(menu.getTitle());
        }

        model.addAttribute("po", menus);

        return "page/sys/menus/menus_editor";
    }

    /**
     * 跳转菜单列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/sys/menus/menus_query";
    }

    /**
     * 跳转菜单明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(MenuEntity entity, Model model) {
        model.addAttribute("po", service.selectById(entity.getId()));
        return "page/sys/menus/menus_detail";
    }

    /**
     * 菜单保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(MenuEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 菜单删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(MenuEntity entity) {
        return Result.success(service.deleteById(entity.getId()));
    }

    /**
     * 菜单明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = MenuEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(MenuEntity entity) {
        return Result.success(service.selectById(entity.getId()));
    }

    /**
     * 菜单列表
     */
    @RequestMapping("/query")
    @JSON(type = MenuEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result query() {
        EntityWrapper<MenuEntity> ew = new EntityWrapper<>();
        ew.orderBy("sorting", false);
        List<MenuEntity> list = service.selectList(ew);
        return Result.success(list, (new Integer(list.size())).longValue());
    }

    /**
     * 菜单列表
     */
    @RequestMapping("/queryXtree")
    @ResponseBody
    public List<XtreeEntity> queryXtree(MenuEntity entity) {
        return service.queryXtree(entity);
    }
}
