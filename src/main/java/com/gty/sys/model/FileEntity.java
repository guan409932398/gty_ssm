package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_file")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 文件名称
    @TableField("name")
    private String name;
    // 文件类型
    @TableField("type")
    private String type;
    // 文件所属表
    @TableField("table")
    private String table;
    // 文件所属主键
    @TableField("key")
    private String key;
    // 文件地址
    @TableField("path")
    private String path;
    // 文件状态
    @TableField("state")
    private String state;
    // 文件排序号
    @TableField("sort")
    private int sort;
    // 文件缓存图片路径
    @TableField("thumb")
    private String thumb;
}
