package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 配置表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_config")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 配置代码
    @TableField("code")
    private String code;
    // 配置名称
    @TableField("name")
    private String name;
    // 配置类别
    @TableField("category")
    private String category;
    // 配置类型
    @TableField("type")
    private String type;
    // 配置值
    @TableField("values")
    private String values;
    // 备注
    @TableField("remark")
    private String remark;
}
