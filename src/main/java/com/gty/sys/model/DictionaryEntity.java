package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 字典表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_dictionary")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DictionaryEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 名称
    @TableField("name")
    private String name;
    // 代码
    @TableField("code")
    private String code;
    // 配对码
    @TableField("mate")
    private String mate;
    // 上级ID
    @TableField("upper_id")
    private String upperId;
    // 代码状态
    @TableField("state")
    private String state;
    // 图片ID
    @TableField("image")
    private String image;
    // 排序号
    @TableField("sort")
    private Double sort;
    // 备注
    @TableField("remark")
    private String remark;

    // 子节点
    @TableField(exist = false)
    private List<DictionaryEntity> dics;
}
