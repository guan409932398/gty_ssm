package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_roles")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 角色名
    @TableField("name")
    private String name;
    // 角色备注
    @TableField("remark")
    private String remark;

    // 菜单id
    @TableField(exist = false)
    private String menuIds;
}
