package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色菜单表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_roles_menus")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleMenuEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 菜单ID
    @TableField("menu_id")
    private String menuId;
    // 角色ID
    @TableField("role_id")
    private String roleId;
}
