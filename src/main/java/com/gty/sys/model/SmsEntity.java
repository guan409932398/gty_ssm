package com.gty.sys.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 短信 vo
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsEntity {
    // 用户帐号
    private String phone;
    // 验证码类型
    private String smsType;
    // 验证码值
    private String smsCode;
}
