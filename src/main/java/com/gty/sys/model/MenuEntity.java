package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 菜单表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_menus")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 菜单名
    @TableField("title")
    private String title;
    // 图标
    @TableField("icon")
    private String icon;
    // 资源地址
    @TableField("href")
    private String href;
    // 权限
    @TableField("perms")
    private String perms;
    // true：展开，false：不展开
    @TableField("spread")
    private String spread;
    // 父节点
    @TableField("parent_id")
    private String parentId;
    // 排序号
    @TableField("sorting")
    private Long sorting;

    // 父节点名称
    @TableField(exist = false)
    private String parentName;
    // 节点打开状态
    @TableField(exist = false)
    private Boolean isOpen = false;
    // 角色ID
    @TableField(exist = false)
    private String roleId;
    // 子节点列表
    @TableField(exist = false)
    private List<MenuEntity> childNodes;
}
