package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
* 公司部门表 Entity
*
* @author 官天野
* @version v1.0
* @email: guan409932398@qq.com
* @date 2019-06-26 11:12:00
*/
@TableName("tb_sys_department")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 部门名称
    @TableField("name")
    private String name;
    // 部门类型
    @TableField("type")
    private String type;
    // 级别串
    @TableField("level")
    private String level;
    // 上级ID
    @TableField("parent_id")
    private String parentId;
    // 排序号
    @TableField("sort")
    private Integer sort;
}
