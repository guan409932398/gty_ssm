package com.gty.sys.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * 用户表对象
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@TableName("tb_sys_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity extends BasePO {
    private static final long serialVersionUID = 20190612L;

    // 用户帐号
    @TableField("account")
    private String account;
    // 用户密码
    @TableField("password")
    private String password;
    // 用户昵称
    @TableField("nickname")
    private String nickname;
    // 真实姓名
    @TableField("real_name")
    private String realName;
    // 用户手机号
    @TableField("phone")
    private String phone;
    // 用户邮箱
    @TableField("email")
    private String email;
    // 用户头像
    @TableField("image")
    private String image;
    // 性别
    @TableField("gender")
    private String gender;
    // 年龄
    @TableField("age")
    private String age;
    // 地址
    @TableField("address")
    private String address;
    // 简介
    @TableField("desc")
    private String desc;
    // 用户类别
    @TableField("category")
    private String category;
    // 用户类型
    @TableField("type")
    private String type;
    // 用户状态
    @TableField("state")
    private String state;
    // 用户角色
    @TableField("user_role")
    private String userRole;
    // 推荐人ID
    @TableField("referral_user")
    private String referralUser;

    // 角色名称
    @TableField(exist = false)
    private String roleName;
    // 验证码
    @TableField(exist = false)
    private String securityCode;
    // 旧密码
    @TableField(exist = false)
    private String oldPassword;
    // 用户菜单
    @TableField(exist = false)
    private List<MenuEntity> userMenus;
    // 用户权限
    @TableField(exist = false)
    private Set<String> userRoles;
}
