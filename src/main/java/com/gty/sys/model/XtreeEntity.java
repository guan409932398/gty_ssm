package com.gty.sys.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Xtree vo
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class XtreeEntity implements Serializable {
    private static final long serialVersionUID = 20190613L;

    // 名称
    private String title;
    // 值
    private String value;
    // 选中状态
    private Boolean checked = false;
    // 可用状态
    private Boolean disabled;
    // 子节点
    private List<XtreeEntity> data;
}
