package com.gty.sys.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.sys.model.ConfigEntity;

/**
 * 配置表 mapper
 *
 * @author 官天野 QQ:409932398
 * @version v1.0
 * @email guan409932398@qq.com
 * @date 2018-09-08 17:25:42
 */
public interface ConfigMapper extends BaseMapper<ConfigEntity> {
}