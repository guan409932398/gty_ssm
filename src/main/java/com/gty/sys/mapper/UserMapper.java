package com.gty.sys.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.sys.model.UserEntity;

/**
 * 用户表 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface UserMapper extends BaseMapper<UserEntity> {
}
