package com.gty.sys.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.gty.sys.model.MenuEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 菜单表 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface MenusMapper extends BaseMapper<MenuEntity> {

    @Select("<script>" +
            "SELECT m.* " +
            "FROM tb_sys_menus m INNER JOIN tb_sys_roles_menus rm on m.id = rm.menu_id " +
            "WHERE rm.role_id = #{roleId} " +
                "<if test='parentId != null'> AND parent_id = #{parentId} </if> " +
            "ORDER BY sorting DESC" +
            "</script>")
    List<MenuEntity> queryJoinRole(@Param("roleId") String roleId, @Param("parentId") String parentId);
}
