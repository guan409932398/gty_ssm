package com.gty.sys.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.sys.model.DepartmentEntity;

/**
 * 公司部门表 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-26 11:12:00
 */
public interface DepartmentMapper extends BaseMapper<DepartmentEntity> {
}
