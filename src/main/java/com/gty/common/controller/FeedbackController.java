package com.gty.common.controller;

import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.common.model.FeedbackEntity;
import com.gty.common.service.FeedbackService;
import com.gty.sys.model.DictionaryEntity;
import com.gty.sys.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * tb_common_feedback Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-25 15:47:46
 */
@Controller
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService service;
    @Autowired
    private UserService userSer;

    /**
     * 跳转tb_common_feedback新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/common/feedback/feedback_editor";
    }

    /**
     * 跳转tb_common_feedback编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {

        FeedbackEntity fb = service.selectById(id);

        // 查询用户名
        if (StringUtils.isNotBlank(fb.getUserId())) {
            fb.setUserName(userSer.selectById(fb.getUserId()).getNickname());
        }

        model.addAttribute("po", fb);
        return "page/common/feedback/feedback_editor";
    }

    /**
     * 跳转tb_common_feedback列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/common/feedback/feedback_query";
    }

    /**
     * 跳转tb_common_feedback明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/common/feedback/feedback_detail";
    }

    /**
     * tb_common_feedback保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(FeedbackEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * tb_common_feedback删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * tb_common_feedback明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = FeedbackEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * tb_common_feedback列表
     */
    @RequestMapping("/query")
    @JSON(type = FeedbackEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(FeedbackEntity entity) {
        Map<String, Object> map = service.query(entity);

        List<FeedbackEntity> fbs = (List<FeedbackEntity>) map.get("list");

        if (fbs != null && !fbs.isEmpty()) {
            fbs.forEach(fb -> {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fb.getType());
                fb.setTypeName(dic == null ? "" : dic.getName());
            });
        }

        return Result.success(fbs, (Long) map.get("count"));
    }
}
