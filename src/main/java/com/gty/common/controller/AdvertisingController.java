package com.gty.common.controller;

import com.gty.common.model.AdvertisingEntity;
import com.gty.common.service.AdvertisingService;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.sys.model.DictionaryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * tb_common_advertising Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 15:12:41
 */
@Controller
@RequestMapping("/advertising")
public class AdvertisingController {

    @Autowired
    private AdvertisingService service;

    /**
     * 跳转tb_common_advertising新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/common/advertising/advertising_editor";
    }

    /**
     * 跳转tb_common_advertising编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/common/advertising/advertising_editor";
    }

    /**
     * 跳转tb_common_advertising列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/common/advertising/advertising_query";
    }

    /**
     * 跳转tb_common_advertising明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/common/advertising/advertising_detail";
    }

    /**
     * tb_common_advertising保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(AdvertisingEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * tb_common_advertising删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * tb_common_advertising明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = AdvertisingEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * tb_common_advertising列表
     */
    @RequestMapping("/query")
    @JSON(type = AdvertisingEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(AdvertisingEntity entity) {
        Map<String, Object> map = service.query(entity);

        List<AdvertisingEntity> ads = (List<AdvertisingEntity>) map.get("list");

        if (ads != null && !ads.isEmpty()) {
            ads.forEach(ad -> {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(ad.getCategory());
                ad.setCategoryName(dic == null ? "" : dic.getName());
            });
        }

        return Result.success(ads, (Long) map.get("count"));
    }
}
