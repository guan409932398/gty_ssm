package com.gty.common.controller;

import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.common.model.NoticeEntity;
import com.gty.common.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * tb_common_notice Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 17:06:52
 */
@Controller
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    private NoticeService service;

    /**
     * 跳转tb_common_notice新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/common/notice/notice_editor";
    }

    /**
     * 跳转tb_common_notice编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/common/notice/notice_editor";
    }

    /**
     * 跳转tb_common_notice列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/common/notice/notice_query";
    }

    /**
     * 跳转tb_common_notice明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/common/notice/notice_detail";
    }

    /**
     * tb_common_notice保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(NoticeEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }
    
    /**
     * tb_common_notice删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * tb_common_notice明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = NoticeEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }
    
    /**
     * tb_common_notice列表
     */
    @RequestMapping("/query")
    @JSON(type = NoticeEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(NoticeEntity entity) {
        Map<String, Object> map = service.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }
}
