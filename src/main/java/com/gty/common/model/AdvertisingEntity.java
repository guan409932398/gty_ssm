package com.gty.common.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * tb_common_advertising Entity
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 15:12:41
 */
@TableName("tb_common_advertising")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisingEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 标题
    @TableField("title")
    private String title;
    // 类别
    @TableField("category")
    private String category;
    // 排序
    @TableField("sort")
    private Integer sort;
    // 图片
    @TableField("image")
    private String image;
    // 内容
    @TableField("content")
    private String content;
    // 备注
    @TableField("remark")
    private String remark;

    @TableField(exist = false)
    private String categoryName;
}
