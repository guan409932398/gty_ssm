package com.gty.common.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * tb_common_feedback Entity
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-25 15:47:46
 */
@TableName("tb_common_feedback")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 反馈标题
    @TableField("title")
    private String title;
    // 反馈类型
    @TableField("type")
    private String type;
    // 反馈状态
    @TableField("state")
    private String state;
    // 反馈内容
    @TableField("content")
    private String content;
    // 反馈人ID 
    @TableField("user_id")
    private String userId;

    // 类型名称
    @TableField(exist = false)
    private String typeName;
    // 反馈人
    @TableField(exist = false)
    private String userName;
}
