package com.gty.common.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
* tb_common_notice Entity
*
* @author 官天野
* @version v1.0
* @email: guan409932398@qq.com
* @date 2019-06-24 17:06:52
*/
@TableName("tb_common_notice")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 标题
    @TableField("title")
    private String title;
    // 内容
    @TableField("content")
    private String content;
    // 类型
    @TableField("type")
    private String type;
}
