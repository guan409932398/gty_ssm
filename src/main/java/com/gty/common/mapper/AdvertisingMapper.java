package com.gty.common.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.common.model.AdvertisingEntity;

/**
 * tb_common_advertising Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 15:12:41
 */
public interface AdvertisingMapper extends BaseMapper<AdvertisingEntity> {
}
