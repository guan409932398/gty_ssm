package com.gty.common.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.common.model.NoticeEntity;

/**
 * tb_common_notice Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 17:06:52
 */
public interface NoticeMapper extends BaseMapper<NoticeEntity> {
}
