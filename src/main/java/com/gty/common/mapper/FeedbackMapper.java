package com.gty.common.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.common.model.FeedbackEntity;

/**
 * tb_common_feedback Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-25 15:47:46
 */
public interface FeedbackMapper extends BaseMapper<FeedbackEntity> {
}
