package com.gty.common.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.common.mapper.FeedbackMapper;
import com.gty.common.model.FeedbackEntity;
import com.gty.common.service.FeedbackService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * tb_common_feedback Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-25 15:47:46
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, FeedbackEntity> implements FeedbackService {
    // Mapper对象
    @Autowired
    private FeedbackMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(FeedbackEntity entity) {
        if (StringUtils.isBlank(entity.getId())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(FeedbackEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<FeedbackEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<FeedbackEntity> getWrapper(FeedbackEntity entity) {
        EntityWrapper<FeedbackEntity> wrapper = new EntityWrapper<>();

        // 标题
        if (StringUtils.isNotBlank(entity.getTitle())) {
            wrapper.like("title", entity.getTitle());
        }

        // 类别
        if (StringUtils.isNotBlank(entity.getType())) {
            wrapper.eq("type", entity.getType());
        }

        // 状态
        if (StringUtils.isNotBlank(entity.getState())) {
            wrapper.eq("state", entity.getState());
        }

        wrapper.orderBy("create_time");

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
