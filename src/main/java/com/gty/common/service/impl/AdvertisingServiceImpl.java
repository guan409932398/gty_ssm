package com.gty.common.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.common.mapper.AdvertisingMapper;
import com.gty.common.model.AdvertisingEntity;
import com.gty.common.service.AdvertisingService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * tb_common_advertising Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 15:12:41
 */
@Service
public class AdvertisingServiceImpl extends ServiceImpl<AdvertisingMapper, AdvertisingEntity> implements AdvertisingService {
    // Mapper对象
    @Autowired
    private AdvertisingMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(AdvertisingEntity entity) {
        if (StringUtils.isBlank(entity.getId())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(AdvertisingEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<AdvertisingEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<AdvertisingEntity> getWrapper(AdvertisingEntity entity) {
        EntityWrapper<AdvertisingEntity> wrapper = new EntityWrapper<>();

        // 标题
        if (StringUtils.isNotBlank(entity.getTitle())) {
            wrapper.like("title", entity.getTitle());
        }

        // 类别
        if (StringUtils.isNotBlank(entity.getCategory())) {
            wrapper.like("category", entity.getCategory());
        }

        wrapper.orderBy("create_time");

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
