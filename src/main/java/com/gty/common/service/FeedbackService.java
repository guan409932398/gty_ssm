package com.gty.common.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.common.model.FeedbackEntity;

import java.util.Map;

/**
 * tb_common_feedback Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-25 15:47:46
 */
public interface FeedbackService extends IService<FeedbackEntity> {

    /**
     * 保存对象
     */
    boolean save(FeedbackEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(FeedbackEntity entity);
}
