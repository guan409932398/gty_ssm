package com.gty.common.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.common.model.NoticeEntity;

import java.util.Map;

/**
 * tb_common_notice Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 17:06:52
 */
public interface NoticeService extends IService<NoticeEntity> {

    /**
     * 保存对象
     */
    boolean save(NoticeEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(NoticeEntity entity);
}
