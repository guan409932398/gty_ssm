package com.gty.common.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.common.model.AdvertisingEntity;

import java.util.Map;

/**
 * tb_common_advertising Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-24 15:12:41
 */
public interface AdvertisingService extends IService<AdvertisingEntity> {

    /**
     * 保存对象
     */
    boolean save(AdvertisingEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(AdvertisingEntity entity);
}
