package com.gty.fxjc.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 方兴产品 Entity
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-15 17:24:50
 */
@TableName("tb_fxjc_fxproject")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FxprojectEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 产品名称
    @TableField("name")
    private String name;
    // 产品型号
    @TableField("model")
    private String model;
    // 产品颜色
    @TableField("color")
    private String color;
    // 生产日期
    @TableField("production_date")
    private Long productionDate;
    // 批量编号
    @TableField("lot_no")
    private String lotNo;
    // 执行标准
    @TableField("execution_standard")
    private String executionStandard;
    // 质量等级
    @TableField("quality_grade")
    private String qualityGrade;
    // 检验员
    @TableField("inspector")
    private String inspector;

    // 产品名称
    @TableField(exist = false)
    private String nameShow;
    // 产品型号
    @TableField(exist = false)
    private String modelShow;
    // 产品颜色
    @TableField(exist = false)
    private String colorShow;
    // 执行标准
    @TableField(exist = false)
    private String executionStandardShow;
    // 生产日期
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date productionDateShow;
    // 质量等级
    @TableField(exist = false)
    private String qualityGradeShow;
    // 检验员
    @TableField(exist = false)
    private String inspectorShow;
}
