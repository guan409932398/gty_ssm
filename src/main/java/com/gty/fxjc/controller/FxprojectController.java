package com.gty.fxjc.controller;

import cn.hutool.core.date.DateUtil;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.fxjc.model.FxprojectEntity;
import com.gty.fxjc.service.FxprojectService;
import com.gty.sys.model.DictionaryEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * 方兴产品 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-15 17:24:50
 */
@Controller
@RequestMapping("/fxproject")
public class FxprojectController {

    @Autowired
    private FxprojectService service;

    /**
     * 跳转方兴产品新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd(Model model) {
        FxprojectEntity project = new FxprojectEntity();

        // 产品名称（fxjc_product_name）
        DictionaryEntity dic = GlobalCache.DIC_CACHE.get("fxjc_product_name");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setName(dictionary.getCode());
                    break;
                }
            }
        }

        // 产品型号
        dic = GlobalCache.DIC_CACHE.get("fxjc_model");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setModel(dictionary.getCode());
                    break;
                }
            }
        }

        // 产品颜色
        dic = GlobalCache.DIC_CACHE.get("fxjc_color");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setColor(dictionary.getCode());
                    break;
                }
            }
        }

        // 执行标准
        dic = GlobalCache.DIC_CACHE.get("fxjc_execution_standard");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setExecutionStandard(dictionary.getCode());
                    break;
                }
            }
        }

        // 质量等级
        dic = GlobalCache.DIC_CACHE.get("fxjc_quality_grade");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setQualityGrade(dictionary.getCode());
                    break;
                }
            }
        }

        // 检验员
        dic = GlobalCache.DIC_CACHE.get("fxjc_quality_inspector");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setInspector(dictionary.getCode());
                    break;
                }
            }
        }

        // 生产日期
        project.setProductionDate(DateUtil.currentSeconds());

        model.addAttribute("po", project);
        return "page/fxjc/fxproject/fxproject_editor";
    }

    /**
     * 跳转方兴产品新增页面
     */
    @RequestMapping("/goMobileAdd")
    public String goMobileAdd(Model model) {
        FxprojectEntity project = new FxprojectEntity();

        // 产品名称（fxjc_product_name）
        DictionaryEntity dic = GlobalCache.DIC_CACHE.get("fxjc_product_name");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setName(dictionary.getCode());
                    break;
                }
            }
        }

        // 产品型号
        dic = GlobalCache.DIC_CACHE.get("fxjc_model");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setModel(dictionary.getCode());
                    break;
                }
            }
        }

        // 产品颜色
        dic = GlobalCache.DIC_CACHE.get("fxjc_color");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setColor(dictionary.getCode());
                    break;
                }
            }
        }

        // 执行标准
        dic = GlobalCache.DIC_CACHE.get("fxjc_execution_standard");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setExecutionStandard(dictionary.getCode());
                    break;
                }
            }
        }

        // 质量等级
        dic = GlobalCache.DIC_CACHE.get("fxjc_quality_grade");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setQualityGrade(dictionary.getCode());
                    break;
                }
            }
        }

        // 检验员
        dic = GlobalCache.DIC_CACHE.get("fxjc_quality_inspector");

        // 判断是否存在默认值
        if (dic != null && dic.getDics() != null && !dic.getDics().isEmpty()) {
            for (DictionaryEntity dictionary : dic.getDics()) {
                if ("default".equals(dictionary.getRemark())) {
                    project.setInspector(dictionary.getCode());
                    break;
                }
            }
        }

        // 生产日期
        project.setProductionDate(DateUtil.currentSeconds());

        model.addAttribute("po", project);
        return "page/fxjc/fxproject/fxproject_mobile_editor";
    }

    /**
     * 跳转方兴产品编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/fxjc/fxproject/fxproject_editor";
    }

    /**
     * 跳转方兴产品编辑页面
     */
    @RequestMapping("/goMobileEditor")
    public String goMobileEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/fxjc/fxproject/fxproject_mobile_editor";
    }

    /**
     * 跳转方兴产品列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/fxjc/fxproject/fxproject_query";
    }

    /**
     * 跳转方兴产品列表手机页面
     */
    @RequestMapping("/goMobileQuery")
    public String goMobileQuery() {
        return "page/fxjc/fxproject/fxproject_mobile_query";
    }

    /**
     * 跳转方兴产品列表页面
     */
    @RequestMapping("/goManageQuery")
    public String goManageQuery() {
        return "page/fxjc/fxproject/fxproject_manage_query";
    }

    /**
     * 跳转方兴产品二维码页面
     */
    @RequestMapping("/goQrCode")
    public String goQrCode(String id, Model model) {
        model.addAttribute("id", id);
        return "page/fxjc/fxproject/fxproject_qrCode";
    }


    /**
     * 跳转方兴产品明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        FxprojectEntity fp = service.selectById(id);

        if (fp != null) {
            // 产品名称（fxjc_product_name）
            if (StringUtils.isNotBlank(fp.getName())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getName());

                fp.setNameShow(dic == null ? "" : dic.getName());
            }

            // 产品型号
            if (StringUtils.isNotBlank(fp.getModel())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getModel());

                fp.setModelShow(dic == null ? "" : dic.getName());
            }

            // 产品颜色
            if (StringUtils.isNotBlank(fp.getColor())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getColor());

                fp.setColorShow(dic == null ? "" : dic.getName());
            }

            // 执行标准
            if (StringUtils.isNotBlank(fp.getExecutionStandard())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getExecutionStandard());

                fp.setExecutionStandardShow(dic == null ? "" : dic.getName());
            }

            // 质量等级
            if (StringUtils.isNotBlank(fp.getQualityGrade())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getQualityGrade());

                fp.setQualityGradeShow(dic == null ? "" : dic.getName());
            }

            // 检验员
            if (StringUtils.isNotBlank(fp.getInspector())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getInspector());

                fp.setInspectorShow(dic == null ? "" : dic.getName());
            }
        }

        model.addAttribute("po", fp);
        return "page/fxjc/fxproject/fxproject_detail";
    }

    /**
     * 方兴产品保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(FxprojectEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 方兴产品删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * 方兴产品二维码生成
     */
    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result generate(String id) {

        FxprojectEntity fp = service.selectById(id);

        if (fp != null) {
            fp.setRecordState("generate");
            return Result.success(service.updateById(fp));
        } else {
            return Result.fail("记录不存在");
        }
    }

    /**
     * 方兴产品明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = FxprojectEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        FxprojectEntity fp = service.selectById(id);

        if (fp != null) {
            // 产品名称（fxjc_product_name）
            if (StringUtils.isNotBlank(fp.getName())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getName());

                fp.setNameShow(dic == null ? "" : dic.getName());
            }

            // 产品型号
            if (StringUtils.isNotBlank(fp.getModel())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getModel());

                fp.setModelShow(dic == null ? "" : dic.getName());
            }

            // 产品颜色
            if (StringUtils.isNotBlank(fp.getColor())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getColor());

                fp.setColorShow(dic == null ? "" : dic.getName());
            }

            // 执行标准
            if (StringUtils.isNotBlank(fp.getExecutionStandard())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getExecutionStandard());

                fp.setExecutionStandardShow(dic == null ? "" : dic.getName());
            }

            // 质量等级
            if (StringUtils.isNotBlank(fp.getQualityGrade())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getQualityGrade());

                fp.setQualityGradeShow(dic == null ? "" : dic.getName());
            }

            // 检验员
            if (StringUtils.isNotBlank(fp.getInspector())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getInspector());

                fp.setInspectorShow(dic == null ? "" : dic.getName());
            }
        }

        return Result.success(fp);
    }

    /**
     * 方兴产品列表
     */
    @RequestMapping("/query")
    @JSON(type = FxprojectEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(FxprojectEntity entity) {
        Map<String, Object> map = service.query(entity);

        List<FxprojectEntity> list = (List<FxprojectEntity>) map.get("list");

        list.forEach(fp -> {
            // 产品名称（fxjc_product_name）
            if (StringUtils.isNotBlank(fp.getName())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getName());

                fp.setNameShow(dic == null ? "" : dic.getName());
            }

            // 产品型号
            if (StringUtils.isNotBlank(fp.getModel())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getModel());

                fp.setModelShow(dic == null ? "" : dic.getName());
            }

            // 产品颜色
            if (StringUtils.isNotBlank(fp.getColor())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getColor());

                fp.setColorShow(dic == null ? "" : dic.getName());
            }

            // 执行标准
            if (StringUtils.isNotBlank(fp.getExecutionStandard())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getExecutionStandard());

                fp.setExecutionStandardShow(dic == null ? "" : dic.getName());
            }

            // 质量等级
            if (StringUtils.isNotBlank(fp.getQualityGrade())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getQualityGrade());

                fp.setQualityGradeShow(dic == null ? "" : dic.getName());
            }

            // 检验员
            if (StringUtils.isNotBlank(fp.getInspector())) {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(fp.getInspector());

                fp.setInspectorShow(dic == null ? "" : dic.getName());
            }
        });

        return Result.success(list, (Long) map.get("count"));
    }
}
