package com.gty.fxjc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.fxjc.model.FxprojectEntity;

/**
 * 方兴产品 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-15 17:24:50
 */
public interface FxprojectMapper extends BaseMapper<FxprojectEntity> {
}
