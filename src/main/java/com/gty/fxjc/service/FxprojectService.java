package com.gty.fxjc.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.fxjc.model.FxprojectEntity;

import java.util.Map;

/**
 * 方兴产品 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-15 17:24:50
 */
public interface FxprojectService extends IService<FxprojectEntity> {

    /**
     * 保存对象
     */
    boolean save(FxprojectEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(FxprojectEntity entity);
}
