package com.gty.fxjc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.fxjc.mapper.FxprojectMapper;
import com.gty.fxjc.model.FxprojectEntity;
import com.gty.fxjc.service.FxprojectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 方兴产品 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-15 17:24:50
 */
@Service
public class FxprojectServiceImpl extends ServiceImpl<FxprojectMapper, FxprojectEntity> implements FxprojectService {
    // Mapper对象
    @Autowired
    private FxprojectMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(FxprojectEntity entity) {
        if (entity.getProductionDateShow() != null) {
            entity.setProductionDate(entity.getProductionDateShow().getTime() / 1000);
        }

        if (StringUtils.isBlank(entity.getId())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(FxprojectEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<FxprojectEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<FxprojectEntity> getWrapper(FxprojectEntity entity) {
        EntityWrapper<FxprojectEntity> wrapper = new EntityWrapper<>();

        // 产品名称
        if (StringUtils.isNotBlank(entity.getName())) {
            wrapper.eq("name", entity.getName());
        }

        // 产品型号
        if (StringUtils.isNotBlank(entity.getModel())) {
            wrapper.eq("model", entity.getModel());
        }

        // 产品颜色
        if (StringUtils.isNotBlank(entity.getColor())) {
            wrapper.eq("color", entity.getColor());
        }

        // 执行标准
        if (StringUtils.isNotBlank(entity.getExecutionStandard())) {
            wrapper.eq("execution_standard", entity.getExecutionStandard());
        }

        // 质量等级
        if (StringUtils.isNotBlank(entity.getQualityGrade())) {
            wrapper.eq("quality_grade", entity.getQualityGrade());
        }

        // 检验员
        if (StringUtils.isNotBlank(entity.getInspector())) {
            wrapper.eq("'#inspector'", entity.getInspector());
        }

        // 生产开始日期
        if (entity.getBeginDate() != null) {
            wrapper.ge("production_date", entity.getBeginDate().getTime() / 1000);
        }

        // 生产结束日期
        if (entity.getEndDate() != null) {
            wrapper.le("production_date", entity.getEndDate().getTime() / 1000 + 86399);
        }

        wrapper.orderBy("production_date DESC");

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
