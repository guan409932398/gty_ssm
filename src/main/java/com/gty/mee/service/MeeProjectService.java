package com.gty.mee.service;

import com.gty.global.exception.CustomException;

import java.util.Map;

/**
 * 环评建设项目 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public interface MeeProjectService {
    /**
     * 查询列表
     *
     * @param page      页码
     * @param limit     每页记录数
     * @param provinces 省份
     */
    Map<String, Object> query(int page, int limit, String provinces) throws CustomException;

    /**
     * 查询对象
     *
     * @param id        建设项目ID
     * @param provinces 省份
     */
    Map<String, Object> detail(Long id, String provinces) throws CustomException;
}
