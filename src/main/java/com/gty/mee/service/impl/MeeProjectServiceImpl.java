package com.gty.mee.service.impl;

import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.mee.service.MeeProjectService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 建设项目 Service impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Service
public class MeeProjectServiceImpl implements MeeProjectService {

    // 日志
    private final static Logger logger = LoggerFactory.getLogger(MeeProjectServiceImpl.class);
    // 接口版本
    private final static String version = "1.0";

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(int page, int limit, String provinces) throws CustomException {
        String time = System.currentTimeMillis() / 1000 + "";
        String data = JSON.toJSONString(getQueryParams(provinces, page, limit));
        String sign = new MD5().digestHex("getProjectInfoList" + time + data + version + GlobalCache.CONFIG_CACHE.get("mee_ekey"));

        try {
            String content = HttpUtil.post((GlobalCache.CONFIG_CACHE.get("mee_project_query_url") + "getProjectInfoList"), getSignMap(time, sign, data));

            logger.info(">>>>建设项目列表信息：" + content);

            // 环保系统返回数据
            if (StringUtils.isNotBlank(content)) {
                Map<String, Object> rootMap = JSONObject.parseObject(content);

                // 判断查询是否成功
                if ("200".equals(rootMap.get("status").toString())) {
                    Map<String, Object> bodyMap = JSONObject.parseObject(rootMap.get("body").toString());

                    List<Map<String, Object>> list = (List<Map<String, Object>>) JSON
                            .parse(bodyMap.get("list").toString());

                    for (Map<String, Object> map : list) {
                        map.put("gty", provinces);
                    }

                    // 创建返回对象
                    Map<String, Object> resultMap = new HashMap<>();

                    resultMap.put("list", list);
                    resultMap.put("count", Long.parseLong(bodyMap.get("total").toString()));

                    return resultMap;
                } else {
                    throw new CustomException(rootMap.get("message").toString());
                }
            }
        } catch (Exception e) {
            throw new CustomException("调用第三方错误");
        }

        return null;
    }

    /**
     * 获取请求参数Map
     */
    private Map<String, Object> getQueryParams(String provinces, int page, int limit) {
        // 创建参数Map
        Map<String, Object> params = new HashMap<>();

        params.put("key", provinces);
        params.put("page", page);
        params.put("limit", limit);

        return params;
    }

    /**
     * 获取认证Map
     */
    private Map<String, Object> getSignMap(String time, String sign, String data) {
        // 创建认证Map
        Map<String, Object> signs = new HashMap<>();

        signs.put("version", version);
        signs.put("time", time);
        signs.put("sign", sign);
        signs.put("data", data);

        return signs;
    }

    /**
     * 查询对象
     */
    @Override
    public Map<String, Object> detail(Long id, String provinces) throws CustomException {
        Map<String, Object> resultMap = new HashMap<>();

        if (StringUtils.isNotBlank(provinces)) {
            resultMap.putAll(projectDetail(id, provinces));
            resultMap.putAll(companyDetail(Long.parseLong(resultMap.get("companyId").toString()), provinces));
        }

        return resultMap;
    }

    /**
     * 查询建设项目信息
     */
    private Map<String, Object> projectDetail(Long id, String provinces) throws CustomException {
        String time = System.currentTimeMillis() / 1000 + "";
        String data = JSON.toJSONString(getDetailParams(id, provinces));
        String sign = new MD5().digestHex("getProjectInfoOne" + time + data + version + GlobalCache.CONFIG_CACHE.get("mee_ekey"));

        // 调用环保系统
        String content = HttpUtil.post((GlobalCache.CONFIG_CACHE.get("mee_project_detail_url") + "getProjectInfoOne"), getSignMap(time, sign, data));

        logger.info(">>>>建设项目详情：" + content);

        // 创建返回对象
        Map<String, Object> resultMap = new HashMap<>();

        // 环保系统返回数据
        if (StringUtils.isNotBlank(content)) {
            Map<String, Object> rootMap = JSONObject.parseObject(content);

            // 判断查询是否成功
            if ("200".equals(rootMap.get("status").toString())) {
                Map<String, Object> bodyMap = JSONObject.parseObject(rootMap.get("body").toString());

                // 建设项目信息
                Map<String, Object> cpMap = JSONObject.parseObject(bodyMap.get("companyProject").toString());
                resultMap.put("companyProject", cpMap);
                // 建设单位ID
                resultMap.put("companyId", Long.parseLong(cpMap.get("companyId").toString()));
                // 建设项目地址信息
                List<Map<String, Object>> paList = (List<Map<String, Object>>) JSON.parse(bodyMap.get("projectAddressList").toString());
                resultMap.put("projectAddressList", paList);
                // ？？？信息
                List<Map<String, Object>> bcList = (List<Map<String, Object>>) JSON.parse(bodyMap.get("buildCommentList").toString());
                resultMap.put("buildCommentList", bcList);
                // 位置信息
                List<Map<String, Object>> lList = (List<Map<String, Object>>) JSON.parse(bodyMap.get("locations").toString());
                resultMap.put("locations", lList);
                // 评价单位信息
                Map<String, Object> euMap = JSONObject.parseObject(bodyMap.get("evaluateUnit").toString());
                resultMap.put("evaluateUnit", euMap);
            } else {
                throw new CustomException(rootMap.get("message").toString());
            }
        }

        return resultMap;
    }

    /**
     * 获取请求参数Map
     */
    private Map<String, Object> getDetailParams(Long id, String provinces) {
        // 创建参数Map
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);
        params.put("key", provinces);

        return params;
    }

    /**
     * 查询建设单位基本信息
     */
    private Map<String, Object> companyDetail(Long companyId, String provinces) throws CustomException {
        String time = System.currentTimeMillis() / 1000 + "";
        String data = JSON.toJSONString(getCompanyParams(companyId, provinces));
        String sign = new MD5().digestHex("getCompanyDetail" + time + data + version + GlobalCache.CONFIG_CACHE.get("mee_ekey"));

        // 调用环保系统
        String content = HttpUtil.post((GlobalCache.CONFIG_CACHE.get("mee_company_detail_url") + "getCompanyDetail"), getSignMap(time, sign, data));

        logger.info(">>>>建设项目-建设单位：" + content);

        // 创建返回对象
        Map<String, Object> resultMap = new HashMap<>();

        // 环保系统返回数据
        if (StringUtils.isNotBlank(content)) {
            Map<String, Object> rootMap = JSONObject.parseObject(content);

            // 判断查询是否成功
            if ("200".equals(rootMap.get("status").toString())) {
                Map<String, Object> bodyMap = JSONObject.parseObject(rootMap.get("body").toString());

                // 建设单位信息
                resultMap.put("companyDetail", bodyMap);
            } else {
                throw new CustomException(rootMap.get("message").toString());
            }
        }

        return resultMap;
    }

    /**
     * 获取请求参数Map
     */
    private Map<String, Object> getCompanyParams(Long companyId, String provinces) {
        // 创建参数Map
        Map<String, Object> params = new HashMap<>();

        params.put("key", provinces);
        params.put("companyId", companyId);

        return params;
    }
}
