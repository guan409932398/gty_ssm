package com.gty.mee.controller;

import com.alibaba.fastjson.JSON;
import com.gty.global.exception.CustomException;
import com.gty.global.result.Result;
import com.gty.mee.service.MeeProjectService;
import freemarker.template.utility.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 环评建设项目 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Controller
@RequestMapping("/meeProject")
public class MeeProjectController {

    // 日志
    private final static Logger logger = LoggerFactory.getLogger(MeeProjectController.class);

    @Autowired
    private MeeProjectService ser;

    /**
     * 跳转列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/mee/mee_project/project_query";
    }

    /**
     * 跳转详情页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(Long key, String provinces, Model model) throws CustomException {
        Map<String, Object> resultMap = ser.detail(key, provinces);
        model.addAttribute("po", resultMap);

        logger.info(JSON.toJSONString(resultMap));

        return "page/mee/mee_project/project_detail";
    }

    /**
     * 查询结果集
     */
    @RequestMapping("/query")
    @ResponseBody
    public Result query(int page, int limit, String provinces) throws CustomException {
        if (StringUtils.isBlank(provinces)) {
            return Result.fail("请选择查询省");
        } else {
            Map<String, Object> map = ser.query(page, limit, provinces);
            return Result.success(map.get("list"), (Long) map.get("count"));
        }
    }

}
