package com.gty.utils;

/**
 * 字符串工具类
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190615
 */
public class TxtUtils {
    private static final char UNDERLINE = '_';

    /**
     * 替换最后出现串
     */
    public static String replaceLast(String text, String regex, String replacement) {
        return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
    }

    /**
     * 驼峰格式字符串转换为下划线格式字符串
     */
    public static String camelToUnderline(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (Character.isUpperCase(c)) {
                sb.append(UNDERLINE);
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 下划线格式字符串转换为驼峰格式字符串
     */
    public static String underlineToCame(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c == UNDERLINE) {
                if (++i < len) {
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 首字母转大写
     */
    public static String convertFirstCharUpper(String name) {
        return ("" + name.charAt(0)).toUpperCase() + name.substring(1);
    }

    /**
     * 统计字数，参照MS office word 2007规则t
     */
    public static int getMSWordsCount(String content) {
        if (org.apache.commons.lang3.StringUtils.isBlank(content)) {
            return 0;
        }

        //中文单词
        String cnWords = content.replaceAll("[^(\\u4e00-\\u9fa5，。《》？；’‘：“”【】、）（……￥！·)]", "");

        //非中文单词
        String nonCnWords = content.replaceAll("[^(a-zA-Z0-9`\\-=\';.,/~!@#$%^&*()_+|}{\":><?\\[\\])]", " ");
        String[] ss = nonCnWords.split(" ");

        int nonCnWordsCount = 0;

        for (String s : ss) {
            if (s.trim().length() != 0) {
                nonCnWordsCount++;
            }
        }

        //中文和非中文单词合计
        return cnWords.length() + nonCnWordsCount;
    }

    /**
     * 半角转全角
     */
    public static String toSBC(String input) {
        char c[] = input.toCharArray();

        for (int i = 0; i < c.length; i++) {
            if (c[i] == ' ') {
                c[i] = '\u3000';
            } else if (c[i] < '\177') {
                c[i] = (char) (c[i] + 65248);
            }
        }

        return new String(c);
    }

    /**
     * 全角转半角
     */
    public static String toDBC(String input) {
        char c[] = input.toCharArray();

        for (int i = 0; i < c.length; i++) {
            if (c[i] == '\u3000') {
                c[i] = ' ';
            } else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
                c[i] = (char) (c[i] - 65248);
            }
        }

        return new String(c);
    }

    /**
     * 替换全部空白符
     */
    public static String replaceAllWhite(String input) {
        return input.replaceAll("\\s*", "");
    }

    /**
     * 替换空白行
     */
    public static String replaceWhiteLine(String input) {
        return input.replaceAll("(?m)^\\s*$(\\n|\\r\\n)", "");
    }

    /**
     * 切分行
     */
    public static String[] splitLines(String input) {
        return input.split("\\r?\\n", -1);
    }

    /**
     * 将emoji表情替换成空串
     */
    public static String filterEmoji(String source) {
        if (source != null && source.length() > 0) {
            return source.replaceAll("[\ud800\udc00-\udbff\udfff\ud800-\udfff]", "");
        } else {
            return source;
        }
    }
}
