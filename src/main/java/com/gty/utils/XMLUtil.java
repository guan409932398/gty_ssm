package com.gty.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * XML 解析工具类
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public class XMLUtil {
    /**
     * 解析xml,返回第一级元素键值对。 如果第一级元素有子节点， 则此节点的值是子节点的xml数据。
     */
    public static SortedMap<String, String> doXMLParse(String strxml) throws JDOMException, IOException {
        strxml = strxml.replaceFirst("encoding=\".*\"", "encoding=\"UTF-8\"");

        if ("".equals(strxml)) {
            return null;
        }

        SortedMap<String, String> map = new TreeMap<>();
        InputStream in = new ByteArrayInputStream(strxml.getBytes(StandardCharsets.UTF_8));
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(in);
        Element root = doc.getRootElement();
        List list = root.getChildren();
        Iterator it = list.iterator();

        while (it.hasNext()) {
            Element e = (Element) it.next();
            String key = e.getName();
            String value;
            List children = e.getChildren();

            if (children.isEmpty()) {
                value = e.getTextNormalize();
            } else {
                value = XMLUtil.getChildrenText(children);
            }

            map.put(key, value);
        }

        // 关闭流
        in.close();
        return map;
    }

    /**
     * 获取子结点的xml
     */
    public static String getChildrenText(List children) {
        StringBuilder sb = new StringBuilder();

        if (!children.isEmpty()) {
            Iterator it = children.iterator();

            while (it.hasNext()) {
                Element e = (Element) it.next();
                String name = e.getName();
                String value = e.getTextNormalize();
                List list = e.getChildren();
                sb.append("<");
                sb.append(name);
                sb.append(">");

                if (!list.isEmpty()) {
                    sb.append(XMLUtil.getChildrenText(list));
                }

                sb.append(value);
                sb.append("</");
                sb.append(name);
                sb.append(">");
            }
        }

        return sb.toString();
    }

}