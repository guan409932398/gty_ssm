package com.gty.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 * 短信工具类
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public class SmsUtils {

    private static final Logger logger = Logger.getLogger(SmsUtils.class);

    // 阿里云产品名称:云通信短信API产品,开发者无需替换
    private static final String product = "Dysmsapi";
    // 阿里云产品域名,开发者无需替换
    private static final String domain = "dysmsapi.aliyuncs.com";

    /**
     * 阿里云发送短信
     *
     * @param accessKeyId     阿里身份id
     * @param accessKeySecret 阿里签名访问参数
     * @param tel             手机号
     * @param signName        签名
     * @param templateCode    模版编号
     * @param params          参数JSON格式
     */
    public SendSmsResponse sendAliSms(String accessKeyId, String accessKeySecret, String tel, String signName,
                                      String templateCode, String params) throws ClientException {

        // 初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        // 组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        // 必填:待发送手机号
        request.setPhoneNumbers(tel);
        // 必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        // 必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(templateCode);
        // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为{\"name\":\"Tom\"}
        request.setTemplateParam(params);

        // 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        // request.setSmsUpExtendCode("90997");

        // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("yourOutId");

        // hint 此处可能会抛出异常，注意catch
        return acsClient.getAcsResponse(request);
    }

    /**
     * 向聚合短信接口发送信息
     */
    public boolean sendJuheSms(String tel, String tplId, String content, String key) throws IOException {
        String codestr = URLEncoder.encode(content, "utf-8");

        URL url = new URL(MessageFormat.format("http://v.juhe.cn/sms/send?mobile={0}&tpl_id={1}&tpl_value={2}&key={3}",
                tel, tplId, codestr, key));

        // 打开链接
        URLConnection connection = url.openConnection();
        // 向服务器发送请求
        connection.connect();
        // 获得服务器响应数据
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
        StringBuilder builder = new StringBuilder();
        String linedata;

        while ((linedata = bufferedReader.readLine()) != null) {
            builder.append(linedata);
        }

        bufferedReader.close();

        return builder.toString().indexOf("\"error_code\":0") >= 0 ? true : false;
    }

    /**
     * 测试类
     */
    public static void main(String[] args) throws IOException {
        // 阿里短信测试
        // aliTest();
        // 聚合短信测试
        juheTest();
    }

    /**
     * 阿里短信测试类
     */
    private static void aliTest() throws ClientException {
        // 发短信
        SendSmsResponse response = new SmsUtils().sendAliSms("LTAIjjYO9lRnbd5k", "VtulK6pX9H4dU9abae30gI5zg35jln",
                "18510113058", "阿里云短信测试专用", "SMS_135290218", "{\"code\":\"3452\"}");

        logger.info("--------------->: 短信接口返回的数据");
        logger.info("Code=" + response.getCode());
        logger.info("Message=" + response.getMessage());
        logger.info("RequestId=" + response.getRequestId());
        logger.info("BizId=" + response.getBizId());
    }

    /**
     * 聚合短信测试类
     */
    private static void juheTest() throws IOException {
        logger.info("-------------> send begin!");
        boolean temp = new SmsUtils().sendJuheSms("18510113058", "87368", "#code#=123&#=李四",
                "f2625e47395197438c5a20ee8831e6ec");
        logger.info("-------------> send end! 执行结果：" + temp);
    }
}
