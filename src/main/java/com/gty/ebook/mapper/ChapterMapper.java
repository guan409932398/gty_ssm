package com.gty.ebook.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.ebook.model.ChapterEntity;
import org.apache.ibatis.annotations.Select;

/**
 * 章节表 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 10:06:04
 */
public interface ChapterMapper extends BaseMapper<ChapterEntity> {
    /**
     * 获取最大章节序号
     */
    @Select("SELECT MAX(sort) FROM tb_ebook_chapter WHERE bookid = #{bookid}")
    Integer getMaxSortByBookId(String bookid);
}
