package com.gty.ebook.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.ebook.model.BookEntity;

/**
 * 书籍表 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 09:55:54
 */
public interface BookMapper extends BaseMapper<BookEntity> {
}
