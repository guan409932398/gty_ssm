package com.gty.ebook.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.ebook.model.ChapterEntity;

import java.util.Map;

/**
 * 章节表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 10:06:04
 */
public interface ChapterService extends IService<ChapterEntity> {

    /**
     * 保存对象
     */
    boolean save(ChapterEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(ChapterEntity entity);

    /**
     * 根据书籍ID获取最大章节序号
     */
    Integer getMaxSortByBookId(String bookid);
}
