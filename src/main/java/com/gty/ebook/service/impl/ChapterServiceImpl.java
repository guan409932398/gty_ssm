package com.gty.ebook.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.houbb.opencc4j.util.ZhConverterUtil;
import com.gty.ebook.mapper.ChapterMapper;
import com.gty.ebook.model.BookEntity;
import com.gty.ebook.model.ChapterEntity;
import com.gty.ebook.service.BookService;
import com.gty.ebook.service.ChapterService;
import com.gty.utils.TxtUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 章节表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 10:06:04
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, ChapterEntity> implements ChapterService {
    // Mapper对象
    @Autowired
    private ChapterMapper mapper;
    // 书籍服务
    @Autowired
    private BookService bookSer;

    /**
     * 保存对象
     */
    @Override
    public boolean save(ChapterEntity entity) {
        // 写入默认值
        if (StringUtils.isBlank(entity.getProofread())) {
            entity.setProofread("unchecked");
        }

        if (StringUtils.isBlank(entity.getId())) {
            // 处理章节内容:去除空白符-全角转半角-去除空白行
            entity.setContent(processContent(entity.getContent()));
            // 计算章节字数
            entity.setWord(TxtUtils.getMSWordsCount(entity.getContent()));
            // 保存章节信息
            insert(entity);
            // 获取书籍信息
            BookEntity book = bookSer.selectById(entity.getBookid());
            // 计算书籍字数
            book.setWords(book.getWords() == null ? entity.getWord() : (book.getWords() + entity.getWord()));
            // 更新书籍字数
            return bookSer.updateById(book);
        } else {
            // 处理章节内容
            entity.setContent(processContent(entity.getContent()));
            // 计算章节字数
            entity.setWord(new TxtUtils().getMSWordsCount(entity.getContent()));
            // 更新章节信息
            updateById(entity);

            // 获取书籍信息
            BookEntity book = bookSer.selectById(entity.getBookid());
            // 获取原章节信息
            ChapterEntity chapter = selectById(entity.getId());
            // 计算书籍字数
            book.setWords(book.getWords() == null ? entity.getWord() : (book.getWords() - chapter.getWord() + entity.getWord()));
            // 更新书籍字数
            return bookSer.updateById(book);
        }
    }

    /**
     * 内容 处理
     */
    private String processContent(String content) {
        if (StringUtils.isBlank(content)) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();

            // 按行切分
            String[] lines = TxtUtils.splitLines(content);

            for (String line : lines) {
                // 判断行内是否存在内容
                if (StringUtils.isNotBlank(line)) {
                    // 简体转换为繁体：convertToTraditional
                    // 全角转半角 > 去空白符  > 繁体转简体 > 替换特殊字符 > 去除emoji字符
                    sb.append(TxtUtils.filterEmoji(ZhConverterUtil.convertToSimple(TxtUtils.replaceAllWhite(TxtUtils.toDBC(line)))
                            .replaceAll("「", "“").replaceAll("」", "”")
                            .replaceAll("\uE4C9", "").replaceAll("\uE5E5", "")));
                    sb.append("\r\n");
                }
            }

            return sb.toString();
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(ChapterEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<ChapterEntity> list = mapper.selectPage(page, getWrapper(entity));
        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 根据书籍ID获取最大章节序号
     */
    public Integer getMaxSortByBookId(String bookid) {
        return mapper.getMaxSortByBookId(bookid);
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<ChapterEntity> getWrapper(ChapterEntity entity) {
        EntityWrapper<ChapterEntity> wrapper = new EntityWrapper<>();

        if (StringUtils.isNotBlank(entity.getBookid())) {
            wrapper.eq("bookid", entity.getBookid());
        }

        if (StringUtils.isNotBlank(entity.getTitle())) {
            wrapper.like("title", entity.getTitle());
        }

        if (entity.getSort() != null) {
            wrapper.eq("sort", entity.getSort());
        }

        wrapper.orderBy("create_time");

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
