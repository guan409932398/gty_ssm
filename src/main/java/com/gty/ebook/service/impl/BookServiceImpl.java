package com.gty.ebook.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.ebook.mapper.BookMapper;
import com.gty.ebook.model.BookEntity;
import com.gty.ebook.service.BookService;
import com.gty.global.exception.CustomException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 书籍表 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 09:55:54
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, BookEntity> implements BookService {
    // Mapper对象
    @Autowired
    private BookMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(BookEntity entity) throws CustomException {
        // 根据书名查询
        EntityWrapper<BookEntity> ew = new EntityWrapper<>();
        ew.eq("title", entity.getTitle());

        List<BookEntity> books = selectList(ew);

        if (StringUtils.isBlank(entity.getId())) {
            if (books != null && !books.isEmpty()) {
                throw new CustomException("书名已存在");
            }

            return insert(entity);
        } else {
            if (books != null && !books.isEmpty()) {
                BookEntity book = books.get(0);

                if (!book.getId().equals(entity.getId())) {
                    throw new CustomException("书名已存在");
                }
            }

            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(BookEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<BookEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<BookEntity> getWrapper(BookEntity entity) {
        EntityWrapper<BookEntity> wrapper = new EntityWrapper<>();

        // 书籍名称
        if (StringUtils.isNotBlank(entity.getTitle())) {
            wrapper.like("title", entity.getTitle());
        }

        // 书籍作者
        if (StringUtils.isNotBlank(entity.getAuthor())) {
            wrapper.like("author", entity.getAuthor());
        }

        // 书籍状态
        if (StringUtils.isNotBlank(entity.getState())) {
            wrapper.eq("state", entity.getState());
        }

        // 书籍类别
        if (StringUtils.isNotBlank(entity.getCategory())) {
            wrapper.eq("category", entity.getCategory());
        }

        // 关键字
        if (StringUtils.isNotBlank(entity.getTag())) {
            // 拆分关键字
            String[] tags = entity.getTag().split(",");

            wrapper.andNew();
            wrapper.eq("1", 1);

            // 添加查询条件
            for (int i = 0; i < tags.length; i++) {
                if (i != 0) {
                    wrapper.or();
                }

                wrapper.like("tag", tags[i]);
            }
        }

        wrapper.orderBy("create_time", false);

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
