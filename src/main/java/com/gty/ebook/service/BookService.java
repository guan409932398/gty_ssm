package com.gty.ebook.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.ebook.model.BookEntity;

import java.util.Map;

/**
 * 书籍表 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 09:55:54
 */
public interface BookService extends IService<BookEntity> {

    /**
     * 保存对象
     */
    boolean save(BookEntity entity) throws CustomException;

    /**
     * 查询列表
     */
    Map<String, Object> query(BookEntity entity);
}
