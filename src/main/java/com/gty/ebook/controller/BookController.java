package com.gty.ebook.controller;

import com.gty.ebook.model.BookEntity;
import com.gty.ebook.service.BookService;
import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.sys.model.DictionaryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * 书籍表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 09:55:54
 */
@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService service;

    /**
     * 跳转书籍表新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd() {
        return "page/ebook/book/book_editor";
    }

    /**
     * 跳转书籍表编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/ebook/book/book_editor";
    }

    /**
     * 跳转书籍表列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/ebook/book/book_query";
    }

    /**
     * 书籍表保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(BookEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 书籍表删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * 书籍表明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = BookEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * 书籍表列表
     */
    @RequestMapping("/query")
    @JSON(type = BookEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(BookEntity entity) {
        Map<String, Object> map = service.query(entity);

        List<BookEntity> books = (List<BookEntity>) map.get("list");

        if (books != null && !books.isEmpty()) {
            books.forEach(book -> {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(book.getCategory());
                book.setCategoryName(dic == null ? "" : dic.getName());
            });
        }

        return Result.success(books, (Long) map.get("count"));
    }
}
