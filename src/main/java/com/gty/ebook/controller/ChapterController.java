package com.gty.ebook.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.gty.ebook.model.ChapterEntity;
import com.gty.ebook.service.BookService;
import com.gty.ebook.service.ChapterService;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.utils.TxtUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 章节表 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 10:06:04
 */
@Controller
@RequestMapping("/chapter")
public class ChapterController {

    @Autowired
    private ChapterService service;
    @Autowired
    private BookService bookSer;

    /**
     * 跳转章节表新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd(String bookid, Model model) {
        ChapterEntity chapter = new ChapterEntity();
        chapter.setBookid(bookid);
        Integer maxSort = service.getMaxSortByBookId(bookid);
        // 查询当前最大章节号
        chapter.setSort(maxSort == null || maxSort.intValue() == 0 ? 1 : ++maxSort);
        chapter.setTitle("第" + String.format("%03d", chapter.getSort()) + "章");
        model.addAttribute("po", chapter);
        return "page/ebook/chapter/chapter_editor";
    }

    /**
     * 跳转章节表编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/ebook/chapter/chapter_editor";
    }

    /**
     * 跳转章节表列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery(String bookId, String bookName, Model model) {
        model.addAttribute("bookId", bookId);
        model.addAttribute("bookName", bookName);
        return "page/ebook/chapter/chapter_query";
    }

    /**
     * 跳转章节表明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        ChapterEntity chapter = service.selectById(id);
        String content = chapter.getContent();
        chapter.setContent("");

        model.addAttribute("po", chapter);
        model.addAttribute("author", bookSer.selectById(chapter.getBookid()).getAuthor());
        model.addAttribute("contents", TxtUtils.splitLines(content));
        return "page/ebook/chapter/chapter_detail";
    }

    /**
     * 跳转章节表明细页面
     */
    @RequestMapping("/goFormat")
    public String goFormat(String id, Model model) {
        model.addAttribute("po", service.selectById(id));
        return "page/ebook/chapter/chapter_format";
    }

    /**
     * 章节表保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(ChapterEntity entity) throws CustomException {
        return Result.success(service.save(entity));
    }

    /**
     * 章节表删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * 章节表明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = ChapterEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * 章节表列表
     */
    @RequestMapping("/query")
    @JSON(type = ChapterEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(ChapterEntity entity) {
        Map<String, Object> map = service.query(entity);
        return Result.success(map.get("list"), (Long) map.get("count"));
    }

    /**
     * 文件下载
     */
    @RequestMapping(value = "/download")
    public void download(String bookId, String bookName, HttpServletResponse response)
            throws IOException {
        EntityWrapper<ChapterEntity> wrapper = new EntityWrapper<>();

        wrapper.eq("bookid", bookId);

        wrapper.orderBy("sort");

        // 根据书籍ID查询所有书籍列表
        List<ChapterEntity> chapters = service.selectList(wrapper);
        ServletOutputStream out = null;

        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("multipart/form-data");
            response.addHeader("Content-Disposition", "attachment; filename=" + new String(bookName.getBytes("utf-8"), "ISO8859-1") + ".txt");
            out = response.getOutputStream();

            if (chapters != null && !chapters.isEmpty()) {
                // 遍历所有章节
                for (ChapterEntity chapter : chapters) {
                    // 标题
                    byte[] title = chapter.getTitle().getBytes();
                    out.write(title, 0, title.length);
                    out.flush();

                    // 换行
                    byte[] br = new String("\r\n").getBytes();
                    out.write(br, 0, br.length);
                    out.flush();

                    // 内容
                    byte[] content = chapter.getContent().getBytes();
                    out.write(content, 0, content.length);
                    out.flush();

                    // 换行
                    byte[] chapterBr = new String("\r\n").getBytes();
                    out.write(chapterBr, 0, chapterBr.length);
                    out.flush();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
