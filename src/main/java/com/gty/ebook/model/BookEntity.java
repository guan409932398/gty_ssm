package com.gty.ebook.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 书籍表 Entity
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 09:55:54
 */
@TableName("tb_ebook_book")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 书籍标题
    @TableField("title")
    private String title;
    // 作者
    @TableField("author")
    private String author;
    // 书籍状态
    @TableField("state")
    private String state;
    // 类别
    @TableField("category")
    private String category;
    // 字数
    @TableField("words")
    private Integer words;
    // 校对（checked：已校对，unchecked：未校对）
    @TableField("proofread")
    private String proofread;
    // 关键字
    @TableField("tag")
    private String tag;
    // 备注
    @TableField("remark")
    private String remark;

    // 类别名称
    @TableField(exist = false)
    private String categoryName;
}
