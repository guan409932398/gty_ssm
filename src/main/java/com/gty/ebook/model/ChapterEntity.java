package com.gty.ebook.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 章节表 Entity
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-06-20 10:06:04
 */
@TableName("tb_ebook_chapter")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChapterEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 章节标题
    @TableField("title")
    private String title;
    // 章节序号
    @TableField("sort")
    private Integer sort;
    // 字数
    @TableField("word")
    private Integer word;
    // 章节内容
    @TableField("content")
    private String content;
    // 书籍ID
    @TableField("bookid")
    private String bookid;
    // 校对（checked：已校对，unchecked：未校对）
    @TableField("proofread")
    private String proofread;
}
