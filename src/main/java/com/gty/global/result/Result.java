package com.gty.global.result;

import lombok.Getter;
import lombok.Setter;

/**
 * 全局返回格式
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Getter
@Setter
public class Result {
    // 返回代码
    private Integer code;
    // 返回信息
    private String msg;
    // 返回数据
    private Object data;
    // 返回记录数
    private Long count;

    private Result() {

    }

    /**
     * 成功返回
     */
    public static Result success() {
        Result result = new Result();

        result.setCode(200);

        return result;
    }

    /**
     * 成功返回
     */
    public static Result success(Object obj) {
        Result result = new Result();

        result.setCode(200);
        result.setMsg("success");
        result.setData(obj);

        return result;
    }

    /**
     * 成功返回
     */
    public static Result success(Object obj, Long count) {
        Result result = new Result();

        result.setCode(200);
        result.setMsg("success");
        result.setCount(count);
        result.setData(obj);

        return result;
    }

    /**
     * 失败返回
     */
    public static Result fail(String msg) {
        Result result = new Result();

        result.setCode(600);
        result.setMsg(msg);

        return result;
    }
}