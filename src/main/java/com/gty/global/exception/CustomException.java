package com.gty.global.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 自定义异常类
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Getter
@Setter
public class CustomException extends Exception {
    // 错误说明
    private String msg;

    public CustomException(String msg) {
        this.msg = msg;
    }
}