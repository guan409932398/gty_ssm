package com.gty.global.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gty.global.result.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * 全局异常处理器
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public class CustomExceptionResolver implements HandlerExceptionResolver {
    /**
     * 日志log
     */
    private static Logger log = LoggerFactory.getLogger(CustomExceptionResolver.class);

    // 系统抛出的异常
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
                                         Exception ex) {
        ModelAndView mv = new ModelAndView("500");
        // handler就是处理器适配器要执行的Handler对象(只有method)
        // 解析出异常类型。
        /* 使用response返回 */
        response.setStatus(HttpStatus.OK.value()); // 设置状态码
        response.setContentType(MediaType.APPLICATION_JSON_VALUE); // 设置ContentType
        response.setCharacterEncoding("UTF-8"); // 避免乱码
        response.setHeader("Cache-Control", "no-cache, must-revalidate");

        try {
            // 如果该异常是自定义的异常，直接取出异常信息。
            if (ex instanceof CustomException) {
                response.getWriter().write(new ObjectMapper().writeValueAsString(Result.fail(((CustomException) ex).getMsg())));
            } else {
                String errorId = UUID.randomUUID().toString().replace("-", "");

                log.error("------------------------>" + errorId, ex);

                response.getWriter().write(new ObjectMapper().writeValueAsString(Result.fail("Server error(" + errorId + ")!")));
            }
        } catch (IOException e) {
            log.error("与客户端通讯异常:" + e.getMessage(), e);
            e.printStackTrace();
        }

        return mv;
    }
}