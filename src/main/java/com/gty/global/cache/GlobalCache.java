package com.gty.global.cache;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.gty.sys.model.ConfigEntity;
import com.gty.sys.model.DictionaryEntity;
import com.gty.sys.model.SmsEntity;
import com.gty.sys.service.ConfigService;
import com.gty.sys.service.DictionaryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 全局缓存
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Component
public class GlobalCache {

    // 配置服务
    private static ConfigService confSer;
    // 字典服务
    private static DictionaryService dicSer;

    /**
     * Token 缓存(自动续命机制)
     */
    public static Cache<String, String> TOKEN_CACHE = Caffeine.newBuilder().expireAfterAccess(5, TimeUnit.SECONDS)
            .build();

    /**
     * Sms 缓存(固定时长机制)
     */
    public static Cache<String, SmsEntity> SMS_CACHE = Caffeine.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();

    /**
     * Config 缓存(自动续命机制)
     */
    public static LoadingCache<String, String> CONFIG_CACHE = Caffeine.newBuilder()
            .expireAfterAccess(30, TimeUnit.SECONDS).build(code -> loadingConfig(code));

    /**
     * Dictionary 缓存(自动续命机制)
     */
    public static LoadingCache<String, DictionaryEntity> DIC_CACHE = Caffeine.newBuilder()
            .expireAfterAccess(30, TimeUnit.SECONDS).build(code -> loadingDic(code));

    /**
     * 加载配置
     */
    private static String loadingConfig(String code) {
        ConfigEntity config = confSer.selectOne(new EntityWrapper<ConfigEntity>().eq("code", code));
        return config == null || StringUtils.isBlank(config.getId()) ? "" : config.getValues();
    }

    /**
     * 加载字典
     */
    private static DictionaryEntity loadingDic(String code) {
        return dicSer.detailByCode(code);
    }

    @Autowired
    public void setConfSer(ConfigService confSer) {
        GlobalCache.confSer = confSer;
    }

    @Autowired
    public void setDicSer(DictionaryService dicSer) {
        GlobalCache.dicSer = dicSer;
    }
}
