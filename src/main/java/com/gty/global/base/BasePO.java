package com.gty.global.base;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * base po
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasePO implements Serializable {
    private static final long serialVersionUID = 1L;

    // 配置ID
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    // 创建时间
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Long createTime;
    // 最后修改时间
    @TableField(value = "modify_time", fill = FieldFill.INSERT_UPDATE)
    private Long modifyTime;
    // 记录状态
    @TableField(value = "record_state", fill = FieldFill.INSERT)
    private String recordState;

    // 页数
    @TableField(exist = false)
    private int page = 1;
    // 每页记录数
    @TableField(exist = false)
    private int limit = 10;
    // 总记录数
    @TableField(exist = false)
    private Long count = 0L;

    // 开始日期
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;
    // 结束日期
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    // 查询关键字
    @TableField(exist = false)
    private String searchKey;
    // 一些键
    @TableField(exist = false)
    private String moreKey;

    // 触发弹层的弹层index值
    @TableField(exist = false)
    private Integer index;
    // 值字段
    @TableField(exist = false)
    private String valueField;
    // 显示字段
    @TableField(exist = false)
    private String showField;

    // 排序字段
    @TableField(exist = false)
    private String sortField;
    // 排序规则
    @TableField(exist = false)
    private String sortRole;
}
