package com.gty.global.base;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

@Component
public class BaseFieldFillHandler extends MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createTime", DateUtil.currentSeconds(), metaObject);
        this.setFieldValByName("modifyTime", DateUtil.currentSeconds(), metaObject);
        this.setFieldValByName("recordState", "active", metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("modifyTime", DateUtil.currentSeconds(), metaObject);
    }
}
