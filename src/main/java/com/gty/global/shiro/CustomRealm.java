package com.gty.global.shiro;

import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.gty.sys.mapper.UserMapper;
import com.gty.sys.model.MenuEntity;
import com.gty.sys.model.UserEntity;
import com.gty.sys.service.MenusService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 自定义权限控制
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public class CustomRealm extends AuthorizingRealm {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MenusService menusSer;

    public CustomRealm() {

    }

    @Override
    public String getName() {
        return "CustomRealm";
    }

    /**
     * realm授权方法 从输入参数principalCollection得到身份信息 根据身份信息到数据库查找权限信息 将权限信息添加给授权信息对象 返回
     * 授权信息对象(判断用户访问url是否在权限信息中没有体现)
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        UserEntity user = (UserEntity) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(user.getUserRoles());
        return info;
    }

    /**
     * 表单认证过滤器认证时会调用自定义Realm的认证方法进行认证，成功回到index.do，再跳转到index.jsp页面
     * <p>
     * 前提：表单认证过滤器收集和组织用户名和密码信息封装为token对象传递给此方法
     * <p>
     * token:封装了身份信息和凭证信息 2步骤：比对身份 信息；比对凭证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String account = (String) token.getPrincipal();
        String password = new String((char[]) token.getCredentials());

        // 根据帐号获取用户对象
        UserEntity entity = new UserEntity();
        entity.setAccount(account);

        UserEntity user = userMapper.selectOne(entity);

        if (user == null || StringUtils.isBlank(user.getId())) {
            throw new UnknownAccountException("用户不存在");
        } else if (StringUtils.isBlank(user.getPassword())) {
            throw new UnknownAccountException("用户密码未初始化");
        } else if (StringUtils.isBlank(user.getUserRole())) {
            throw new UnknownAccountException("账号未分配角色");
        } else if (!user.getPassword().equals(new MD5().digestHex(password))) {
            throw new UnknownAccountException("帐号或密码不正确");
        } else if (!"active".equals(user.getState())) {
            throw new UnknownAccountException("用户被封禁");
        } else {
            // 写入shrio认证
            SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, new MD5().digestHex(password), getName());

            // 清除密码
            user.setPassword("");

            // 根据角色ID查询角色菜单
            MenuEntity vo = new MenuEntity();
            vo.setParentId("GTY-QQ:409932398");

            // 判断是否为超管角色
            if ("GTY-QQ:409932398".equals(user.getUserRole()) || "GTY-QQ:409932398".equals(user.getId())) {
                // 查询所有菜单
                user.setUserMenus(menusSer.queryALLHierarchy(vo));
                // 查询所有权限代码
                List<MenuEntity> menus = menusSer.selectList(new EntityWrapper<>());

                if (menus != null && !menus.isEmpty()) {
                    Set<String> roles = new HashSet<>();

                    for (MenuEntity menu : menus) {
                        if (org.apache.commons.lang3.StringUtils.isNotBlank(menu.getPerms())) {
                            roles.add(menu.getPerms());
                        }
                    }

                    user.setUserRoles(roles);
                }
             } else {
                vo.setRoleId(user.getUserRole());
                user.setUserMenus(menusSer.queryJoinRoleHierarchy(vo));
                user.setUserRoles(getPerms(user.getUserMenus()));
            }

            return info;
        }
    }

    /**
     * 获取用户权限
     */
    private Set<String> getPerms(List<MenuEntity> menus) {
        Set<String> perms = new HashSet<>();

        if (menus != null && !menus.isEmpty()) {
            for (MenuEntity menu : menus) {
                if (menu.getChildNodes() != null && !menu.getChildNodes().isEmpty()) {
                    perms.addAll(getPerms(menu.getChildNodes()));
                }

                if (StringUtils.isNotBlank(menu.getPerms())) {
                    perms.add(menu.getPerms());
                }
            }
        }

        return perms;
    }
}
