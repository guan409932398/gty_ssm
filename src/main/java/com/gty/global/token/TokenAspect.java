package com.gty.global.token;

import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Token AOP
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
@Aspect
@Component
public class TokenAspect {
    // Controller层切点
    @Pointcut("@annotation(com.gty.global.token.Token)")
    public void controllerAspect() {
    }

    /**
     * 前置通知 用于拦截Controller层记录用户的操作
     */
    @Before("controllerAspect()")
    public void doBefore() throws CustomException {
        // TODO:统一Token验证处理
        if (!"close".equals(GlobalCache.CONFIG_CACHE.get("app_token_control"))) {
            // 获取session中当前用户
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                    .getRequest();

            String secretKey = request.getParameter("secretKey");
            String token = request.getParameter("token");

            if (StringUtils.isBlank(secretKey) || StringUtils.isBlank(token)) {
                throw new CustomException("secretKey和token参数不能为空!");
            }

            String serToken = GlobalCache.TOKEN_CACHE.getIfPresent(secretKey);

            if (StringUtils.isBlank(serToken)) {
                throw new CustomException("token不存在!");
            } else if (!serToken.equals(token)) {
                throw new CustomException("token验证失败!");
            }
        }
    }
}