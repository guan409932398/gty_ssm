package com.gty.global.el;

import com.gty.global.cache.GlobalCache;
import com.gty.sys.model.DictionaryEntity;

/**
 * 自定义EL方法
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 20190611
 */
public class ELFunc {

    /**
     * 配置value
     */
    public static String getCon(String code) {
        return GlobalCache.CONFIG_CACHE.get(code);
    }

    /**
     * 字典 name
     */
    public static DictionaryEntity getDic(String code) {
        return GlobalCache.DIC_CACHE.get(code);
    }
}
