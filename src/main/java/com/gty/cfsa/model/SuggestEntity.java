package com.gty.cfsa.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.gty.global.base.BasePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 立项建议征集 Entity
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-01 09:35:25
 */
@TableName("tb_cfsa_suggest")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuggestEntity extends BasePO {
    private static final long serialVersionUID = 1L;

    // 标准名称（bzname）
    @TableField("name")
    private String name;
    // 制订或修订（zdhxg，make：制定，amend：修订）
    @TableField("make_or_amend")
    private String makeOrAmend;
    // 被修订标准号（bxdbzh）
    @TableField("be_revised_standard_no")
    private String beRevisedStandardNo;
    // 标准类别（bzlb）
    @TableField("standard_category")
    private String standardCategory;
    // 项目提出单位_单位ID（comid）
    @TableField("drafting_unit_id")
    private String draftingUnitId;
    // 项目提出单位_单位名称（comname）
    @TableField("drafting_unit_name")
    private String draftingUnitName;
    // 项目提出单位_地址（comaddress）
    @TableField("drafting_unit_address")
    private String draftingUnitAddress;
    // 项目提出单位_联系人ID（userid）
    @TableField("contact_id")
    private String contactId;
    // 项目提出单位_联系人（username）
    @TableField("contact_name")
    private String contactName;
    // 项目提出单位_联系电话（userphone）
    @TableField("contact_phone")
    private String contactPhone;
    // 项目提出单位_电子邮箱（useremail）
    @TableField("contact_email")
    private String contactEmail;
    // 候选起草单位_单位名称（hxqc_comname）
    @TableField("candidate_unit_name")
    private String candidateUnitName;
    // 候选起草单位_联系人（hxqc_lxr）
    @TableField("candidate_unit_contact_name")
    private String candidateUnitContactName;
    // 候选起草单位_联系电话（hxqc_lxr_phone）
    @TableField("candidate_unit_contact_phone")
    private String candidateUnitContactPhone;
    // 候选起草单位_完成项目所需时限（hxqc_wcsxsx）
    @TableField("complete_time")
    private String completeTime;
    // 拟解决的食品安全问题（njjdspaqwt）
    @TableField("ready_solve_food_safety_issues")
    private String readySolveFoodSafetyIssues;
    // 立项背景和理由（lxbjhly）
    @TableField("background_and_reasons")
    private String backgroundAndReasons;
    // 主要技术指标已开展的风险监测和风险评估情况（zyjszbhfxpg）
    @TableField("technical_indicators_related_information")
    private String technicalIndicatorsRelatedInformation;
    // 标准范围和主要技术内容（bzfwhzyjsnr）
    @TableField("scope_and_content")
    private String scopeAndContent;
    // 国际同类标准和国内相关法规标准情况（gjtlbzhgnxgfgbzqk）
    @TableField("international_equivalent_information")
    private String internationalEquivalentInformation;
    // 项目成本预算（xmcbys）
    @TableField("cost_budget")
    private BigDecimal costBudget;
    // 经费使用计划（jfsyjh）
    @TableField("expenditure_plan")
    private String expenditurePlan;
    // 创建人（log_userid）
    @TableField("create_user_id")
    private String createUserId;
    // 意见状态（status）
    @TableField("opinion_state")
    private String opinionState;
    // 提交时间（submit_date）
    @TableField("submit_time")
    private Long submitTime;
    // 提交操作人ID（submit_userid）
    @TableField("submit_user_id")
    private String submitUserId;
    // 是否转成正式标准（inusing）
    @TableField("turn_standard")
    private String turnStandard;
    // 转成正式项目的人员编号（turn_userid）
    @TableField("turn_user_id")
    private Integer turnUserId;
    // 转成正式项目的时间（turn_date）
    @TableField("turn_date")
    private Long turnDate;
    // 退回原因（th_reason）
    @TableField("reasons_rejection")
    private String reasonsRejection;

    // 类别名称
    @TableField(exist = false)
    private String categoryName;
}
