package com.gty.cfsa.controller;

import com.gty.global.cache.GlobalCache;
import com.gty.global.exception.CustomException;
import com.gty.global.json.JSON;
import com.gty.global.result.Result;
import com.gty.cfsa.model.SuggestEntity;
import com.gty.cfsa.service.SuggestService;
import com.gty.global.shiro.ShiroUtils;
import com.gty.sys.model.DictionaryEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * 立项建议征集 Controller
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-01 09:35:25
 */
@Controller
@RequestMapping("/suggest")
public class SuggestController {

    @Autowired
    private SuggestService service;

    /**
     * 跳转立项建议征集新增页面
     */
    @RequestMapping("/goAdd")
    public String goAdd(Model model) {
        // 用户信息
        model.addAttribute("company", ShiroUtils.getUserEntity());
        return "page/cfsa/suggest/suggest_editor";
    }

    /**
     * 跳转立项建议征集编辑页面
     */
    @RequestMapping("/goEditor")
    public String goEditor(String id, Model model) {
        // 用户信息
        model.addAttribute("company", ShiroUtils.getUserEntity());
        model.addAttribute("po", service.selectById(id));
        return "page/cfsa/suggest/suggest_editor";
    }

    /**
     * 跳转立项建议征集列表页面
     */
    @RequestMapping("/goQuery")
    public String goQuery() {
        return "page/cfsa/suggest/suggest_query";
    }

    /**
     * 跳转立项建议征集明细页面
     */
    @RequestMapping("/goDetail")
    public String goDetail(String id, Model model) {
        SuggestEntity suggest = service.selectById(id);


        if (StringUtils.isNotBlank(suggest.getStandardCategory())) {
            DictionaryEntity dic = GlobalCache.DIC_CACHE.get(suggest.getStandardCategory());

            suggest.setCategoryName(dic == null ? "" : dic.getName());
        }

        model.addAttribute("po", suggest);
        return "page/cfsa/suggest/suggest_detail";
    }

    /**
     * 立项建议征集保存信息
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result save(SuggestEntity entity) throws CustomException {
        // 写入操作用户
        entity.setCreateUserId(ShiroUtils.getUserId());
        return Result.success(service.save(entity));
    }

    /**
     * 立项建议征集删除信息
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @JSON(type = Result.class, filter = "count")
    public Result del(String id) {
        return Result.success(service.deleteById(id));
    }

    /**
     * 立项建议征集明细
     */
    @RequestMapping("/detail")
    @JSON(type = Result.class, filter = "count")
    @JSON(type = SuggestEntity.class, filter = "page,limit,count,index,type,searchKey,beginDate,endDate,moreKey,valueField,showField")
    public Result detail(String id) {
        return Result.success(service.selectById(id));
    }

    /**
     * 立项建议征集列表
     */
    @RequestMapping("/query")
    @JSON(type = SuggestEntity.class, filter = "page,limit,count,index,type,search_key,beginDate,endDate,begin_date,end_date,more_key,value_field,show_field")
    public Result query(SuggestEntity entity) {
        // 获取session中用户
        entity.setCreateUserId(ShiroUtils.getUserId());
        Map<String, Object> map = service.query(entity);

        List<SuggestEntity> suggests = (List<SuggestEntity>) map.get("list");

        if (suggests != null && !suggests.isEmpty()) {
            suggests.forEach(suggest -> {
                DictionaryEntity dic = GlobalCache.DIC_CACHE.get(suggest.getStandardCategory());
                suggest.setCategoryName(dic == null ? "" : dic.getName());
            });
        }

        return Result.success(suggests, (Long) map.get("count"));
    }
}
