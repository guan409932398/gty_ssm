package com.gty.cfsa.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gty.cfsa.mapper.SuggestMapper;
import com.gty.cfsa.model.SuggestEntity;
import com.gty.cfsa.service.SuggestService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 立项建议征集 Service Impl
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-01 09:35:25
 */
@Service
public class SuggestServiceImpl extends ServiceImpl<SuggestMapper, SuggestEntity> implements SuggestService {
    // Mapper对象
    @Autowired
    private SuggestMapper mapper;

    /**
     * 保存对象
     */
    @Override
    public boolean save(SuggestEntity entity) {
        if (StringUtils.isBlank(entity.getId())) {
            return insert(entity);
        } else {
            return updateById(entity);
        }
    }

    /**
     * 查询列表
     */
    @Override
    public Map<String, Object> query(SuggestEntity entity) {
        Page page = new Page(entity.getPage(), entity.getLimit());

        List<SuggestEntity> list = mapper.selectPage(page, getWrapper(entity));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("count", page.getTotal());
        resultMap.put("list", list);

        return resultMap;
    }

    /**
     * 组装查询条件
     */
    private EntityWrapper<SuggestEntity> getWrapper(SuggestEntity entity) {
        EntityWrapper<SuggestEntity> wrapper = new EntityWrapper<>();

        // 登录用户ID
        if (StringUtils.isNotBlank(entity.getCreateUserId())) {
            wrapper.eq("create_user_id", entity.getCreateUserId());
        }

        // 名称
        if (StringUtils.isNotBlank(entity.getName())) {
            wrapper.eq("name", entity.getName());
        }

        // 类别
        if (StringUtils.isNotBlank(entity.getStandardCategory())) {
            wrapper.eq("standard_category", entity.getStandardCategory());
        }

        // 制定/修订
        if (StringUtils.isNotBlank(entity.getMakeOrAmend())) {
            wrapper.eq("make_or_amend", entity.getMakeOrAmend());
        }

        // 提交日期
        if (entity.getBeginDate() != null) {
            wrapper.ge("create_time", entity.getBeginDate().getTime());
        }

        if (entity.getEndDate() != null) {
            wrapper.le("create_time", entity.getEndDate().getTime());
        }

        // 按创建时间
        wrapper.orderBy("create_time", false);

        if (entity == null) {
            return wrapper;
        }

        return wrapper;
    }
}
