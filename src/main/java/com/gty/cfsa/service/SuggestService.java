package com.gty.cfsa.service;

import com.baomidou.mybatisplus.service.IService;
import com.gty.global.exception.CustomException;
import com.gty.cfsa.model.SuggestEntity;

import java.util.Map;

/**
 * 立项建议征集 Service
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-01 09:35:25
 */
public interface SuggestService extends IService<SuggestEntity> {

    /**
     * 保存对象
     */
    boolean save(SuggestEntity entity);

    /**
     * 查询列表
     */
    Map<String, Object> query(SuggestEntity entity);
}
