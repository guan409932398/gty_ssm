package com.gty.cfsa.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gty.cfsa.model.SuggestEntity;

/**
 * 立项建议征集 Mapper
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-01 09:35:25
 */
public interface SuggestMapper extends BaseMapper<SuggestEntity> {
}
