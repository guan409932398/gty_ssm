package com.gty.file;

import cn.hutool.core.io.FileUtil;
import com.gty.utils.MapUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;

/**
 * CMMI5文档修改
 *
 * @author 官天野
 * @version v1.0
 * @email: guan409932398@qq.com
 * @date 2019-07-03
 */
public class CMMI5FileTest {
    // CMMI5文档路径
    private final static String FILE_PATH = "C:\\Users\\Administrator\\Desktop\\青岛鹏顺源实地管理信息系统\\01管理域\\";

    public static void main(String[] args) {
        List<File> files = FileUtil.loopFiles(FILE_PATH);

        if (!files.isEmpty()) {
            for (File file : files) {
                // 重命名文件夹
                // file.renameTo(new File(file.getPath().replaceAll("ZZ-3DGIS", "FT-PSY")));
                System.out.println(file.getPath());
            }
        }

        System.out.println("处理结束，处理文件数：" + files.size());
    }
}
