package com.gty.mee.service.impl;

import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 申报-共享接口测试类
 * @Date: 2019/4/12
 * @Version: 1.0
 */
public class MeeProjectTest {
    // 接口地址
    // static final String url = "http://localhost:8051/signApi/";
    // static final String url = "http://114.251.10.204/declare/signApi/";
    static final String url = "http://114.251.10.204/baseinfo/signApi/";

    // 审批
    // static final String url = "http://114.251.10.204/baseinfo/app/api/";
    // static final String url = "http://114.251.10.204/declare/EE/api/";

    static final String EKey = "8HkocpYLeG1LNi5m";
    // static final String EKey = "!Q2w3e4r5t6y7u8i9o";

    public static void main(String[] args) {
        Map map = getParam();
        String version = "1.0";
        String time = String.valueOf(System.currentTimeMillis() / 1000);
        String data = JSON.toJSONString(map);
        String sign = new MD5().digestHex(getService() + time + data + version + EKey);

        Map param = new HashMap() {{
            put("version", version);
            put("time", time);
            put("sign", sign);
            put("data", data);
            // put("test", "yes");
            // put("selfId", "7");
            // put("sessionId", "80d1994f3851d8e14ed3a1cb8387cdb5");
        }};

        String result = HttpUtil.post(url + getService(), param);
        System.out.println(result);
    }

    /**
     * 接口名称
     *
     * @return String
     */
    protected static String getService() {
        // return "getCompanyDetail";
        return "getCompanyDetail";
        // return "getAppTokenService";
    }

    /**
     * 参数，其中key必传
     *
     * @return 要查询的条件
     */
    protected static Map getParam() {
        return new HashMap() {{
            put("key", "28ac5852a84b4413953f5f525ffab4ca"); // 山东
            // put("page", 1);
            // put("limit", 10);
            // put("id", 7164);
            // put("projectId", 7164);
            // put("id", 10844);
            put("companyId", 34875);
        }};
    }

}
