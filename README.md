# gty_ssm

#### 介绍
	SSM Project for 官天野

#### 系统架构
    layui: 2.4.5
    spring.version: 4.3.10.RELEASE
    aspectjweaver.version: 1.8.13
    mysql.version: 5.1.30
    druid.version: 1.0.18
    mybaitsplus.version: 2.3
    shiro.version: 1.2.3
    caffeine.version: 2.6.2
    log4j.version: 1.2.17
    slf4j.version: 1.7.19
    fastjson.version: 1.2.8
    jackson.version: 2.8.5
    servlet.version: 3.1.0
    jsp.version: 2.3.1
    jstl.version: 1.2
    lang.version: 2.6
    lang3.version: 3.7
    fileupload.version: 1.3.1
    dbcp.version: 1.4
    kaptcha.version: 2.3.2
    hutool.version: 4.5.10
    lombok.version: 1.16.6
    junit.version: 4.12

#### 环境信息
	IDE: Idea 2019
	JDK：1.8.0_161
	Tomcat：8.5.40
	Shiro：1.2.3
	Mysql：5.5
	Maven：3.6.0
	
#### 数据库文件位置
	\gty_ssm\WebContent\WEB-INF\db\gty_ssm.sql

#### 代码生成器使用说明
	1. 只支持mysql
	2. 生成表主键必须为varchar(32)类型
	3. 生成表只允许单列主键
	4. MODEL_NAME 不允许出现特殊字符
	5. 表名必须为：【前缀_功能名】命名，前缀：【tb_模块名_】
	6. 表名中的功能名: 尽量为单个单词（例: tb_oa_meeting , tb_shop_goods）
	7. 表中必须存在 create_time(datetime) 字段（用于生成默认排序）
	8. 列中文名尽量4-5个字，否则生成出页面的样式会错位。
	8. MAC系统可能会因为 \ 问题造成生成时报错。